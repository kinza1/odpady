module Importer
  # This class is used to import seed data from the old database
  class User < Base
    private

    def process(row)
      return if empty?(row)
      ::User.create!(
        options(row)
      )
    end

    # Since import data we received has empty lines
    # we have to ignore them
    def empty?(row)
      row['login'].blank? || row['login'] == 'user@odpady-help.pl'
    end

    def options(row)
      { meta_id: row['id'],
        username: row['login'],
        first_name: row['first_name'],
        last_name: row['last_name'],
        email: email(row),
        password: password(row),
        approved: approved?(row),
        admin: admin?(row),
        category: category }
    end

    # Since our version of the import file has no data for emails
    # we temporary replace them with placeholders
    # until the receiving of the real files
    def email(row)
      "user-#{row['id']}@odpady-help.pl"
    end

    # Since password hashing algorythm was not provided by the client
    # we temporary replace all the passwords with some default value
    def password(_row)
      'qwerty123'
    end

    def approved?(row)
      row['confirmed'].to_s == '1'
    end

    def admin?(row)
      row['group_id'].to_s == '1'
    end

    def category
      @category ||= ::User::Category.order(id: :asc).first
    end
  end
end
