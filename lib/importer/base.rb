module Importer
  # Base class for all the data importers
  class Base
    attr_reader :filename, :imported_rows

    # @param filename [String] name of the file that should be imported
    def initialize(filename:)
      @filename = filename
      @imported_rows = 0
    end

    def call
      ActiveRecord::Base.transaction { call! }
      Rails.logger.info("Imported #{imported_rows} users")
    end

    def call!
      SmarterCSV.process filename, import_options do |chunk|
        chunk.each do |row|
          process(row)
          @imported_rows += 1
        end
      end
    end

    private

    def process(_row)
      raise 'Implement in child class'
    end

    def import_options
      { chunk_size: 100, strings_as_keys: true, downcase_header: false }
    end
  end
end
