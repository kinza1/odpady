module Importer
  module Forum
    class Section < Base
      private

      def process(row)
        ::Forum::Section.where(
          name: row['name']
        ).first_or_create!(
          meta_id: row['id']
        )
      end
    end
  end
end
