module Importer
  module Forum
    class Thread < Base
      private

      def import_options
        super.merge(col_sep: ';')
      end

      def process(row)
        thread = ::Forum::Thread.create!(
          options(row)
        )
        ::Forum::Post.create!(
          thread: thread,
          user: user(row),
          username: username(row),
          message: message(row),
          approved: approved?(row),
          created_at: row['created'],
          updated_at: row['modified']
        )
        thread.update!(updated_at: row['modified'])
      end

      def options(row)
        { meta_id: row['id'],
          user: user(row),
          section: section(row),
          username: username(row),
          subject: subject(row),
          approved: approved?(row),
          closed: closed?(row),
          closed_date: closed_date(row),
          views: row['views'],
          created_at: row['created'],
          updated_at: row['modified'] }
      end

      def user(row)
        return if row['user_id'].blank?
        ::User.find_by(meta_id: row['user_id'])
      end

      def username(row)
        return if user(row)
        row['author']
      end

      def subject(row)
        ActionController::Base.helpers.sanitize(
          row['name'].to_s
        ).truncate(100)
      end

      def section(row)
        if row['category_id'].present?
          ::Forum::Section.find_by(meta_id: row['category_id']) || default_section
        else
          default_section
        end
      end

      def default_section
        ::Forum::Section.find_by(name: 'Dyskusja Ogólna')
      end

      def message(row)
        ActionController::Base.helpers.sanitize(
          row['content'].to_s
        ).truncate(1500)
      end

      def approved?(row)
        row['status'].to_s == '1'
      end

      def closed?(row)
        row['is_closed'].to_s == '1'
      end

      def closed_date(row)
        return unless closed?(row)
        row['modified']
      end
    end
  end
end
