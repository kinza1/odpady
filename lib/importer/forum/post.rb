module Importer
  module Forum
    class Post < Base
      private

      def import_options
        super.merge(col_sep: ';')
      end

      def process(row)
        thread = thread(row)
        # There are deleted threads in the old database
        # We can't import replies to these threads
        return unless thread
        old_modification_date = thread.updated_at
        thread.posts.create!(options(row))
        thread.update!(updated_at: old_modification_date)
      end

      def options(row)
        { meta_id: row['id'],
          user: user(row),
          username: username(row),
          message: message(row),
          approved: approved?(row),
          created_at: row['created'],
          updated_at: row['modified'] }
      end

      def thread(row)
        ::Forum::Thread.find_by(meta_id: row['topic_id'])
      end

      def user(row)
        return if row['user_id'].blank?
        ::User.find_by(meta_id: row['user_id'])
      end

      def username(row)
        return if user(row)
        row['author']
      end

      def message(row)
        ActionController::Base.helpers.sanitize(
          row['content'].to_s
        ).truncate(1500)
      end

      def approved?(row)
        row['status'].to_s == '1'
      end
    end
  end
end
