require 'csv'

namespace :import do
  task users: :environment do
    path = ENV['FILE']
    Importer::User.new(filename: path).call
  end

  task wastes: :environment do
    path = ENV['FILE']
    raise ArgumentError, 'You should pass path to xml by FILE env' if path.blank?

    CSV.foreach(path) do |row|
      unless row[0].eql?('id')
        Waste.create id: row[0].to_i,
                     code: row[1].gsub(/\s+?$/, ''),
                     parent_id: row[2].present? ? row[2].to_i : nil,
                     name: row[4],
                     dry_mass: row[5].eql?('1'),
                     disposal: row[6].eql?('1'),
                     recycling: row[7].eql?('1'),
                     org_fiz: row[8].eql?('1'),
                     battery: row[9].eql?('1'),
                     dangerous: row[10].eql?('1'),
                     communal: row[11].eql?('1')
      end
    end
  end

  namespace :forum do
    task sections: :environment do
      path = ENV['FILE']
      Importer::Forum::Section.new(filename: path).call
    end

    task threads: :environment do
      path = ENV['FILE']
      Importer::Forum::Thread.new(filename: path).call
    end

    task posts: :environment do
      path = ENV['FILE']
      Importer::Forum::Post.new(filename: path).call
    end
  end
end
