server 'ars.rrv.ru', user: 'w3dev-odpady', roles: %w(app db web), port: 2221
set :deploy_to, '/www/odpady.ars.vision'

# Puma setup
set :puma_bind, 'tcp://0.0.0.0:8110'
