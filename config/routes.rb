Rails.application.routes.draw do
  # Static pages
  get  '/regulamin1', to: 'static_pages#regulamin1'
  get  '/regulamin2', to: 'static_pages#regulamin2'
  get  '/reklama', to: 'static_pages#reklama'
  get  '/dla_prasy', to: 'static_pages#dla_prasy'

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: {
    sessions: 'customized_devise/sessions',
    registrations: 'customized_devise/registrations'
  }

  namespace :admin do
    root 'forum/sections#index'
    namespace :forum do
      resources :sections, except: :show
      resources :threads, except: [:new, :create] do
        get :close, on: :member
      end
      resources :posts, except: :show
    end
    namespace :newsletter do
      resources :subscribers, except: :show
      resources :categories, except: :show
    end
    resources :user_categories, except: :show
    resources :users, except: [:new, :create, :show]
    resources :states, except: :show
    resources :branches, except: :show
    resources :events, except: :show
    resources :articles, except: :show
    resources :companies, only: [:index, :edit, :update]
    resources :departments, except: :show
    resources :publications, except: :show
    resources :definitions, except: :show
    resources :calendars, except: :show
    resources :government_plans, except: :show
    resources :banners, except: :show
    resources :links, except: :show
    resources :useful_links, except: :show
    resources :product_payments, except: :show
    resources :email_templates, only: [:index, :edit, :update]
    resources :removal_categories, except: :show do
      resources :removals, except: :show
    end
    resources :trees, except: :show
    resources :coefficients, except: :show

    #
    # Info point
    #
    resources :manual_categories, except: :show do
      resources :manuals, except: :show
    end
    resources :document_categories, except: :show do
      resources :documents, except: :show
    end
    resources :wastes, except: :show
    resources :recycling_categories, except: :show do
      resources :recyclings, except: :show
    end
    resources :environment_water_categories, except: :show do
      resources :environment_waters, except: :show
    end
    resources :gaz_categories, except: :show do
      resources :gazs, except: :show
    end
    resources :water_categories, except: :show do
      resources :waters, except: :show
    end
    resources :gasoline_categories, except: :show do
      resources :gasolines, except: :show
    end
    resources :sng_categories, except: :show do
      resources :sngs, except: :show
    end
    resources :diesel_categories, except: :show do
      resources :diesels, except: :show
    end

    #
    # /Info point
    #

    resources :laws, except: :show
    resources :project_categories, except: :show do
      resources :projects, except: :show
    end
    resources :interpretation_categories, except: :show do
      resources :interpretations, except: :show
    end
    resources :union_document_categories, except: :show do
      resources :union_documents, except: :show
    end
  end

  resources :banners, only: :show
  resources :search, only: [:index]
  resources :articles, only: [:index, :show]
  resources :companies, only: [:index, :show, :new, :create]
  resources :departments, only: :index
  resources :definitions, only: :index
  resources :events, only: [:index, :show]

  #
  # Info point
  #
  namespace :manuals do
    resources :categories, only: [:index, :show]
  end
  resources :manuals, only: :show
  resources :documents, only: :index
  resources :publications, only: :index
  resources :calendars, only: [:index, :show]
  resources :government_plans, only: [:index, :show]
  resources :wastes, only: :index
  resources :waste_fees, only: :index
  namespace :recyclings do
    resources :categories, only: :index
  end
  namespace :removals do
    resources :categories, only: :index
  end
  resources :trees, only: [:index, :show]
  post  'trees/calculate'
  namespace :environment_waters do
    resources :categories, only: :index
  end
  namespace :gazs do
    resources :categories, only: :index
  end
  namespace :waters do
    resources :categories, only: :index
  end
  namespace :gasolines do
    resources :categories, only: :index
  end
  namespace :sngs do
    resources :categories, only: :index
  end
  namespace :diesels do
    resources :categories, only: :index
  end
  resources :useful_links, only: :index

  #
  # /Info point
  #

  #
  # Laws
  #
  resources :laws, only: :index

  namespace :union_documents do
    resources :categories, only: [:index, :show]
  end

  namespace :projects do
    resources :categories, only: [:index, :show]
  end
  resources :projects, only: :show


  namespace :interpretations do
    resources :categories, only: [:index, :show]
  end
  resources :interpretations, only: :show
  #
  # /Laws
  #

  resources :project_categories, only: :index do
    resources :projects, only: [:index, :show]
  end

  namespace :forum do
    root 'sections#index'
    resources :threads, only: [:new, :create]
    resources :sections, only: [:index, :show] do
      resources :threads, only: [:index]
    end
    resources :threads, only: [] do
      resources :posts, only: [:index, :create]
    end
  end

  namespace :newsletter do
    resources :subscribers, only: [:create]
  end

  root 'welcome#index'
end
