SimpleForm.setup do |config|
  config.wrappers :login_form, tag: 'div', class: 'login__input', error_class: 'validate-error' do |b|
    b.use :html5
    b.use :label_text, wrap_with: { tag: 'span' }

    b.use :input
    b.use :error, wrap_with: { tag: 'span', class: 'error-span' }
    b.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
  end

  config.wrappers :login_checkbox, tag: 'div', class: 'login__checkbox clearfix', error_class: 'validate-error' do |b|
    b.use :html5
    b.use :input, class: 'checkbox-custom', label: false
    b.use :label, class: 'checkbox-custom-label'
    b.use :error, wrap_with: { tag: 'span', class: 'error-span' }
  end

  config.wrappers :newsletter_form, tag: 'div', class: 'newsletter-modal__input ', error_class: 'validate-error' do |b|
    b.use :html5
    b.use :label_text, wrap_with: { tag: 'span' }

    b.use :input
    b.use :error, wrap_with: { tag: 'span', class: 'error-span' }
    b.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
  end
end
