# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

def create_user_categories
  return if User::Category.any?
  User::Category.create!(name: 'Ogólna')
end

def create_admins
  return if User.any?
  User.create!(
    username: 'Administartor',
    email: 'admin@odpady-help.pl',
    password: 'qwerty123',
    category: User::Category.first,
    admin: true,
    approved: true
  )
end

def create_states
  return if State.any?
  filename = Rails.root.join('db/seeds/states.yml')
  YAML.load_file(filename)['states'].each do |data|
    State.create!(name: data['name'])
  end
end

def create_branches
  return if Branch.any?
  filename = Rails.root.join('db/seeds/branches.yml')
  YAML.load_file(filename)['branches'].each do |data|
    Branch.create!(name: data['name'])
  end
end

def create_events
  return if Event.any?
  filename = Rails.root.join('db/seeds/events.yml')
  YAML.load_file(filename)['events'].each do |data|
    Event.create!(
      name: data['name'],
      body: data['body'],
      start_at: data['start_at'],
      image: File.open(Rails.root.join('db/seeds/events', data['image'])),
    )
  end
end

def create_companies
  100.times do |n|
    company = Company.first_or_create!(
      state_id: State.order('RANDOM()').first.id,
      name: Faker::Company.name,
      logo: File.open(Rails.root.join("db/seeds/companies/#{Faker::Number.between(1, 4)}.jpg")),
      homepage: Faker::Internet.url,
      email: Faker::Internet.email,
      zipcode: Faker::Address.zip_code,
      city: Faker::Address.city,
      street: Faker::Address.street_name,
      phone: Faker::PhoneNumber.phone_number,
      cellphone: Faker::PhoneNumber.cell_phone,
      fax: Faker::PhoneNumber.phone_number,
      contact_person: Faker::Name.name,
      business_profile: Faker::Company.catch_phrase,
      active: Faker::Boolean.boolean
    )
    Faker::Number.between(1, 3).times do |m|
      branch = Branch.order('RANDOM()').first
      company.branches << branch unless company.branches.include?(branch)
    end
  end
end

def create_departments
  100.times do |n|
    Department.create!(
      state_id: State.order('RANDOM()').first.id,
      name: Faker::Company.name,
      homepage: Faker::Internet.url,
      email: Faker::Internet.email,
      zipcode: Faker::Address.zip_code,
      city: Faker::Address.city,
      adress: Faker::Address.street_address,
      phone: Faker::PhoneNumber.phone_number,
      fax: Faker::PhoneNumber.phone_number,
    )
  end
end

def create_calendars
  return if Calendar.any?
  filename = Rails.root.join('db/seeds/calendars.yml')
  YAML.load_file(filename)['calendars'].each do |data|
    Calendar.create!(
      date: data['date'],
      concerned_to: data['concerned_to'],
      todo: data['todo'],
      body: data['body']
    )
  end
end

def create_manual_categories
  return if Manual::Category.any?
  filename = Rails.root.join('db/seeds/manual_categories.yml')
  YAML.load_file(filename)['manual_categories'].each do |data|
    Manual::Category.create!(name: data['name'])
  end
end

def create_document_categories
  return if Document::Category.any?
  filename = Rails.root.join('db/seeds/document_categories.yml')
  YAML.load_file(filename)['document_categories'].each do |data|
    Document::Category.create!(name: data['name'])
  end
end

def create_project_categories
  return if Project::Category.any?
  filename = Rails.root.join('db/seeds/project_categories.yml')
  YAML.load_file(filename)['project_categories'].each do |data|
    Project::Category.create!(name: data['name'])
  end
end

def create_interpretation_categories
  return if Interpretation::Category.any?
  filename = Rails.root.join('db/seeds/interpretation_categories.yml')
  YAML.load_file(filename)['interpretation_categories'].each do |data|
    Interpretation::Category.create!(name: data['name'])
  end
end

def create_union_document_categories
  return if UnionDocument::Category.any?
  filename = Rails.root.join('db/seeds/union_document_categories.yml')
  YAML.load_file(filename)['union_document_categories'].each do |data|
    UnionDocument::Category.create!(name: data['name'])
  end
end

def create_threads
  return if Forum::Thread.any?

  section = Forum::Section.create!(name: 'Dyskusja Ogólna')

  filename = Rails.root.join('db/seeds/threads.yml')
  YAML.load_file(filename)['threads'].each do |data|
    thread = Forum::Thread.create!(subject: data['subject'], section: section, user: User.first)

    rand(1..9).times do |_|
      Forum::Post.create!(message: Faker::Lorem.paragraph, thread: thread, user: User.order("RANDOM()").first)
    end
  end
end

def create_definitions
  return if Definition.any?
  filename = Rails.root.join('db/seeds/definitions.yml')
  YAML.load_file(filename)['definitions'].each do |data|
    Definition.create!(data)
  end
end

def create_recycling_categories
  return if Recycling::Category.any?
  filename = Rails.root.join('db/seeds/recycling_categories.yml')
  YAML.load_file(filename)['recycling_categories'].each do |data|
    Recycling::Category.create!(data)
  end
end

def create_removal_categories
  return if Removal::Category.any?
  filename = Rails.root.join('db/seeds/removal_categories.yml')
  YAML.load_file(filename)['removal_categories'].each do |data|
    Removal::Category.create!(
      name: data['name'],
      yearname: data['yearname']
    )
  end
end

def create_environment_water_categories
  return if EnvironmentWater::Category.any?
  filename = Rails.root.join('db/seeds/environment_water_categories.yml')
  YAML.load_file(filename)['environment_water_categories'].each do |data|
    EnvironmentWater::Category.create!(
      name: data['name'],
      yearname: data['yearname']
    )
  end
end

def create_gaz_categories
  return if Gaz::Category.any?
  filename = Rails.root.join('db/seeds/gaz_categories.yml')
  YAML.load_file(filename)['gaz_categories'].each do |data|
    Gaz::Category.create!(
      name: data['name'],
      yearname: data['yearname'],
      subname: data['subname']
    )
  end
end

def create_water_categories
  return if Water::Category.any?
  filename = Rails.root.join('db/seeds/water_categories.yml')
  YAML.load_file(filename)['water_categories'].each do |data|
    Water::Category.create!(
      name: data['name'],
      yearname: data['yearname']
    )
  end
end

def create_gasoline_categories
  return if Gasoline::Category.any?
  filename = Rails.root.join('db/seeds/gasoline_categories.yml')
  YAML.load_file(filename)['gasoline_categories'].each do |data|
    Gasoline::Category.create!(
      name: data['name'],
      yearname: data['yearname']
    )
  end
end

def create_trees
  return if Tree.any?
  filename = Rails.root.join('db/seeds/trees.yml')
  YAML.load_file(filename)['trees'].each do |data|
    Tree.create!(data)
  end
end

def create_coefficients
  return if Coefficient.any?
  filename = Rails.root.join('db/seeds/coefficients.yml')
  YAML.load_file(filename)['coefficients'].each do |data|
    Coefficient.create!(data)
  end
end

def create_sng_categories
  return if Sng::Category.any?
  filename = Rails.root.join('db/seeds/sng_categories.yml')
  YAML.load_file(filename)['sng_categories'].each do |data|
    Sng::Category.create!(
      name: data['name'],
      yearname: data['yearname']
    )
  end
end

def create_diesel_categories
  return if Diesel::Category.any?
  filename = Rails.root.join('db/seeds/diesel_categories.yml')
  YAML.load_file(filename)['diesel_categories'].each do |data|
    Diesel::Category.create!(
      name: data['name'],
      yearname: data['yearname']
    )
  end
end

def create_waste_fees
  return if WasteFee.any?
  filename = Rails.root.join('db/seeds/waste_fees.yml')
  YAML.load_file(filename).each do |data|
    WasteFee.create waste_id: data["id"], year: data["year"], value: data["value"]
  end
end

def create_email_templates
  filename = Rails.root.join('db/seeds/email_templates.yml')
  YAML.load_file(filename)['email_templates'].each do |data|
    EmailTemplate.where(name: data['name']).first_or_create!(data)
  end
end

create_user_categories
create_admins
create_states
create_branches
create_events
create_companies
create_departments
create_calendars
create_manual_categories
create_document_categories
create_project_categories
create_interpretation_categories
create_union_document_categories
create_threads
create_definitions
create_recycling_categories
create_removal_categories
create_trees
create_coefficients
create_environment_water_categories
create_gaz_categories
create_water_categories
create_gasoline_categories
create_sng_categories
create_diesel_categories
create_email_templates
