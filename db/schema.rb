# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161019082156) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "article_recommendations", force: :cascade do |t|
    t.integer  "article_id"
    t.integer  "recommendation_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "article_recommendations", ["article_id"], name: "index_article_recommendations_on_article_id", using: :btree
  add_index "article_recommendations", ["recommendation_id"], name: "index_article_recommendations_on_recommendation_id", using: :btree

  create_table "articles", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.string   "image"
    t.boolean  "published",  default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.datetime "date"
    t.text     "preview"
  end

  add_index "articles", ["published"], name: "index_articles_on_published", using: :btree

  create_table "banner_clicks", force: :cascade do |t|
    t.integer  "banner_id"
    t.datetime "date"
    t.integer  "count",      default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "banner_clicks", ["banner_id", "date"], name: "index_banner_clicks_on_banner_id_and_date", unique: true, using: :btree
  add_index "banner_clicks", ["banner_id"], name: "index_banner_clicks_on_banner_id", using: :btree
  add_index "banner_clicks", ["date"], name: "index_banner_clicks_on_date", using: :btree

  create_table "banner_views", force: :cascade do |t|
    t.integer  "banner_id"
    t.datetime "date"
    t.integer  "count",      default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "banner_views", ["banner_id", "date"], name: "index_banner_views_on_banner_id_and_date", unique: true, using: :btree
  add_index "banner_views", ["banner_id"], name: "index_banner_views_on_banner_id", using: :btree
  add_index "banner_views", ["date"], name: "index_banner_views_on_date", using: :btree

  create_table "banners", force: :cascade do |t|
    t.string   "token"
    t.text     "description"
    t.string   "image"
    t.string   "link"
    t.boolean  "top",          default: false
    t.boolean  "sidebar",      default: false
    t.integer  "views_count",  default: 0
    t.integer  "clicks_count", default: 0
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "banners", ["clicks_count"], name: "index_banners_on_clicks_count", using: :btree
  add_index "banners", ["token"], name: "index_banners_on_token", unique: true, using: :btree
  add_index "banners", ["views_count"], name: "index_banners_on_views_count", using: :btree

  create_table "branches", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "branches", ["name"], name: "index_branches_on_name", unique: true, using: :btree
  add_index "branches", ["position"], name: "index_branches_on_position", using: :btree

  create_table "branches_companies", id: false, force: :cascade do |t|
    t.integer "company_id"
    t.integer "branch_id"
  end

  add_index "branches_companies", ["branch_id"], name: "index_branches_companies_on_branch_id", using: :btree
  add_index "branches_companies", ["company_id", "branch_id"], name: "index_branches_companies_on_company_id_and_branch_id", unique: true, using: :btree
  add_index "branches_companies", ["company_id"], name: "index_branches_companies_on_company_id", using: :btree

  create_table "calendars", force: :cascade do |t|
    t.date     "date"
    t.text     "concerned_to"
    t.text     "todo"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.text     "body"
  end

  add_index "calendars", ["date"], name: "index_calendars_on_date", using: :btree

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "coefficients", force: :cascade do |t|
    t.integer  "year"
    t.float    "float"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "logo"
    t.string   "homepage"
    t.string   "email"
    t.string   "zipcode"
    t.string   "city"
    t.string   "street"
    t.integer  "state_id"
    t.string   "phone"
    t.string   "cellphone"
    t.string   "fax"
    t.string   "contact_person"
    t.text     "business_profile"
    t.boolean  "active",           default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "full",             default: false
  end

  add_index "companies", ["active"], name: "index_companies_on_active", using: :btree
  add_index "companies", ["name"], name: "index_companies_on_name", unique: true, using: :btree
  add_index "companies", ["state_id"], name: "index_companies_on_state_id", using: :btree

  create_table "definitions", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "source"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "definitions", ["name"], name: "index_definitions_on_name", using: :btree

  create_table "departments", force: :cascade do |t|
    t.string   "name"
    t.string   "homepage"
    t.string   "email"
    t.string   "zipcode"
    t.string   "city"
    t.string   "adress"
    t.integer  "state_id"
    t.string   "phone"
    t.string   "fax"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "departments", ["name"], name: "index_departments_on_name", using: :btree
  add_index "departments", ["state_id"], name: "index_departments_on_state_id", using: :btree

  create_table "diesel_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "yearname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "diesels", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.float    "value"
    t.boolean  "on"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "diesels", ["category_id"], name: "index_diesels_on_category_id", using: :btree

  create_table "document_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string   "name"
    t.string   "doc"
    t.string   "pdf"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "documents", ["category_id"], name: "index_documents_on_category_id", using: :btree

  create_table "email_templates", force: :cascade do |t|
    t.string   "name"
    t.string   "subject"
    t.string   "liquid_subject"
    t.text     "body"
    t.text     "liquid_body"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "email_templates", ["name"], name: "index_email_templates_on_name", unique: true, using: :btree

  create_table "environment_water_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "yearname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "environment_waters", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.float    "sum"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "environment_waters", ["category_id"], name: "index_environment_waters_on_category_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.datetime "start_at"
    t.string   "image"
    t.boolean  "published",  default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "events", ["published"], name: "index_events_on_published", using: :btree
  add_index "events", ["start_at"], name: "index_events_on_start_at", using: :btree

  create_table "forum_posts", force: :cascade do |t|
    t.integer  "thread_id"
    t.integer  "user_id"
    t.string   "username"
    t.text     "message"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "approved",   default: false
    t.string   "meta_id"
  end

  add_index "forum_posts", ["meta_id"], name: "index_forum_posts_on_meta_id", using: :btree
  add_index "forum_posts", ["thread_id"], name: "index_forum_posts_on_thread_id", using: :btree
  add_index "forum_posts", ["user_id"], name: "index_forum_posts_on_user_id", using: :btree

  create_table "forum_sections", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.boolean  "enabled",    default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "meta_id"
  end

  add_index "forum_sections", ["meta_id"], name: "index_forum_sections_on_meta_id", using: :btree
  add_index "forum_sections", ["position"], name: "index_forum_sections_on_position", using: :btree

  create_table "forum_threads", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "section_id"
    t.string   "username"
    t.string   "subject"
    t.boolean  "notify_me",   default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "approved",    default: false
    t.boolean  "closed",      default: false
    t.datetime "closed_date"
    t.string   "meta_id"
    t.integer  "views",       default: 0
  end

  add_index "forum_threads", ["meta_id"], name: "index_forum_threads_on_meta_id", using: :btree
  add_index "forum_threads", ["section_id"], name: "index_forum_threads_on_section_id", using: :btree
  add_index "forum_threads", ["user_id"], name: "index_forum_threads_on_user_id", using: :btree
  add_index "forum_threads", ["views"], name: "index_forum_threads_on_views", using: :btree

  create_table "gasoline_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "yearname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gasolines", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.float    "value"
    t.boolean  "bs"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "gasolines", ["category_id"], name: "index_gasolines_on_category_id", using: :btree

  create_table "gaz_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "yearname"
    t.string   "subname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gazs", force: :cascade do |t|
    t.string   "name"
    t.string   "power"
    t.integer  "year"
    t.string   "value"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "gazs", ["category_id"], name: "index_gazs_on_category_id", using: :btree

  create_table "government_plans", force: :cascade do |t|
    t.integer  "state_id"
    t.text     "body"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "government_plans", ["state_id"], name: "index_government_plans_on_state_id", using: :btree

  create_table "interpretation_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "interpretations", force: :cascade do |t|
    t.text     "body"
    t.string   "pdf"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "interpretations", ["category_id"], name: "index_interpretations_on_category_id", using: :btree

  create_table "law_on_mains", force: :cascade do |t|
    t.integer  "itemable_id"
    t.string   "itemable_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "icon_id"
  end

  add_index "law_on_mains", ["itemable_type", "itemable_id"], name: "index_law_on_mains_on_itemable_type_and_itemable_id", using: :btree

  create_table "laws", force: :cascade do |t|
    t.string   "address"
    t.string   "title"
    t.string   "pdf"
    t.string   "isap"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "links", force: :cascade do |t|
    t.string   "label"
    t.string   "title"
    t.string   "url"
    t.string   "image"
    t.integer  "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "manual_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "manuals", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "body"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "manuals", ["category_id"], name: "index_manuals_on_category_id", using: :btree

  create_table "newsletter_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "newsletter_categories_subscribers", id: false, force: :cascade do |t|
    t.integer "category_id"
    t.integer "subscriber_id"
  end

  add_index "newsletter_categories_subscribers", ["category_id"], name: "index_newsletter_categories_subscribers_on_category_id", using: :btree
  add_index "newsletter_categories_subscribers", ["subscriber_id"], name: "index_newsletter_categories_subscribers_on_subscriber_id", using: :btree

  create_table "newsletter_subscribers", force: :cascade do |t|
    t.string   "email"
    t.boolean  "enabled",    default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "position"
    t.integer  "state_id"
    t.integer  "branch_id"
  end

  add_index "newsletter_subscribers", ["branch_id"], name: "index_newsletter_subscribers_on_branch_id", using: :btree
  add_index "newsletter_subscribers", ["email"], name: "index_newsletter_subscribers_on_email", unique: true, using: :btree
  add_index "newsletter_subscribers", ["enabled"], name: "index_newsletter_subscribers_on_enabled", using: :btree
  add_index "newsletter_subscribers", ["state_id"], name: "index_newsletter_subscribers_on_state_id", using: :btree

  create_table "project_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.string   "pdf"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "projects", ["category_id"], name: "index_projects_on_category_id", using: :btree

  create_table "publications", force: :cascade do |t|
    t.string   "name"
    t.string   "author"
    t.string   "pdf"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "law"
  end

  add_index "publications", ["name"], name: "index_publications_on_name", using: :btree

  create_table "recycling_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recyclings", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.string   "value"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "recyclings", ["category_id"], name: "index_recyclings_on_category_id", using: :btree

  create_table "removal_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "yearname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "removals", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.float    "value"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "removals", ["category_id"], name: "index_removals_on_category_id", using: :btree

  create_table "sng_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "yearname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sngs", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.float    "value"
    t.boolean  "sng1"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "sngs", ["category_id"], name: "index_sngs_on_category_id", using: :btree

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "states", ["name"], name: "index_states_on_name", unique: true, using: :btree
  add_index "states", ["position"], name: "index_states_on_position", using: :btree

  create_table "trees", force: :cascade do |t|
    t.string   "name"
    t.float    "base"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "union_document_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "union_documents", force: :cascade do |t|
    t.text     "title"
    t.string   "address"
    t.string   "pdf"
    t.integer  "category_id"
    t.string   "eur_lex"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "union_documents", ["category_id"], name: "index_union_documents_on_category_id", using: :btree

  create_table "useful_links", force: :cascade do |t|
    t.text     "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.string   "username"
    t.boolean  "approved",               default: false, null: false
    t.string   "meta_id"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "category_id"
  end

  add_index "users", ["admin"], name: "index_users_on_admin", using: :btree
  add_index "users", ["approved"], name: "index_users_on_approved", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["meta_id"], name: "index_users_on_meta_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "waste_fees", force: :cascade do |t|
    t.integer  "year"
    t.float    "value"
    t.integer  "waste_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "waste_fees", ["waste_id"], name: "index_waste_fees_on_waste_id", using: :btree

  create_table "wastes", force: :cascade do |t|
    t.integer  "parent_id"
    t.string   "code"
    t.string   "name"
    t.boolean  "dry_mass"
    t.boolean  "disposal"
    t.boolean  "recycling"
    t.boolean  "org_fiz"
    t.boolean  "battery"
    t.boolean  "dangerous"
    t.boolean  "communal"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "water_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "yearname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "waters", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.float    "value"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "waters", ["category_id"], name: "index_waters_on_category_id", using: :btree

  add_foreign_key "article_recommendations", "articles"
  add_foreign_key "banner_clicks", "banners"
  add_foreign_key "banner_views", "banners"
  add_foreign_key "branches_companies", "branches"
  add_foreign_key "branches_companies", "companies"
  add_foreign_key "companies", "states"
  add_foreign_key "departments", "states"
  add_foreign_key "diesels", "diesel_categories", column: "category_id"
  add_foreign_key "documents", "document_categories", column: "category_id"
  add_foreign_key "environment_waters", "environment_water_categories", column: "category_id"
  add_foreign_key "forum_posts", "forum_threads", column: "thread_id"
  add_foreign_key "forum_posts", "users"
  add_foreign_key "forum_threads", "forum_sections", column: "section_id"
  add_foreign_key "forum_threads", "users"
  add_foreign_key "gasolines", "gasoline_categories", column: "category_id"
  add_foreign_key "gazs", "gaz_categories", column: "category_id"
  add_foreign_key "government_plans", "states"
  add_foreign_key "interpretations", "interpretation_categories", column: "category_id"
  add_foreign_key "manuals", "manual_categories", column: "category_id"
  add_foreign_key "projects", "project_categories", column: "category_id"
  add_foreign_key "recyclings", "recycling_categories", column: "category_id"
  add_foreign_key "removals", "removal_categories", column: "category_id"
  add_foreign_key "sngs", "sng_categories", column: "category_id"
  add_foreign_key "union_documents", "union_document_categories", column: "category_id"
  add_foreign_key "users", "user_categories", column: "category_id"
  add_foreign_key "waste_fees", "wastes"
  add_foreign_key "waters", "water_categories", column: "category_id"
end
