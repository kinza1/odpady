class AddApprovedToForumPost < ActiveRecord::Migration
  def change
    add_column :forum_posts, :approved, :boolean, default: false
  end
end
