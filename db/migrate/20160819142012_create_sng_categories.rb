class CreateSngCategories < ActiveRecord::Migration
  def change
    create_table :sng_categories do |t|
      t.string :name
      t.string :yearname

      t.timestamps null: false
    end
  end
end
