class CreateRemovals < ActiveRecord::Migration
  def change
    create_table :removals do |t|
      t.string :name
      t.integer :year
      t.float :value
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :removals, :removal_categories, column: :category_id
  end
end
