class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :token
      t.text :description
      t.string :image
      t.string :link
      t.boolean :top, default: false
      t.boolean :sidebar, default: false
      t.integer :views_count, default: 0
      t.integer :clicks_count, default: 0

      t.timestamps null: false
    end
    add_index :banners, :views_count
    add_index :banners, :clicks_count
    add_index :banners, :token, unique: true
  end
end
