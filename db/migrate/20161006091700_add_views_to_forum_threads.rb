class AddViewsToForumThreads < ActiveRecord::Migration
  def change
    add_column :forum_threads, :views, :integer, default: 0
    add_index :forum_threads, :views
  end
end
