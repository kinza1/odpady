class SetDefaultDateToArticles < ActiveRecord::Migration
  def up
    date = Time.current.midday.to_s(:db)
    execute("UPDATE articles SET date = '#{date}'")
  end

  def down
  end
end
