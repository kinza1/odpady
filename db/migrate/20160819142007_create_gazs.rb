class CreateGazs < ActiveRecord::Migration
  def change
    create_table :gazs do |t|
      t.string :name
      t.string :power
      t.integer :year
      t.string :value
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :gazs, :gaz_categories, column: :category_id
  end
end
