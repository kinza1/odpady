class CreateGasolineCategories < ActiveRecord::Migration
  def change
    create_table :gasoline_categories do |t|
      t.string :name
      t.string :yearname

      t.timestamps null: false
    end
  end
end
