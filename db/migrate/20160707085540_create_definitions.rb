class CreateDefinitions < ActiveRecord::Migration
  def change
    create_table :definitions do |t|
      t.string :name
      t.text :description
      t.date :start_at
      t.string :source

      t.timestamps null: false
    end
    add_index :definitions, :name
  end
end
