class ChangeNewsletterSubscriptionFields < ActiveRecord::Migration
  def change
    remove_reference :newsletter_subscribers, :newsletter_category, index: true
  end
end
