class CreateUnionDocuments < ActiveRecord::Migration
  def change
    create_table :union_documents do |t|
      t.text :name
      t.string :address
      t.string :pdf
      t.references :category, index: true
      t.string :eur_lex

      t.timestamps null: false
    end
    add_foreign_key :union_documents, :union_document_categories, column: :category_id
  end
end
