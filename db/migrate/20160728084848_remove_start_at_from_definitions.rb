class RemoveStartAtFromDefinitions < ActiveRecord::Migration
  def change
    remove_column :definitions, :start_at, :datetime
  end
end
