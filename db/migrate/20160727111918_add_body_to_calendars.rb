class AddBodyToCalendars < ActiveRecord::Migration
  def change
    add_column :calendars, :body, :text
  end
end
