class CreateArticleRecommendations < ActiveRecord::Migration
  def change
    create_table :article_recommendations do |t|
      t.references :article, index: true, foreign_key: true
      t.references :recommendation, index: true

      t.timestamps null: false
    end
  end
end
