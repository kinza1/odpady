class CreateManuals < ActiveRecord::Migration
  def change
    create_table :manuals do |t|
      t.string :name
      t.text :description
      t.text :body
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :manuals, :manual_categories, column: :category_id
  end
end
