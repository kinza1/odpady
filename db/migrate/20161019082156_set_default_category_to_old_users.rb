class SetDefaultCategoryToOldUsers < ActiveRecord::Migration
  def up
    category = User::Category.order(id: :asc).first
    User.find_each do |user|
      user.update!(category: category)
    end
  end

  def down
  end
end
