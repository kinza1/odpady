class AddLawToPublications < ActiveRecord::Migration
  def change
    add_column :publications, :law, :string
  end
end
