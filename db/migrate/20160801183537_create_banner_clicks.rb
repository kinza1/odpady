class CreateBannerClicks < ActiveRecord::Migration
  def change
    create_table :banner_clicks do |t|
      t.references :banner, index: true, foreign_key: true
      t.datetime :date
      t.integer :count, default: 0

      t.timestamps null: false
    end
    add_index :banner_clicks, :date
    add_index :banner_clicks, [:banner_id, :date], unique: true
  end
end
