class AddFullToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :full, :boolean, default: false
  end
end
