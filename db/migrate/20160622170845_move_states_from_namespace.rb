class MoveStatesFromNamespace < ActiveRecord::Migration
  def change
    rename_table :newsletter_states, :states
  end
end
