class CreateRecyclings < ActiveRecord::Migration
  def change
    create_table :recyclings do |t|
      t.string :name
      t.integer :year
      t.string :value
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :recyclings, :recycling_categories, column: :category_id
  end
end
