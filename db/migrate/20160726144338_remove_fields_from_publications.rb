class RemoveFieldsFromPublications < ActiveRecord::Migration
  def change
    remove_column :publications, :start_at, :datetime
    remove_column :publications, :modified_at, :datetime
  end
end
