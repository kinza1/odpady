class CreateCalendars < ActiveRecord::Migration
  def change
    create_table :calendars do |t|
      t.date :date
      t.text :concerned_to
      t.text :todo
      t.string :form_pdf
      t.text :source
      t.string :isap

      t.timestamps null: false
    end
    add_index :calendars, :date
  end
end
