class CreateRecyclingCategories < ActiveRecord::Migration
  def change
    create_table :recycling_categories do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
