class AddMetaIdToForumModels < ActiveRecord::Migration
  def change
    add_column :forum_sections, :meta_id, :string
    add_index :forum_sections, :meta_id

    add_column :forum_threads, :meta_id, :string
    add_index :forum_threads, :meta_id

    add_column :forum_posts, :meta_id, :string
    add_index :forum_posts, :meta_id
  end
end
