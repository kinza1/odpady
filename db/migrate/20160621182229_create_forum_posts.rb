class CreateForumPosts < ActiveRecord::Migration
  def change
    create_table :forum_posts do |t|
      t.references :thread, index: true
      t.references :user, index: true, foreign_key: true
      t.string :username
      t.text :message

      t.timestamps null: false
    end
    add_foreign_key :forum_posts, :forum_threads, column: :thread_id
  end
end
