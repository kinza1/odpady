class CreateDiesels < ActiveRecord::Migration
  def change
    create_table :diesels do |t|
      t.string :name
      t.integer :year
      t.float :value
      t.boolean :on
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :diesels, :diesel_categories, column: :category_id
  end
end
