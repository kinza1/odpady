class CreateEnvironmentWaters < ActiveRecord::Migration
  def change
    create_table :environment_waters do |t|
      t.string :name
      t.integer :year
      t.float :sum
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :environment_waters, :environment_water_categories, column: :category_id
  end
end
