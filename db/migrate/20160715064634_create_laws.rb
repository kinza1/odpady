class CreateLaws < ActiveRecord::Migration
  def change
    create_table :laws do |t|
      t.string :address
      t.string :title
      t.string :pdf
      t.string :isap

      t.timestamps null: false
    end
  end
end
