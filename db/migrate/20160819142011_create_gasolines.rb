class CreateGasolines < ActiveRecord::Migration
  def change
    create_table :gasolines do |t|
      t.string :name
      t.integer :year
      t.float :value
      t.boolean :bs
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :gasolines, :gasoline_categories, column: :category_id
  end
end
