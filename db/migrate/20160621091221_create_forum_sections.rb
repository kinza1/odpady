class CreateForumSections < ActiveRecord::Migration
  def change
    create_table :forum_sections do |t|
      t.string :name
      t.integer :position
      t.boolean :enabled, default: true

      t.timestamps null: false
    end
    add_index :forum_sections, :position
  end
end
