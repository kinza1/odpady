class RemoveOnMainFromLaw < ActiveRecord::Migration
  def change
    remove_column :laws, :on_main, :boolean
  end
end
