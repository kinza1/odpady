class AddApprovedToForumThreads < ActiveRecord::Migration
  def change
    add_column :forum_threads, :approved, :boolean, default: false
  end
end
