class CreateSngs < ActiveRecord::Migration
  def change
    create_table :sngs do |t|
      t.string :name
      t.integer :year
      t.float :value
      t.boolean :sng1
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :sngs, :sng_categories, column: :category_id
  end
end
