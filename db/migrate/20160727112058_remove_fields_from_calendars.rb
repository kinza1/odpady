class RemoveFieldsFromCalendars < ActiveRecord::Migration
  def change
    remove_column :calendars, :form_pdf, :string
    remove_column :calendars, :source, :text
    remove_column :calendars, :isap, :string
  end
end
