class CreateInterpretations < ActiveRecord::Migration
  def change
    create_table :interpretations do |t|
      t.text :body
      t.string :pdf
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :interpretations, :interpretation_categories, column: :category_id
  end
end
