class AddFieldstToNewsletter < ActiveRecord::Migration
  def change
    add_column :newsletter_subscribers, :position, :string
    add_reference :newsletter_subscribers, :state, index: true
    add_reference :newsletter_subscribers, :branch, index: true
    add_reference :newsletter_subscribers, :newsletter_category, index: true
  end
end
