class CreateBranches < ActiveRecord::Migration
  def change
    create_table :branches do |t|
      t.string :name
      t.integer :position

      t.timestamps null: false
    end
    add_index :branches, :name, unique: true
    add_index :branches, :position
  end
end
