class CreateTrees < ActiveRecord::Migration
  def change
    create_table :trees do |t|
      t.string :name
      t.float :base

      t.timestamps null: false
    end
  end
end
