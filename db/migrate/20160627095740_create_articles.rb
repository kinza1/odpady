class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :name
      t.text :body
      t.string :image
      t.boolean :published, default: false

      t.timestamps null: false
    end
    add_index :articles, :published
  end
end
