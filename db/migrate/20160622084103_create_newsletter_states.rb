class CreateNewsletterStates < ActiveRecord::Migration
  def change
    create_table :newsletter_states do |t|
      t.string :name
      t.integer :position

      t.timestamps null: false
    end
    add_index :newsletter_states, :name, unique: true
    add_index :newsletter_states, :position
  end
end
