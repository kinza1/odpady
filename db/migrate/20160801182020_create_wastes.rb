class CreateWastes < ActiveRecord::Migration
  def change
    create_table :wastes do |t|
      t.integer :parent_id
      t.string :code
      t.string :name
      t.boolean :dry_mass
      t.boolean :disposal
      t.boolean :recycling
      t.boolean :org_fiz
      t.boolean :battery
      t.boolean :dangerous
      t.boolean :communal

      t.timestamps null: false
    end
  end
end
