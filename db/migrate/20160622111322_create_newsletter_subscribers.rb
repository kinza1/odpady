class CreateNewsletterSubscribers < ActiveRecord::Migration
  def change
    create_table :newsletter_subscribers do |t|
      t.string :email
      t.boolean :enabled, default: true

      t.timestamps null: false
    end
    add_index :newsletter_subscribers, :email, unique: true
    add_index :newsletter_subscribers, :enabled
  end
end
