class CreateBranchesCompanies < ActiveRecord::Migration
  def change
    create_table :branches_companies, id: false  do |t|
      t.references :company, index: true, foreign_key: true
      t.references :branch, index: true, foreign_key: true
    end
    add_index :branches_companies, [:company_id, :branch_id], unique: true
  end
end
