class RenamePdfColumnOfPublications < ActiveRecord::Migration
  def change
    rename_column :publications, :publication_pdf, :pdf
  end
end
