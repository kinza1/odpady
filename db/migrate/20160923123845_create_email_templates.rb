class CreateEmailTemplates < ActiveRecord::Migration
  def change
    create_table :email_templates do |t|
      t.string :name
      t.string :subject
      t.string :liquid_subject
      t.text :body
      t.text :liquid_body

      t.timestamps null: false
    end
    add_index :email_templates, :name, unique: true
  end
end
