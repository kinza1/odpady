class AddClosedToForumThreads < ActiveRecord::Migration
  def change
    add_column :forum_threads, :closed, :boolean, default: false
    add_column :forum_threads, :closed_date, :datetime
  end
end
