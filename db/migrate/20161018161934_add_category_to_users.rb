class AddCategoryToUsers < ActiveRecord::Migration
  def change
    add_column :users, :category_id, :integer, index: true
    add_foreign_key :users, :user_categories, column: :category_id
  end
end
