class AddOnMainToLaws < ActiveRecord::Migration
  def change
    add_column :laws, :on_main, :boolean
  end
end
