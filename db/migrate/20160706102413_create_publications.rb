class CreatePublications < ActiveRecord::Migration
  def change
    create_table :publications do |t|
      t.string :name
      t.date :start_at
      t.date :modified_at
      t.string :author
      t.string :publication_pdf

      t.timestamps null: false
    end
    add_index :publications, :name
  end
end
