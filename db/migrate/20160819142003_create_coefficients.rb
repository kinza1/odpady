class CreateCoefficients < ActiveRecord::Migration
  def change
    create_table :coefficients do |t|
      t.integer :year
      t.float :float

      t.timestamps null: false
    end
  end
end
