class CreateDieselCategories < ActiveRecord::Migration
  def change
    create_table :diesel_categories do |t|
      t.string :name
      t.string :yearname

      t.timestamps null: false
    end
  end
end
