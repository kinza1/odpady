class CreateWasteFees < ActiveRecord::Migration
  def change
    create_table :waste_fees do |t|
      t.integer :year
      t.float :value
      t.references :waste, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
