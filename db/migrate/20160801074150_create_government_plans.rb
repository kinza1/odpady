class CreateGovernmentPlans < ActiveRecord::Migration
  def change
    create_table :government_plans do |t|
      t.references :state, index: true, foreign_key: true
      t.text :body
      t.string :image

      t.timestamps null: false
    end
  end
end
