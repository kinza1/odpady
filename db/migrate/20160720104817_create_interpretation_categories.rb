class CreateInterpretationCategories < ActiveRecord::Migration
  def change
    create_table :interpretation_categories do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
