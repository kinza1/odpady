class CreateUnionDocumentCategories < ActiveRecord::Migration
  def change
    create_table :union_document_categories do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
