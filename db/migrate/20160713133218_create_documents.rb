class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :name
      t.string :doc
      t.string :pdf
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :documents, :document_categories, column: :category_id
  end
end
