class CreateForumThreads < ActiveRecord::Migration
  def change
    create_table :forum_threads do |t|
      t.references :user, index: true, foreign_key: true
      t.references :section, index: true
      t.string :username
      t.string :subject
      t.boolean :notify_me, default: false

      t.timestamps null: false
    end
    add_foreign_key :forum_threads, :forum_sections, column: :section_id
  end
end
