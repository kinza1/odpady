class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.text :body
      t.string :pdf
      t.references :category, index: true
      t.timestamps null: false
    end
    add_foreign_key :projects, :project_categories, column: :category_id
  end
end
