class CreateGazCategories < ActiveRecord::Migration
  def change
    create_table :gaz_categories do |t|
      t.string :name
      t.string :yearname
      t.string :subname

      t.timestamps null: false
    end
  end
end
