class CreateNewsletterCategories < ActiveRecord::Migration
  def change
    create_table :newsletter_categories do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
