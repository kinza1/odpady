class RenameTitleOfUnionDocuments < ActiveRecord::Migration
  def change
    rename_column :union_documents, :name, :title
  end
end
