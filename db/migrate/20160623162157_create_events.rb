class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :body
      t.datetime :start_at
      t.string :image
      t.boolean :published, default: false

      t.timestamps null: false
    end
    add_index :events, :start_at
    add_index :events, :published
  end
end
