class AddMetaIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :meta_id, :string
    add_index :users, :meta_id
  end
end
