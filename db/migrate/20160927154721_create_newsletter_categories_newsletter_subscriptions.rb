class CreateNewsletterCategoriesNewsletterSubscriptions < ActiveRecord::Migration
  def change
    create_table :newsletter_categories_subscribers, id: false do |t|
      t.references :category, index: true
      t.references :subscriber, index: true
    end
  end
end
