class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.string :name
      t.string :homepage
      t.string :email
      t.string :zipcode
      t.string :city
      t.string :adress
      t.references :state, index: true, foreign_key: true
      t.string :phone
      t.string :fax

      t.timestamps null: false
    end
    add_index :departments, :name
  end
end
