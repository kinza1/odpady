class CreateDefaultUserCategory < ActiveRecord::Migration
  def up
    User::Category.create!(
      name: 'Ogólna'
    )
  end

  def down
  end
end
