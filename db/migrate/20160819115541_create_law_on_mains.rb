class CreateLawOnMains < ActiveRecord::Migration
  def change
    create_table :law_on_mains do |t|
      t.references :itemable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
