class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :logo
      t.string :homepage
      t.string :email
      t.string :zipcode
      t.string :city
      t.string :street
      t.references :state, index: true, foreign_key: true
      t.string :phone
      t.string :cellphone
      t.string :fax
      t.string :contact_person
      t.text :business_profile
      t.boolean :active, default: false

      t.timestamps null: false
    end
    add_index :companies, :name, unique: true
    add_index :companies, :active
  end
end
