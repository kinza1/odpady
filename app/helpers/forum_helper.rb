module ForumHelper
  def recent_threads
    @recent_threads ||= Forum::Thread.approved.recent
  end

  def thread_color(thread)
    if thread.posts.approved.count < 10
      'bg_blue'
    elsif thread.posts.approved.count < 30
      'bg_orange'
    else
      'bg_red'
    end
  end
end
