module EventsHelper
  def recent_events
    @recent_events ||= Event.soon.order(start_at: :desc).limit(5)
  end
end
