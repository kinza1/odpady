class ThreadCheckService
  attr_reader :thread, :user, :closed

  def initialize(thread, user)
    @thread = thread
    @user = user
    @closed = @thread.closed
  end

  def check(current_closed)
    if current_closed != closed
      set_closed if current_closed
      set_opened unless current_closed
    end
  end

  private

  def set_closed
    thread.posts.create(user: user,
                        username: '',
                        approved: true,
                        message: 'Wątek zostanie zamknięty.
Powód: temat został poruszony w innym wątku.
Zapraszamy do kontynuacji tematu pod linkiem: [link]
Jeżeli nie zgadzasz się z powodem zamknięcia wątku prosimy o skontaktowanie się z Administratorem forum.
Zamknięty wątek zostanie usunięty w ciągu 7 dni.'
    )

    time = thread.closed_date - Time.zone.now
    ThreadCloseWorker.perform_in(time, thread.id)
  end

  def set_opened
    last = thread.posts.order(id: :desc).first
    last.destroy
  end
end
