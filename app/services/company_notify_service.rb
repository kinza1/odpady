class CompanyNotifyService
  attr_reader :company

  def initialize(company)
    @company = company
  end

  def notify
    send_admin_email
    send_user_email if company.email.present?
  end

  def send_admin_email
    email = EmailTemplate.find_by_name!('company_subscription_for_admin')
    email.deliver('news@odpady-help.pl', name: company.name)
  end

  def send_user_email
    email = EmailTemplate.find_by_name!('company_subscription_for_user')
    email.deliver(company.email)
  end
end
