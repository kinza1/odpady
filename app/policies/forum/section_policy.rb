module Forum
  class SectionPolicy < ApplicationPolicy
    def index?
      true
    end

    def show?
      record.enabled?
    end
  end
end
