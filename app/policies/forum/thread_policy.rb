module Forum
  class ThreadPolicy < ApplicationPolicy
    def index?
      true
    end

    def show?
      true
    end

    def new?
      create?
    end

    def create?
      true
    end
  end
end
