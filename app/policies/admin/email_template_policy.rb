module Admin
  class EmailTemplatePolicy < ApplicationPolicy
    def index?
      allowed?
    end

    def edit?
      update?
    end

    def update?
      allowed?
    end

    private

    def allowed?
      user.admin?
    end
  end
end
