module Admin
  module Gaz
    class CategoryPolicy < ApplicationPolicy
      def index?
        allowed?
      end

      def new?
        create?
      end

      def create?
        allowed?
      end

      def edit?
        update?
      end

      def update?
        allowed?
      end

      def destroy?
        allowed?
      end

      private

      def allowed?
        user.admin?
      end
    end
  end
end
