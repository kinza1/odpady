module Newsletter
  class SubscriberPolicy < ApplicationPolicy
    def create?
      true
    end
  end
end
