class DefaultMailer < ApplicationMailer
  default from: 'no-reply@odpady-hepl.pl'

  # Delivers email prepared by EmailTemplate class
  def email(to:, subject:, body:)
    mail to: to,
         subject: subject,
         content_type: 'text/html',
         body: body
  end
end
