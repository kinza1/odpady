class ArticleImageUploader < BaseImageUploader
  version :thumb do
    process resize_to_fill: [100, 100]
  end

  version :medium do
    process resize_to_fill: [260, 200]
  end

  version :large do
    process resize_to_fill: [800, 400]
  end

  version :slider do
    process resize_to_fill: [820, 580]
  end
end
