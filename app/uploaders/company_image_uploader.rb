class CompanyImageUploader < BaseImageUploader
  version :thumb do
    process resize_to_fit: [170, 50]
  end

  version :square do
    process resize_to_fill: [100, 100]
  end

  version :main_page do
    process resize_to_fill: [150, 50]
  end
end
