class EventImageUploader < BaseImageUploader
  version :thumb do
    process resize_to_fill: [100, 100]
  end

  version :medium do
    process resize_to_fill: [200, 200]
  end
end
