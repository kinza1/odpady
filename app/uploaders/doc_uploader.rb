class DOCUploader < BaseUploader
  def extension_white_list
    %w(doc docx)
  end
end
