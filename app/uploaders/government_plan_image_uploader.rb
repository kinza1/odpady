class GovernmentPlanImageUploader < BaseImageUploader
  version :thumb do
    process resize_to_fill: [100, 150]
  end

  version :medium do
    process resize_to_fill: [500, 700]
  end
end
