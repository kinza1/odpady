class BannerUploader < BaseImageUploader
  version :top_thumb do
    process resize_to_fill: [196, 30]
  end

  version :top do
    process resize_to_fill: [980, 150]
  end

  version :sidebar_thumb do
    process resize_to_fill: [80, 56]
  end

  version :sidebar do
    process resize_to_fill: [400, 280]
  end
end
