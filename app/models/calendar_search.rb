require 'searchlight/adapters/action_view'

class CalendarSearch < Searchlight::Search
  PER = 10

  include Searchlight::Adapters::ActionView

  def base_query
    Calendar.order('extract(month from date) ASC').order('extract(day from date) ASC')
            .page(options[:page]).per(25)
  end

  def search_month
    query.where('extract(month from date) = ?', options[:month])
  end
end
