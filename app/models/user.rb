class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :omniauthable,
  # :recoverable
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable

  belongs_to :category, required: true, class_name: 'User::Category'

  def active_for_authentication?
    super && approved?
  end

  validates :username, presence: true, uniqueness: true
end
