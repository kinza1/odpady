require 'searchlight/adapters/action_view'

class UserSearch < Searchlight::Search
  include Searchlight::Adapters::ActionView

  def base_query
    User.order(id: :desc)
        .includes(:category)
        .page(options[:page])
        .per(10)
  end

  def admin_select
    [true, false].map do |option|
      [I18n.t("admin.users.filters.admin.#{option}_s"), option]
    end
  end

  def approved_select
    [true, false].map do |option|
      [I18n.t("admin.users.filters.approved.#{option}_s"), option]
    end
  end

  def search_admin
    query.where(admin: options[:admin])
  end

  def search_approved
    query.where(approved: options[:approved])
  end

  def search_category_id
    query.where(category_id: options[:category_id])
  end

  def search_username
    q = "%#{options[:username].to_s.mb_chars.downcase}%"
    query.where('LOWER(users.username) LIKE ?', q)
  end

  def search_email
    q = "%#{options[:email].to_s.mb_chars.downcase}%"
    query.where('LOWER(users.email) LIKE ?', q)
  end
end
