require 'searchlight/adapters/action_view'

class DepartmentSearch < Searchlight::Search
  PER = 10

  include Searchlight::Adapters::ActionView

  def base_query
    Department.includes(:state).order(name: :asc)
              .page(options[:page]).per(PER).uniq
  end

  def search_state_id
    query.where(state_id: options[:state_id])
  end

  def search_name
    q = "%#{options[:name].to_s.mb_chars.downcase}%"
    query.where('LOWER(departments.name) LIKE ?', q)
  end
end
