class Removal
  class Category < ActiveRecord::Base
    has_many :removals, dependent: :restrict_with_error
    validates :name, presence: true
  end
end
