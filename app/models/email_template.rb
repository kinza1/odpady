class EmailTemplate < ActiveRecord::Base
  include Liquidize::Model

  liquidize :subject
  liquidize :body

  validates :name, presence: true, uniqueness: true
  validates :subject, presence: true
  validates :body, presence: true

  def deliver(email, options = {})
    DefaultMailer.email(
      to: email,
      subject: render_subject(options.stringify_keys),
      body: render_body(options.stringify_keys)
    ).deliver_later!
  end
end
