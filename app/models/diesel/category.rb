class Diesel
  class Category < ActiveRecord::Base
    has_many :diesels, dependent: :restrict_with_error
  end
end
