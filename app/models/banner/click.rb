class Banner
  class Click < ActiveRecord::Base
    belongs_to :banner, required: true

    validates :date, presence: true, uniqueness: { scope: :banner_id }
  end
end
