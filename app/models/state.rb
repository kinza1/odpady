class State < ActiveRecord::Base
  include Positionable

  has_many :companies, dependent: :restrict_with_error
  has_many :departments, dependent: :restrict_with_error
  has_one :government_plan, dependent: :restrict_with_error

  validates :name, presence: true, uniqueness: true
end
