class WasteFee < ActiveRecord::Base
  belongs_to :waste

  validates :year, :value, presence: true
end
