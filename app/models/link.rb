class Link < ActiveRecord::Base
  validates :label, :title, :url, presence: true

  scope :ordered, -> (order = :asc) { order(sort_order: order) }

  mount_uploader :image, ArticleImageUploader
end
