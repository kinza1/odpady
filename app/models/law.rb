class Law < ActiveRecord::Base
  include LawOnMainable

  validates :title, presence: true
end
