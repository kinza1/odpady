class UsefulLink < ActiveRecord::Base
  validates :link, presence: true
end
