require 'searchlight/adapters/action_view'

class CompanySearch < Searchlight::Search
  PER = 10

  include Searchlight::Adapters::ActionView

  def base_query
    Company.includes(:state, :branches).order(created_at: :desc)
           .page(options[:page]).per(PER).uniq
  end

  def search_state_id
    query.where(state_id: options[:state_id])
  end

  def search_branch_ids
    query.where(branches: { id: options[:branch_ids] })
  end

  def search_status
    case options[:status]
    when 'active'
      query.where(active: true)
    when 'inactive'
      query.where(active: false)
    else
      query
    end
  end

  def search_name
    q = "%#{options[:name].to_s.mb_chars.downcase}%"
    query.where('LOWER(companies.name) LIKE ?', q)
  end

  def branches_collection
    [[nil, nil]] | Branch.all.map { |b| [b.name, b.id] }
  end

  def states_collection
    [[nil, nil]] | State.all.map { |s| [s.name, s.id] }
  end
end
