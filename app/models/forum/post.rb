module Forum
  class Post < ActiveRecord::Base
    include Searchable

    before_save :check_user

    # @!group Associations
    belongs_to :thread, class_name: 'Forum::Thread',
                        required: true,
                        inverse_of: :posts,
                        touch: true
    belongs_to :user
    # @!endgroup

    # @!group Validations
    validates :message, presence: true, length: { maximum: 1500 }
    # @!endgroup

    # @!group Guest-specific validations
    validates :username, presence: true, if: :guest?
    # @!endgroup

    # @!group Registered user-specific validations
    validates :username, inclusion: { in: [nil, ''] }, unless: :guest?
    # @!endgroup

    # @!group Scopes
    scope :latest, -> { order(id: :desc) }
    # @!endgroup
    scope :approved, -> { where(approved: true) }

    # @return [Boolean] whether this post was created by guest
    def guest?
      user.nil?
    end

    def check_user
      self.approved = true if user.present?
    end

    def search_field
      message
    end
  end
end
