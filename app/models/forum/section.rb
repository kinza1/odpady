module Forum
  class Section < ActiveRecord::Base
    include Positionable

    # @!group Associations
    has_many :threads, dependent: :restrict_with_error

    # @!group Validations
    validates :name, presence: true, uniqueness: true
    # @!endgroup

    # @!group Scopes
    scope :active, -> { where(enabled: true) }
    # @!endgroup
  end
end
