module Forum
  class Thread < ActiveRecord::Base
    include Searchable

    attr_accessor :close_body

    # @!group Associations
    belongs_to :user
    belongs_to :section, required: true, class_name: 'Forum::Section'

    has_many :posts, inverse_of: :thread, dependent: :destroy
    # @!endgroup

    # @!group Validations
    validates :subject, presence: true, length: { maximum: 100 }
    # @!endgroup

    # @!group Guest-specific validations
    validates :notify_me, inclusion: { in: [false] }, if: :guest?
    validates :username, presence: true, if: :guest?
    # @!endgroup

    # @!group Registered user-specific validations
    validates :username, inclusion: { in: [nil, ''] }, unless: :guest?
    # @!endgroup

    scope :recent, -> { order(updated_at: :desc).limit(5) }
    scope :approved, -> { where(approved: true) }
    scope :closed, -> { where(closed: true) }

    # @return [Boolean] whether this thread was created by guest
    def guest?
      user.nil?
    end

    def search_field
      subject
    end
  end
end
