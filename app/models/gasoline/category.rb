class Gasoline
  class Category < ActiveRecord::Base
    has_many :gasolines, dependent: :restrict_with_error
  end
end
