class EnvironmentWater
  class Category < ActiveRecord::Base
    has_many :environment_waters, dependent: :restrict_with_error
  end
end
