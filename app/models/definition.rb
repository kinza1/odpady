class Definition < ActiveRecord::Base
  validates :name, presence: true
  validates :description, presence: true

  scope :letters, -> { uniq.pluck('upper(substr(name, 1, 1)) AS letter').sort }
end
