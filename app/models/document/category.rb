class Document
  class Category < ActiveRecord::Base
    has_many :documents, dependent: :restrict_with_error
    validates :name, presence: true
  end
end
