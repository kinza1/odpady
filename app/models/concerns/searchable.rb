require 'elasticsearch/model'

module Searchable
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks

    index_name [Rails.application.class.parent_name.downcase,
                Rails.env,
                model_name.collection.tr('/', '_')].join('_')

    settings index: { number_of_shards: 1 } do
      mapping dynamic: 'false' do
        indexes :search_field, type: 'string', analyzer: 'snowball'
        indexes :approved, type: 'boolean'
      end
    end

    def as_indexed_json(_options = {})
      as_json(
        only: [:id, :search_field, :class_name, :approved],
        methods: [:search_field, :class_name]
      )
    end

    def class_name
      self.class.name
    end
  end
end
