module Positionable
  extend ActiveSupport::Concern

  included do
    # @!group (Re)calculate position hooks
    before_validation :set_position
    after_destroy :recalculate_positions
    # @!endgroup

    # @!group Validations
    validates :position, presence: true, numericality: { greater_than: 0 }
    # @!endgroup
  end

  private

  def set_position
    self.position ||= self.class.count + 1
  end

  def recalculate_positions
    self.class.where('position > ?', position).update_all('position = position - 1')
  end
end
