module LawOnMainable
  extend ActiveSupport::Concern

  included do
    has_one :law_on_main, as: :itemable

    accepts_nested_attributes_for :law_on_main, allow_destroy: true
  end
end
