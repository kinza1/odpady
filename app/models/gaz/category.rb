class Gaz
  class Category < ActiveRecord::Base
    has_many :gazs, dependent: :restrict_with_error
  end
end
