class Company < ActiveRecord::Base
  belongs_to :state, required: true
  has_and_belongs_to_many :branches
  mount_uploader :logo, CompanyImageUploader
  validates :name, presence: true, uniqueness: true
  validates :business_profile, length: { maximum: 500 }

  scope :active, -> { where(active: true) }
  scope :recent, -> { active.order(created_at: :desc) }
end
