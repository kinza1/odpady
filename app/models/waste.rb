class Waste < ActiveRecord::Base
  validates :code, :name, presence: true

  belongs_to :parent, class_name: 'Waste'
  has_many :waste_fees

  accepts_nested_attributes_for :waste_fees,
                                reject_if: :all_blank,
                                allow_destroy: true
end
