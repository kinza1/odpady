class Water
  class Category < ActiveRecord::Base
    has_many :waters, dependent: :restrict_with_error
  end
end
