class Project < ActiveRecord::Base
  include LawOnMainable

  belongs_to :category
  validates :name, presence: true
end
