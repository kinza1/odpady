class Interpretation
  class Category < ActiveRecord::Base
    has_many :interpretations, dependent: :restrict_with_error
    validates :name, presence: true
  end
end
