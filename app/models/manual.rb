class Manual < ActiveRecord::Base
  belongs_to :category, required: true, class_name: 'Manual::Category'
  validates :name, presence: true
  validates :body, presence: true
end
