require 'searchlight/adapters/action_view'

class SubscriberSearch < Searchlight::Search
  include Searchlight::Adapters::ActionView

  def base_query
    Newsletter::Subscriber.includes(:newsletter_categories)
                          .order(id: :desc)
                          .page(options[:page])
                          .per(10)
  end

  def search_newsletter_category_ids
    query.where(newsletter_categories: { id: options[:newsletter_category_ids] })
  end

  def search_email
    q = "%#{options[:email].to_s.mb_chars.downcase}%"
    query.where('LOWER(newsletter_subscribers.email) LIKE ?', q)
  end
end
