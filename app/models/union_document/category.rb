class UnionDocument
  class Category < ActiveRecord::Base
    has_many :union_documents, dependent: :restrict_with_error
    validates :name, presence: true
  end
end
