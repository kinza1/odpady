class LawOnMain < ActiveRecord::Base
  belongs_to :itemable, polymorphic: true

  validates :icon_id, inclusion: { in: [1, 2, 3, 4, 5] }

  def self.on_main_halfs
    a = all
    if a.empty?
      [[], []]
    else
      a.each_slice((a.size / 2.0).round).to_a
    end
  end
end
