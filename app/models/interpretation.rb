class Interpretation < ActiveRecord::Base
  include LawOnMainable

  belongs_to :category, required: true, class_name: 'Interpretation::Category'
  validates :body, presence: true
end
