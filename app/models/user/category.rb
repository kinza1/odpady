class User
  class Category < ActiveRecord::Base
    has_many :users, dependent: :restrict_with_error
  end
end
