class Recycling
  class Category < ActiveRecord::Base
    has_many :recyclings, dependent: :restrict_with_error
  end
end
