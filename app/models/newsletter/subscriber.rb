module Newsletter
  class Subscriber < ActiveRecord::Base
    validates :email, presence: true,
                      uniqueness: { case_sensitive: false },
                      format: { with: Devise.email_regexp }

    scope :enabled, -> { where(enabled: true) }

    has_many :newsletter_categories_subscribers
    has_and_belongs_to_many :newsletter_categories, class_name: 'Newsletter::Category'
  end
end
