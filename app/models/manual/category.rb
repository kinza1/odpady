class Manual
  class Category < ActiveRecord::Base
    has_many :manuals, dependent: :restrict_with_error
    validates :name, presence: true
  end
end
