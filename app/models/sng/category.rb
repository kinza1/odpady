class Sng
  class Category < ActiveRecord::Base
    has_many :sngs, dependent: :restrict_with_error
  end
end
