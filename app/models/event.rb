class Event < ActiveRecord::Base
  mount_uploader :image, EventImageUploader
  validates :name, presence: true
  validates :body, presence: true
  validates :start_at, presence: true
  validates :image, presence: true

  scope :published, -> { where(published: true) }
  scope :soon, -> { where('events.start_at > ?', Time.current) }
  scope :old, -> { where('events.start_at < ?', Time.current) }
end
