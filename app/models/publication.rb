class Publication < ActiveRecord::Base
  mount_uploader :pdf, PDFUploader
  validates :name, presence: true
end
