class ArticleRecommendation < ActiveRecord::Base
  belongs_to :article
  belongs_to :recommendation, class_name: 'Article'
end
