class Article < ActiveRecord::Base
  validates :name, :preview, :body, :image, :date, presence: true

  scope :published, -> { where(published: true) }
  scope :except_id, -> (id) { published.order(id: :desc).where.not(id: id).limit(50) }

  has_many :article_recommendations
  has_many :recommendations, through: :article_recommendations

  mount_uploader :image, ArticleImageUploader
end
