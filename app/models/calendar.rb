class Calendar < ActiveRecord::Base
  validates :date, presence: true
  validates :concerned_to, presence: true
  validates :todo, presence: true
  validates :body, presence: true
end
