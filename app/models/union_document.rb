class UnionDocument < ActiveRecord::Base
  include LawOnMainable

  belongs_to :category, class_name: 'UnionDocument::Category', required: true
  validates :title, presence: true
end
