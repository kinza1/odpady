class MainSearch
  attr_reader :params, :search_classes, :search

  def initialize(attrs = {})
    @params = attrs.fetch(:params, {})
    @search_classes = attrs.fetch(:search_classes, [Forum::Thread, Forum::Post])
    do_search
  end

  def results
    MainSearchCollection.new(result_collection, search.page, search.limit_value, search.current_page, search.total_pages)
  end

  private

  def result_collection
    search.results.map do |r|
      model = OpenStruct.new(record: r.class_name.constantize.find(r.id),
                             record_class: r.class_name.constantize)

      model.highlight = highlight(r)

      model
    end
  end

  def highlight(r)
    if r.respond_to?(:highlight)
      r.highlight.search_field.first
    else
      r.search_field
    end
  end

  def do_search
    @search = Elasticsearch::Model.search(options, search_classes)
                                  .page(params[:page] || 1)
                                  .per(10)
  end

  def options
    opts = base

    opts[:query][:filtered][:query] = {
      query_string: {
        query: "search_field:*#{query}*"
      }
    } if params[:query].present?

    opts
  end

  def base
    {
      query: query_hash,
      highlight: highlight_hash
    }
  end

  def query_hash
    {
      filtered: {
        filter: {
          term: {
            approved: true
          }
        }
      }
    }
  end

  def highlight_hash
    {
      pre_tags: ['<span class="higlight_search">'],
      post_tags: ['</span>'],
      fields: {
        search_field: {}
      }
    }
  end

  def query
    pattern = %r{(\'|\"|\.|\*|\/|\-|\\|\)|\$|\+|\(|\^|\?|\!|\~|\`|\:)}
    params[:query].gsub(pattern) { |match| "\\#{match}" }
  end
end

class MainSearchCollection
  attr_reader :collection, :page, :limit_value,
              :current_page, :total_pages

  def initialize(collection, page, limit_value, current_page, total_pages)
    @collection = collection
    @page = page
    @limit_value = limit_value
    @current_page = current_page
    @total_pages = total_pages
  end

  delegate :each, to: :collection
  delegate :length, to: :collection
end
