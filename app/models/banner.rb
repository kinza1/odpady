class Banner < ActiveRecord::Base
  mount_uploader :image, BannerUploader

  has_many :views, class_name: 'Banner::View', dependent: :destroy
  has_many :clicks, class_name: 'Banner::Click', dependent: :destroy

  validates :token, presence: true, uniqueness: true
  validates :image, presence: true
  validates :link, presence: true

  before_validation :set_token

  scope :top, -> { where(top: true) }
  scope :sidebar, -> { where(sidebar: true) }
  scope :active, -> { where('banners.top = :value OR banners.sidebar = :value', value: true) }

  # Generates unique token
  # @return [String] new token
  def self.generate_token
    loop do
      token = SecureRandom.hex(32)
      break token unless exists?(token: token)
    end
  end

  def view
    transaction { view! }
    true
  rescue
    false
  end

  def click
    transaction { click! }
    true
  rescue
    false
  end

  private

  def set_token
    self.token ||= self.class.generate_token
  end

  def view!
    date = Time.current.midday
    view = views.where(date: date).first_or_create
    view.increment(:count)
    view.save
    increment(:views_count)
    save
  end

  def click!
    date = Time.current.midday
    click = clicks.where(date: date).first_or_create
    click.increment(:count)
    click.save
    increment(:clicks_count)
    save
  end
end
