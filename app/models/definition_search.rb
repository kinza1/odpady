require 'searchlight/adapters/action_view'

class DefinitionSearch < Searchlight::Search
  PER = 10

  include Searchlight::Adapters::ActionView

  def base_query
    Definition.order(name: :asc).page(options[:page]).per(PER)
  end

  def search_letter
    letter = options[:letter].to_s.mb_chars.upcase
    query.where('upper(substr(name, 1, 1)) = ?', letter)
  end
end
