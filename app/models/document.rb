class Document < ActiveRecord::Base
  mount_uploader :pdf, PDFUploader
  mount_uploader :doc, DOCUploader

  belongs_to :category, required: true, class_name: 'Document::Category'

  validates :name, presence: true
end
