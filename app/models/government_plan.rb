class GovernmentPlan < ActiveRecord::Base
  belongs_to :state, required: true
  validates :state_id, presence: true, uniqueness: true
  mount_uploader :image, GovernmentPlanImageUploader
end
