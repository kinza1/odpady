class Department < ActiveRecord::Base
  belongs_to :state, required: true
  validates :name, presence: true
end
