class ArticlesController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /articles
  def index
    @articles = Article.published
                       .order(date: :desc)
                       .page(params[:page])
                       .per(10)
  end

  # GET /articles/:id
  def show
    @article = Article.published.find(params[:id])
  end
end
