module Gasolines
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /gasolines/categories
    def index
      @categories = ::Gasoline::Category.all
    end
  end
end
