class WastesController < ApplicationController
  skip_after_action :verify_authorized

  # GET /wastes
  def index
    @wastes_grouped = Waste.order(code: :asc)
                           .group_by(&:parent_id)
  end
end
