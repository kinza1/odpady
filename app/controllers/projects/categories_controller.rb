module Projects
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /interpretations/categories
    def index
      @categories = ::Project::Category.all
    end

    # GET /interpretations/categories/:id
    def show
      @category = ::Project::Category.find(params[:id])
      @projects = @category.projects.page(params[:page]).per(10)
    end
  end
end
