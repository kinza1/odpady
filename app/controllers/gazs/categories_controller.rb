module Gazs
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /gazs/categories
    def index
      @categoriesnames = ::Gaz::Category.all.map(&:name).uniq
    end
  end
end
