module EnvironmentWaters
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /interpretations/removals
    def index
      @categories = ::EnvironmentWater::Category.all
    end
  end
end
