class LawsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /laws
  def index
    @laws = Law.order(created_at: :desc).page(params[:page]).per(25)
  end
end
