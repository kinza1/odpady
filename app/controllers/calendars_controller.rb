class CalendarsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /calendars
  def index
    @months = (1..12).map { |m| t('date.month_names')[m] }
    @search = CalendarSearch.new(search_params)
    @calendars = @search.results
    @month =
      if params[:search]
        @months.at(params[:search][:month].to_i - 1)
      else
        'wszystkie'
      end
  end

  # GET /calendars/:id
  def show
    @calendar = Calendar.find(params[:id])
  end
end
