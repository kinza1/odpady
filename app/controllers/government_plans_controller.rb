class GovernmentPlansController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized
  before_action :set_states

  # GET /government_plans
  def index
  end

  # GET /government_plans/:id
  def show
    @government_plan = GovernmentPlan.find(params[:id])
  end

  private

  def set_states
    @states = State.where(
      id: GovernmentPlan.select(:state_id)
    ).order(position: :asc).uniq
  end
end
