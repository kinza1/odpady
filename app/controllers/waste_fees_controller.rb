class WasteFeesController < ApplicationController
  skip_after_action :verify_authorized

  # GET /waste_fees
  def index
    @wastes_grouped = Waste.order(code: :asc)
                           .group_by(&:parent_id)

    @years = WasteFee.select('DISTINCT year').order(year: :asc).where.not(value: 0).map(&:year)
  end
end
