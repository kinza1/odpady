class PublicationsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /publications
  def index
    @publications = Publication.order(created_at: :desc)
                               .page(params[:page])
                               .per(10)
  end
end
