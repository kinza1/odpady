module Interpretations
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /interpretations/categories
    def index
      @categories = ::Interpretation::Category.all
    end

    # GET /interpretations/categories/:id
    def show
      @category = ::Interpretation::Category.find(params[:id])
    end
  end
end
