class BannersController < ApplicationController
  # GET /banners/:id
  def show
    @banner = Banner.active.find(params[:id])
    @banner.click
    redirect_to @banner.link
  end
end
