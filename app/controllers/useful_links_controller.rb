class UsefulLinksController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /useful_links
  def index
    @useful_links = UsefulLink.order(created_at: :desc)
  end
end
