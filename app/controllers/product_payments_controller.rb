class ProductPaymentsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /product_payments
  def index
    @product_payments = ProductPayment.where.not(name: 'opakowania razem').order(:year)
    @names = @product_payments.map(&:name).uniq
    @years = @product_payments.map(&:year).uniq
    @product_payments_all = ProductPayment.where(name: 'opakowania razem')
    @years_all = @product_payments_all.map(&:year)
  end
end
