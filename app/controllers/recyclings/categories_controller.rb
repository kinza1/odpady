module Recyclings
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /recyclings/categories
    def index
      @categories = ::Recycling::Category.all
    end
  end
end
