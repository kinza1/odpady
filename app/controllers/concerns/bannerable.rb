module Bannerable
  extend ActiveSupport::Concern

  included do
    before_action :set_banners
    after_action :update_banners

    helper_method :top_banner_shown!
    helper_method :sidebar_banner_shown!
  end

  private

  def set_banners
    set_top_banner
    set_sidebar_banner
  end

  def update_banners
    update_top_banner
    update_sidebar_banner
  end

  def set_top_banner
    @top_banner = Banner.top.order('RANDOM()').first
  end

  def set_sidebar_banner
    @sidebar_banner = Banner.sidebar
    @sidebar_banner = @sidebar_banner.where.not(id: @top_banner.id) if @top_banner
    @sidebar_banner = @sidebar_banner.order('RANDOM()').first
  end

  def update_top_banner
    return unless @top_banner
    return unless top_banner_shown?
    @top_banner.view
  end

  def update_sidebar_banner
    return unless @sidebar_banner
    return unless sidebar_banner_shown?
    @sidebar_banner.view
  end

  def top_banner_shown!
    @top_banner_shown = true
  end

  def sidebar_banner_shown!
    @sidebar_banner_shown = true
  end

  def top_banner_shown?
    @top_banner_shown ||= false
  end

  def sidebar_banner_shown?
    @sidebar_banner_shown ||= false
  end
end
