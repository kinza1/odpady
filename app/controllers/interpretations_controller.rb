class InterpretationsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /interpretations
  def index
    @interpretation_category = ::Interpretation::Category.find(params[:interpretation_category_id])
    @interpretations = @interpretation_category.interpretations
  end

  # GET /interpretation_categories/:category_id/interpretation
  def show
    @interpretation = Interpretation.find(params[:id])
  end
end
