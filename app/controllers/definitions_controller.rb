class DefinitionsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /definitions
  def index
    @letters = Definition.letters
    @search = DefinitionSearch.new(search_params)
    @definitions = @search.results
  end
end
