class ProjectsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /categories/:id
  def show
    @project = Project.find(params[:id])
  end
end
