class CompaniesController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /companies
  def index
    @search = CompanySearch.new(search_params.merge(status: 'active'))
    @companies = @search.results
  end

  # GET /companies/:id
  def show
    @company = Company.active.find(params[:id])
  end

  # GET /companies/new
  def new
    @form = CreateCompanyForm.new
  end

  # POST /companies
  def create
    @form = CreateCompanyForm.new(params: company_params)
    if @form.save
      CompanyNotifyService.new(@form.company).notify
      redirect_to companies_url, notice: t('.success')
    else
      render :new
    end
  end

  private

  def company_params
    params.require(:company).permit(
      :state_id, :name, :fake_logo, :logo, :homepage, :email, :zipcode,
      :city, :street, :phone, :cellphone, :fax,
      :contact_person, :business_profile,
      branch_ids: []
    )
  end
end
