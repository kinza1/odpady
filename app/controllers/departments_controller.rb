class DepartmentsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /departments
  def index
    @states = State.includes(:departments)
  end
end
