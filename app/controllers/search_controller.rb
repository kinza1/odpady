class SearchController < ApplicationController
  include Bannerable

  before_action :fix_params

  # GET /forum/search
  def index
    authorize MainSearch

    @results = if params[:search][:query].present?
                 MainSearch.new(params: params[:search].merge(page: params[:page])).results
               else
                 OpenStruct.new(page: 1, current_page: 1, total_pages: 0, limit_value: 0)
               end
  end

  private

  def fix_params
    params[:search] = params[:search] || {}
  end
end
