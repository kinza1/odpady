module Admin
  class LinksController < BaseController
    before_action :set_and_authorize_link, only: [:edit, :update, :destroy]

    # GET /admin/links
    def index
      authorize [:admin, :Link]
      @links = Link.ordered
    end

    # GET /admin/links/new
    def new
      authorize [:admin, :Link]
      @link = Link.new
    end

    # POST /admin/links
    def create
      authorize [:admin, :Link]
      @link = Link.new(link_params)
      if @link.save
        redirect_to admin_links_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/links/:id/edit
    def edit
    end

    # PATCH/PUT /admin/links/:id
    def update
      if @link.update(link_params)
        redirect_to admin_links_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/links/:id
    def destroy
      if @link.destroy
        redirect_to admin_links_url, notice: t('shared.destroyed')
      else
        redirect_to admin_links_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_link
      @link = Link.find(params[:id])
      authorize [:admin, @link]
    end

    def link_params
      params.require(:link).permit(
        :label, :title, :url, :image
      )
    end
  end
end
