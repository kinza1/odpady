module Admin
  class UnionDocumentCategoriesController < BaseController
    before_action :set_and_authorize_category, only: [:edit, :update, :destroy]

    # GET /admin/union_document_categories
    def index
      authorize [:admin, ::UnionDocument::Category]
      @union_document_categories = ::UnionDocument::Category.all
    end

    # GET /admin/union_document_categories/new
    def new
      authorize [:admin, ::UnionDocument::Category]
      @union_document_category = ::UnionDocument::Category.new
    end

    # POST /admin/union_document_categories
    def create
      authorize [:admin, ::UnionDocument::Category]
      @union_document_category = ::UnionDocument::Category.new(category_params)
      if @union_document_category.save
        redirect_to admin_union_document_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/union_document_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/union_document_categories/:id
    def update
      if @union_document_category.update(category_params)
        redirect_to admin_union_document_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/union_document_categories/:id
    def destroy
      if @union_document_category.destroy
        redirect_to admin_union_document_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_union_document_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_category
      @union_document_category = ::UnionDocument::Category.find(params[:id])
      authorize [:admin, @union_document_category]
    end

    def category_params
      params.require(:union_document_category).permit(:name)
    end
  end
end
