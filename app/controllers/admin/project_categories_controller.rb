module Admin
  class ProjectCategoriesController < BaseController
    before_action :set_and_authorize_category, only: [:edit, :update, :destroy]

    # GET /admin/project_categories
    def index
      authorize [:admin, ::Project::Category]
      @project_categories = ::Project::Category.all
    end

    # GET /admin/project_categories/new
    def new
      authorize [:admin, ::Project::Category]
      @project_category = ::Project::Category.new
    end

    # POST /admin/project_categories
    def create
      authorize [:admin, ::Project::Category]
      @project_category = ::Project::Category.new(category_params)
      if @project_category.save
        redirect_to admin_project_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/project_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/project_categories/:id
    def update
      if @project_category.update(category_params)
        redirect_to admin_project_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/project_categories/:id
    def destroy
      if @project_category.destroy
        redirect_to admin_project_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_project_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_category
      @project_category = ::Project::Category.find(params[:id])
      authorize [:admin, @project_category]
    end

    def category_params
      params.require(:project_category).permit(:name)
    end
  end
end
