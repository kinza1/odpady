module Admin
  class GasolinesController < BaseController
    before_action :set_and_authorize_gasoline, only: [:edit, :update, :destroy]

    # GET /admin/gasoline_categories/:category_id/gasolines
    def index
      authorize [:admin, ::Gasoline]
      @gasoline_category = ::Gasoline::Category.find(params[:gasoline_category_id])
      @gasolines = @gasoline_category.gasolines.order(:name).order(:year).page(params[:page]).per(25)
    end

    # GET /admin/gasoline_categories/:category_id/gasolines/new
    def new
      authorize [:admin, ::Gasoline]
      @gasoline_category = ::Gasoline::Category.find(params[:gasoline_category_id])
      @gasoline = @gasoline_category.gasolines.new
    end

    # POST /admin/gasoline_categories/:category_id/gasolines
    def create
      authorize [:admin, ::Gasoline]
      @gasoline_category = ::Gasoline::Category.find(params[:gasoline_category_id])
      @gasoline = @gasoline_category.gasolines.new(gasoline_params)
      if @gasoline.save
        redirect_to admin_gasoline_category_gasolines_path(@gasoline_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/gasoline_categories/:category_id/gasolines/:id/edit
    def edit
      @gasoline_category = ::Gasoline::Category.find(params[:gasoline_category_id])
    end

    # PATCH/PUT /admin/gasoline_categories/:category_id/gasolines/:id
    def update
      @gasoline_category = ::Gasoline::Category.find(params[:gasoline_category_id])
      if @gasoline.update(gasoline_params)
        redirect_to admin_gasoline_category_gasolines_path(@gasoline_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/gasoline_categories/:category_id/gasolines/:id
    def destroy
      @gasoline_category = ::Gasoline::Category.find(params[:gasoline_category_id])
      if @gasoline.destroy
        redirect_to admin_gasoline_category_gasolines_path(@gasoline_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_gasoline_category_gasolines_path(@gasoline_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_gasoline
      @gasoline = ::Gasoline.find(params[:id])
      authorize [:admin, @gasoline]
    end

    def gasoline_params
      params.require(:gasolines).permit(:name, :year, :value, :bs)
    end
  end
end
