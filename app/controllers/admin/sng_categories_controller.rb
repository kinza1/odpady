module Admin
  class SngCategoriesController < BaseController
    before_action :set_and_authorize_sng_category, only: [:edit, :update, :destroy]

    # GET /admin/sng_categories
    def index
      authorize [:admin, ::Sng::Category]
      @sng_categories = ::Sng::Category.all
    end

    # GET /admin/sng_categories/new
    def new
      authorize [:admin, ::Sng::Category]
      @sng_category = ::Sng::Category.new
    end

    # POST /admin/sng_categories
    def create
      authorize [:admin, ::Sng::Category]
      @sng_category = ::Sng::Category.new(sng_category_params)
      if @sng_category.save
        redirect_to admin_sng_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/sng_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/sng_categories/:id
    def update
      if @sng_category.update(sng_category_params)
        redirect_to admin_sng_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/sng_categories/:id
    def destroy
      if @sng_category.destroy
        redirect_to admin_sng_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_sng_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_sng_category
      @sng_category = ::Sng::Category.find(params[:id])
      authorize [:admin, @sng_category]
    end

    def sng_category_params
      params.require(:sng_category).permit(:name, :yearname)
    end
  end
end
