module Admin
  class UsersController < BaseController
    before_action :set_and_authorize_user, only: [:edit, :update, :destroy]

    # GET /admin/users
    def index
      authorize [:admin, :User]
      @search = UserSearch.new(search_params)
      @users = @search.results
    end

    # GET /admin/users/:id/edit
    def edit
    end

    # PATCH/PUT /admin/users/:id
    def update
      if @user.update(user_params)
        redirect_to admin_users_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/users/:id
    def destroy
      if @user.destroy
        redirect_to admin_users_url, notice: t('shared.destroyed')
      else
        redirect_to admin_users_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_user
      @user = ::User.find(params[:id])
      authorize [:admin, @user]
    end

    def user_params
      params.require(:user).permit(
        :email, :username, :admin, :approved, :category_id
      )
    end
  end
end
