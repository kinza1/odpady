module Admin
  class DepartmentsController < BaseController
    before_action :set_and_authorize_department, only: [:edit, :update, :destroy]

    # GET /admin/departments
    def index
      authorize [:admin, :Department]
      @search = DepartmentSearch.new(search_params)
      @departments = @search.results
    end

    # GET /admin/departments/new
    def new
      authorize [:admin, :Department]
      @department = Department.new
    end

    # POST /admin/departments
    def create
      authorize [:admin, :Department]
      @department = Department.new(department_params)
      if @department.save
        redirect_to admin_departments_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/departments/:id/edit
    def edit
    end

    # PATCH/PUT /admin/departments/:id
    def update
      if @department.update(department_params)
        redirect_to admin_departments_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/departments/:id
    def destroy
      if @department.destroy
        redirect_to admin_departments_url, notice: t('shared.destroyed')
      else
        redirect_to admin_departments_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_department
      @department = Department.find(params[:id])
      authorize [:admin, @department]
    end

    def department_params
      params.require(:department).permit(
        :state_id, :name, :homepage, :email, :zipcode,
        :city, :adress, :phone, :fax
      )
    end
  end
end
