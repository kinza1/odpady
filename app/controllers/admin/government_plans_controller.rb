module Admin
  class GovernmentPlansController < BaseController
    before_action :set_and_authorize_government_plan, only: [:edit, :update, :destroy]

    # GET /admin/government_plans
    def index
      authorize [:admin, GovernmentPlan]
      @government_plans = GovernmentPlan.all
    end

    # GET /admin/government_plans/new
    def new
      authorize [:admin, GovernmentPlan]
      @government_plan = GovernmentPlan.new
    end

    # POST /admin/government_plans
    def create
      authorize [:admin, GovernmentPlan]
      @government_plan = GovernmentPlan.new(government_plan_params)
      if @government_plan.save
        redirect_to admin_government_plans_path, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/government_plans/:id/edit
    def edit
    end

    # PATCH/PUT /admin/government_plans/:id
    def update
      if @government_plan.update(government_plan_params)
        redirect_to admin_government_plans_path, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/government_plans/:id
    def destroy
      if @government_plan.destroy
        redirect_to admin_government_plans_path, notice: t('shared.destroyed')
      else
        redirect_to admin_government_plans_path, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_government_plan
      @government_plan = GovernmentPlan.find(params[:id])
      authorize [:admin, @government_plan]
    end

    def government_plan_params
      params.require(:government_plan).permit(
        :state_id, :body, :image
      )
    end
  end
end
