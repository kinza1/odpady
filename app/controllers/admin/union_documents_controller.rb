module Admin
  class UnionDocumentsController < BaseController
    before_action :set_and_authorize_union_document, only: [:edit, :update, :destroy]

    # GET /admin/union_document_categories/:catproj_id/union_documents
    def index
      authorize [:admin, ::UnionDocument]
      @union_document_category = ::UnionDocument::Category.find(params[:union_document_category_id])
      @union_documents = @union_document_category.union_documents
    end

    # GET /admin/union_document_categories/:catproj_id/union_documents/new
    def new
      authorize [:admin, ::UnionDocument]
      @union_document_category = ::UnionDocument::Category.find(params[:union_document_category_id])
      @union_document = @union_document_category.union_documents.new
      @union_document.build_law_on_main
    end

    # POST /admin/union_document_categories/:catproj_id/union_documents
    def create
      authorize [:admin, ::UnionDocument]
      @union_document_category = ::UnionDocument::Category.find(params[:union_document_category_id])
      @union_document = @union_document_category.union_documents
                                                .new(union_document_params)
      if @union_document.save
        redirect_to admin_union_document_category_union_documents_path(@union_document_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/union_document_categories/:catproj_id/union_documents/:id/edit
    def edit
      @union_document_category = ::UnionDocument::Category.find(params[:union_document_category_id])
      @union_document.law_on_main || @union_document.build_law_on_main
    end

    # PATCH/PUT /admin/union_document_categories/:catproj_id/union_documents/:id
    def update
      @union_document_category = ::UnionDocument::Category.find(params[:union_document_category_id])
      if @union_document.update(union_document_params)
        redirect_to admin_union_document_category_union_documents_path(@union_document_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/union_document_categories/:catproj_id/union_documents/:id
    def destroy
      @union_document_category = ::UnionDocument::Category.find(params[:union_document_category_id])
      if @union_document.destroy
        redirect_to admin_union_document_category_union_documents_path(@union_document_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_union_document_category_union_documents_path(@union_document_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_union_document
      @union_document = ::UnionDocument.find(params[:id])
      authorize [:admin, @union_document]
    end

    def union_document_params
      params.require(:union_document).permit(
        :union_document_category_id, :title, :address, :pdf, :eur_lex,
        law_on_main_attributes: [:_destroy, :id, :itemable_id, :itemable_type, :icon_id]
      )
    end
  end
end
