module Admin
  class BranchesController < BaseController
    before_action :set_and_authorize_branch, only: [:edit, :update, :destroy]

    # GET /admin/branches
    def index
      authorize [:admin, Branch]
      @branches = Branch.order(position: :asc)
    end

    # GET /admin/branches/new
    def new
      authorize [:admin, Branch]
      @branch = Branch.new
    end

    # POST /admin/branches
    def create
      authorize [:admin, Branch]
      @branch = Branch.new(branch_params)
      if @branch.save
        redirect_to admin_branches_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/branches/:id/edit
    def edit
    end

    # PATCH/PUT /admin/branches/:id
    def update
      if @branch.update(branch_params)
        redirect_to admin_branches_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/branches/:id
    def destroy
      if @branch.destroy
        redirect_to admin_branches_url, notice: t('shared.destroyed')
      else
        redirect_to admin_branches_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_branch
      @branch = Branch.find(params[:id])
      authorize [:admin, @branch]
    end

    def branch_params
      params.require(:branch).permit(:name)
    end
  end
end
