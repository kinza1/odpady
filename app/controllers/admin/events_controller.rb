module Admin
  class EventsController < BaseController
    before_action :set_and_authorize_event, only: [:edit, :update, :destroy]

    # GET /admin/events
    def index
      authorize [:admin, :Event]
      @events = Event.order(start_at: :desc)
    end

    # GET /admin/events/new
    def new
      authorize [:admin, :Event]
      @event = Event.new(start_at: Time.current.midday)
    end

    # POST /admin/events
    def create
      authorize [:admin, :Event]
      @event = Event.new(event_params)
      if @event.save
        redirect_to admin_events_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/events/:id/edit
    def edit
    end

    # PATCH/PUT /admin/events/:id
    def update
      if @event.update(event_params)
        redirect_to admin_events_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/events/:id
    def destroy
      if @event.destroy
        redirect_to admin_events_url, notice: t('shared.destroyed')
      else
        redirect_to admin_events_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_event
      @event = Event.find(params[:id])
      authorize [:admin, @event]
    end

    def event_params
      params.require(:event).permit(:name, :body, :start_at, :image, :published)
    end
  end
end
