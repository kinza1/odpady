module Admin
  class ProjectsController < BaseController
    before_action :set_and_authorize_project, only: [:edit, :update, :destroy]

    # GET /admin/project_categories/:catproj_id/projects
    def index
      authorize [:admin, ::Project]
      @project_category = ::Project::Category.find(params[:project_category_id])
      @projects = @project_category.projects
    end

    # GET /admin/project_categories/:catproj_id/projects/new
    def new
      authorize [:admin, ::Project]
      @project_category = ::Project::Category.find(params[:project_category_id])
      @project = @project_category.projects.new
      @project.build_law_on_main
    end

    # POST /admin/project_categories/:catproj_id/projects
    def create
      authorize [:admin, ::Project]
      @project_category = ::Project::Category.find(params[:project_category_id])
      @project = @project_category.projects.new(project_params)
      if @project.save
        redirect_to admin_project_category_projects_path(@project_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/project_categories/:catproj_id/projects/:id/edit
    def edit
      @project_category = ::Project::Category.find(params[:project_category_id])
      @project.law_on_main || @project.build_law_on_main
    end

    # PATCH/PUT /admin/project_categories/:catproj_id/projects/:id
    def update
      @project_category = ::Project::Category.find(params[:project_category_id])
      if @project.update(project_params)
        redirect_to admin_project_category_projects_path(@project_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/project_categories/:catproj_id/projects/:id
    def destroy
      @project_category = ::Project::Category.find(params[:project_category_id])
      if @project.destroy
        redirect_to admin_project_category_projects_path(@project_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_project_category_projects_path(@project_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_project
      @project = ::Project.find(params[:id])
      authorize [:admin, @project]
    end

    def project_params
      params.require(:project).permit(
        :project_category_id, :name, :body, :pdf,
        law_on_main_attributes: [:_destroy, :id, :itemable_id, :itemable_type, :icon_id]
      )
    end
  end
end
