module Admin
  class EmailTemplatesController < BaseController
    before_action :set_and_authorize_email_template, only: [:edit, :update]

    # GET /admin/email_templates
    def index
      authorize [:admin, EmailTemplate]
      @email_templates = EmailTemplate.order(id: :asc)
    end

    # GET /admin/email_templates/:id/edit
    def edit
    end

    # PATCH/PUT /admin/email_templates/:id
    def update
      if @email_template.update(email_template_params)
        redirect_to admin_email_templates_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    private

    def set_and_authorize_email_template
      @email_template = EmailTemplate.find(params[:id])
      authorize [:admin, @email_template]
    end

    def email_template_params
      params.require(:email_template).permit(:subject, :body)
    end
  end
end
