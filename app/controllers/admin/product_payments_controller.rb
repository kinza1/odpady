module Admin
  class ProductPaymentsController < BaseController
    before_action :set_and_authorize_product_payment, only: [:edit, :update, :destroy]

    # GET /admin/product_payments
    def index
      authorize [:admin, :ProductPayment]
      @product_payments = ProductPayment.order(:name)
    end

    # GET /admin/product_payments/new
    def new
      authorize [:admin, :ProductPayment]
      @product_payment = ProductPayment.new
    end

    # POST /admin/product_payments
    def create
      authorize [:admin, :ProductPayment]
      @product_payment = ProductPayment.new(product_payment_params)
      if @product_payment.save
        redirect_to admin_product_payments_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/product_payments/:id/edit
    def edit
    end

    # PATCH/PUT /admin/product_payments/:id
    def update
      if @product_payment.update(product_payment_params)
        redirect_to admin_product_payments_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/product_payments/:id
    def destroy
      if @product_payment.destroy
        redirect_to admin_product_payments_url, notice: t('shared.destroyed')
      else
        redirect_to admin_product_payments_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_product_payment
      @product_payment = ProductPayment.find(params[:id])
      authorize [:admin, @product_payment]
    end

    def product_payment_params
      params.require(:product_payment).permit(
        :name, :year, :sum, :percentage
      )
    end
  end
end
