module Admin
  class ManualsController < BaseController
    before_action :set_and_authorize_manual, only: [:edit, :update, :destroy]

    # GET /admin/manual_categories/:category_id/manuals
    def index
      authorize [:admin, ::Manual]
      @manual_category = ::Manual::Category.find(params[:manual_category_id])
      @manuals = @manual_category.manuals
    end

    # GET /admin/manual_categories/:category_id/manuals/new
    def new
      authorize [:admin, ::Manual]
      @manual_category = ::Manual::Category.find(params[:manual_category_id])
      @manual = @manual_category.manuals.new
    end

    # POST /admin/manual_categories/:category_id/manuals
    def create
      authorize [:admin, ::Manual]
      @manual_category = ::Manual::Category.find(params[:manual_category_id])
      @manual = @manual_category.manuals.new(manual_params)
      if @manual.save
        redirect_to admin_manual_category_manuals_url(@manual_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/manual_categories/:category_id/manuals/:id/edit
    def edit
      @manual_category = ::Manual::Category.find(params[:manual_category_id])
    end

    # PATCH/PUT /admin/manual_categories/:category_id/manuals/:id
    def update
      @manual_category = ::Manual::Category.find(params[:manual_category_id])
      if @manual.update(manual_params)
        redirect_to admin_manual_category_manuals_url(@manual_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/manual_categories/:category_id/manuals/:id
    def destroy
      @manual_category = ::Manual::Category.find(params[:manual_category_id])
      if @manual.destroy
        redirect_to admin_manual_category_manuals_url(@manual_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_manual_category_manuals_url(@manual_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_manual
      @manual = ::Manual.find(params[:id])
      authorize [:admin, @manual]
    end

    def manual_params
      params.require(:manual).permit(:manual_category_id, :name, :description, :body)
    end
  end
end
