module Admin
  class BannersController < BaseController
    before_action :set_and_authorize_banner, only: [:edit, :update, :destroy]

    # GET /admin/banners
    def index
      authorize [:admin, :Banner]
      @banners = Banner.order(
        views_count: :desc, clicks_count: :desc, created_at: :desc
      )
    end

    # GET /admin/banners/new
    def new
      authorize [:admin, :Banner]
      @banner = Banner.new
    end

    # POST /admin/banners
    def create
      authorize [:admin, :Banner]
      @banner = Banner.new(banner_params)
      if @banner.save
        redirect_to admin_banners_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/banners/:id/edit
    def edit
    end

    # PATCH/PUT /admin/banners/:id
    def update
      if @banner.update(banner_params)
        redirect_to admin_banners_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/banners/:id
    def destroy
      if @banner.destroy
        redirect_to admin_banners_url, notice: t('shared.destroyed')
      else
        redirect_to admin_banners_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_banner
      @banner = Banner.find(params[:id])
      authorize [:admin, @banner]
    end

    def banner_params
      params.require(:banner).permit(
        :description, :image, :link, :top, :sidebar
      )
    end
  end
end
