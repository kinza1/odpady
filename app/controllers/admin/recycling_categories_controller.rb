module Admin
  class RecyclingCategoriesController < BaseController
    before_action :set_and_authorize_recycling_category, only: [:edit, :update, :destroy]

    # GET /admin/recycling_categories
    def index
      authorize [:admin, ::Recycling::Category]
      @recycling_categories = ::Recycling::Category.all
    end

    # GET /admin/recycling_categories/new
    def new
      authorize [:admin, ::Recycling::Category]
      @recycling_category = ::Recycling::Category.new
    end

    # POST /admin/recycling_categories
    def create
      authorize [:admin, ::Recycling::Category]
      @recycling_category = ::Recycling::Category.new(recycling_category_params)
      if @recycling_category.save
        redirect_to admin_recycling_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/recycling_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/recycling_categories/:id
    def update
      if @recycling_category.update(recycling_category_params)
        redirect_to admin_recycling_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/recycling_categories/:id
    def destroy
      if @recycling_category.destroy
        redirect_to admin_recycling_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_recycling_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_recycling_category
      @recycling_category = ::Recycling::Category.find(params[:id])
      authorize [:admin, @recycling_category]
    end

    def recycling_category_params
      params.require(:recycling_category).permit(:name)
    end
  end
end
