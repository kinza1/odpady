module Admin
  class DieselsController < BaseController
    before_action :set_and_authorize_diesel, only: [:edit, :update, :destroy]

    # GET /admin/diesel_categories/:category_id/diesels
    def index
      authorize [:admin, ::Diesel]
      @diesel_category = ::Diesel::Category.find(params[:diesel_category_id])
      @diesels = @diesel_category.diesels.order(:name).order(:year).page(params[:page]).per(25)
    end

    # GET /admin/diesel_categories/:category_id/diesels/new
    def new
      authorize [:admin, ::Diesel]
      @diesel_category = ::Diesel::Category.find(params[:diesel_category_id])
      @diesel = @diesel_category.diesels.new
    end

    # POST /admin/diesel_categories/:category_id/diesels
    def create
      authorize [:admin, ::Diesel]
      @diesel_category = ::Diesel::Category.find(params[:diesel_category_id])
      @diesel = @diesel_category.diesels.new(diesel_params)
      if @diesel.save
        redirect_to admin_diesel_category_diesels_path(@diesel_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/diesel_categories/:category_id/diesels/:id/edit
    def edit
      @diesel_category = ::Diesel::Category.find(params[:diesel_category_id])
    end

    # PATCH/PUT /admin/diesel_categories/:category_id/diesels/:id
    def update
      @diesel_category = ::Diesel::Category.find(params[:diesel_category_id])
      if @diesel.update(diesel_params)
        redirect_to admin_diesel_category_diesels_path(@diesel_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/diesel_categories/:category_id/diesels/:id
    def destroy
      @diesel_category = ::Diesel::Category.find(params[:diesel_category_id])
      if @diesel.destroy
        redirect_to admin_diesel_category_diesels_path(@diesel_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_diesel_category_diesels_path(@diesel_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_diesel
      @diesel = ::Diesel.find(params[:id])
      authorize [:admin, @diesel]
    end

    def diesel_params
      params.require(:diesels).permit(:name, :year, :value, :on)
    end
  end
end
