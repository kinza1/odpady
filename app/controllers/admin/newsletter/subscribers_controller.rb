require 'csv'

module Admin
  module Newsletter
    class SubscribersController < BaseController
      before_action :set_and_authorize_subscriber, only: [:edit, :update, :destroy]

      # GET /admin/newsletter/subscribers
      def index
        authorize [:admin, ::Newsletter::Subscriber]
        @search = SubscriberSearch.new(search_params)
        @subscribers = @search.results
        respond_to do |format|
          format.html
          format.csv { index_csv }
        end
      end

      # GET /admin/newsletter/subscribers/new
      def new
        authorize [:admin, ::Newsletter::Subscriber]
        @subscriber = ::Newsletter::Subscriber.new
      end

      # POST /admin/newsletter/subscribers
      def create
        authorize [:admin, ::Newsletter::Subscriber]
        @subscriber = ::Newsletter::Subscriber.new(subscriber_params)
        if @subscriber.save
          redirect_to admin_newsletter_subscribers_url, notice: t('shared.created')
        else
          render :new
        end
      end

      # GET /admin/newsletter/subscribers/:id/edit
      def edit
      end

      # PATCH/PUT /admin/newsletter/subscribers/:id
      def update
        if @subscriber.update(subscriber_params)
          redirect_to admin_newsletter_subscribers_url, notice: t('shared.updated')
        else
          render :edit
        end
      end

      # DELETE /admin/newsletter/subscribers/:id
      def destroy
        if @subscriber.destroy
          redirect_to admin_newsletter_subscribers_url, notice: t('shared.destroyed')
        else
          redirect_to admin_newsletter_subscribers_url, alert: t('shared.not_destroyed')
        end
      end

      private

      def index_csv
        @subscribers = @subscribers.enabled
        add_csv_headers('index.csv')
      end

      def add_csv_headers(name)
        headers['Content-Disposition'] = "attachment; filename=\"#{name}\""
        headers['Content-Type'] ||= 'text/csv'
      end

      def set_and_authorize_subscriber
        @subscriber = ::Newsletter::Subscriber.find(params[:id])
        authorize [:admin, @subscriber]
      end

      def subscriber_params
        params.require(:subscriber).permit(
          :email, :enabled, :position, :state_id, :branch_id, newsletter_category_ids: []
        )
      end
    end
  end
end
