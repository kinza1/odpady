module Admin
  module Newsletter
    class CategoriesController < BaseController
      before_action :set_and_authorize_category, only: [:edit, :update, :destroy]

      # GET /admin/newsletter/categories
      def index
        authorize [:admin, ::Newsletter::Category]
        @categories = ::Newsletter::Category.order(id: :desc)
      end

      # GET /admin/newsletter/categories/new
      def new
        authorize [:admin, ::Newsletter::Category]
        @category = ::Newsletter::Category.new
      end

      # POST /admin/newsletter/categories
      def create
        authorize [:admin, ::Newsletter::Category]
        @category = ::Newsletter::Category.new(category_params)
        if @category.save
          redirect_to admin_newsletter_categories_url, notice: t('shared.created')
        else
          render :new
        end
      end

      # GET /admin/newsletter/categories/:id/edit
      def edit
      end

      # PATCH/PUT /admin/newsletter/categories/:id
      def update
        if @category.update(category_params)
          redirect_to admin_newsletter_categories_url, notice: t('shared.updated')
        else
          render :edit
        end
      end

      # DELETE /admin/newsletter/categories/:id
      def destroy
        if @category.destroy
          redirect_to admin_newsletter_categories_url, notice: t('shared.destroyed')
        else
          redirect_to admin_newsletter_categories_url, alert: t('shared.not_destroyed')
        end
      end

      private

      def category_params
        params.require(:category).permit(:name)
      end

      def set_and_authorize_category
        @category = ::Newsletter::Category.find(params[:id])
        authorize [:admin, @category]
      end
    end
  end
end
