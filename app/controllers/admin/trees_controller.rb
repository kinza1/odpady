module Admin
  class TreesController < BaseController
    before_action :set_and_authorize_tree, only: [:edit, :update, :destroy]

    # GET /admin/trees
    def index
      authorize [:admin, :Tree]
      @trees = Tree.all
    end

    # GET /admin/trees/new
    def new
      authorize [:admin, :Tree]
      @tree = Tree.new
    end

    # POST /admin/trees
    def create
      authorize [:admin, :Tree]
      @tree = Tree.new(tree_params)
      if @tree.save
        redirect_to admin_trees_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/trees/:id/edit
    def edit
    end

    # PATCH/PUT /admin/trees/:id
    def update
      if @tree.update(tree_params)
        redirect_to admin_trees_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/trees/:id
    def destroy
      if @tree.destroy
        redirect_to admin_trees_url, notice: t('shared.destroyed')
      else
        redirect_to admin_trees_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_tree
      @tree = Tree.find(params[:id])
      authorize [:admin, @tree]
    end

    def tree_params
      params.require(:tree).permit(:name, :base)
    end
  end
end
