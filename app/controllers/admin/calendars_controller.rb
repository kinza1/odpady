module Admin
  class CalendarsController < BaseController
    before_action :set_and_authorize_calendar, only: [:edit, :update, :destroy]

    # GET /admin/calendars
    def index
      authorize [:admin, Calendar]
      @months = ['']
      (1..12).each { |m| @months << t('date.month_names')[m] }
      @search = CalendarSearch.new(search_params)
      @calendars = @search.results
    end

    # GET /admin/calendars/new
    def new
      authorize [:admin, Calendar]
      @calendar = Calendar.new
    end

    # POST /admin/calendars
    def create
      authorize [:admin, Calendar]
      @calendar = Calendar.new(calendar_params)
      if @calendar.save
        redirect_to admin_calendars_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/calendars/:id/edit
    def edit
    end

    # PATCH/PUT /admin/calendars/:id
    def update
      if @calendar.update(calendar_params)
        redirect_to admin_calendars_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/calendars/:id
    def destroy
      if @calendar.destroy
        redirect_to admin_calendars_url, notice: t('shared.destroyed')
      else
        redirect_to admin_calendars_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_calendar
      @calendar = Calendar.find(params[:id])
      authorize [:admin, @calendar]
    end

    def calendar_params
      params.require(:calendar).permit(
        :date, :concerned_to, :todo, :body
      )
    end
  end
end
