module Admin
  module Forum
    class SectionsController < BaseController
      before_action :set_and_authorize_section, only: [:edit, :update, :destroy]

      # GET /admin/forum/sections
      def index
        authorize [:admin, ::Forum::Section]
        @sections = ::Forum::Section.order(position: :asc)
      end

      # GET /admin/forum/sections/new
      def new
        authorize [:admin, ::Forum::Section]
        @section = ::Forum::Section.new
      end

      # POST /admin/forum/sections
      def create
        authorize [:admin, ::Forum::Section]
        @section = ::Forum::Section.new(section_params)
        if @section.save
          redirect_to admin_forum_sections_url, notice: t('shared.created')
        else
          render :new
        end
      end

      # GET /admin/forum/sections/:id/edit
      def edit
      end

      # PATCH/PUT /admin/forum/sections/:id
      def update
        if @section.update(section_params)
          redirect_to admin_forum_sections_url, notice: t('shared.updated')
        else
          render :edit
        end
      end

      # DELETE /admin/forum/sections/:id
      def destroy
        if @section.destroy
          redirect_to admin_forum_sections_url, notice: t('shared.destroyed')
        else
          redirect_to admin_forum_sections_url, alert: t('shared.not_destroyed')
        end
      end

      private

      def set_and_authorize_section
        @section = ::Forum::Section.find(params[:id])
        authorize [:admin, @section]
      end

      def section_params
        params.require(:section).permit(:name, :enabled)
      end
    end
  end
end
