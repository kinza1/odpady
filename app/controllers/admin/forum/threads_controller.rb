module Admin
  module Forum
    class ThreadsController < BaseController
      before_action :set_and_authorize_thread, only: [:edit, :close, :update, :destroy]

      # GET /admin/forum/threads
      def index
        authorize [:admin, ::Forum::Thread]
        @threads = ::Forum::Thread.order(created_at: :desc).page(params[:page]).per(25)
      end

      # GET /admin/forum/threads
      def show
        authorize [:admin, ::Forum::Thread]
        @thread = ::Forum::Thread.find(params[:id])
        @posts = @thread.posts.page(params[:page]).per(25)
      end

      # GET /admin/forum/threads/:id/edit
      def edit
      end

      # PATCH/PUT /admin/forum/threads/:id
      def update
        service = ThreadCheckService.new(@thread, current_user)
        if @thread.update(thread_params)
          service.check(@thread.closed)
          redirect_to admin_forum_threads_url, notice: t('shared.updated')
        else
          render :edit
        end
      end

      # DELETE /admin/forum/threads/:id
      def destroy
        if @thread.destroy
          redirect_to admin_forum_threads_url, notice: t('shared.destroyed')
        else
          redirect_to admin_forum_threads_url, alert: t('shared.not_destroyed')
        end
      end

      # GET /admin/forum/threads/:id/close
      def close
      end

      private

      def set_and_authorize_thread
        @thread = ::Forum::Thread.find(params[:id])
        authorize [:admin, @thread]
      end

      def thread_params
        params.require(:thread).permit(
          :subject, :section_id, :username, :notify_me,
          :approved, :closed, :closed_date, :close_body
        )
      end
    end
  end
end
