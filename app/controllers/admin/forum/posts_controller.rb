module Admin
  module Forum
    class PostsController < BaseController
      before_action :set_and_authorize_post, only: [:edit, :update, :destroy]

      # GET /admin/forum/posts
      def index
        authorize [:admin, ::Forum::Post]
        @posts = ::Forum::Post.order(id: :desc)
      end

      # GET /admin/forum/posts/:id/edit
      def edit
      end

      # PATCH/PUT /admin/forum/posts/:id
      def update
        if @post.update(post_params)
          redirect_to admin_forum_thread_path(@post.thread), notice: t('shared.updated')
        else
          render :edit
        end
      end

      # DELETE /admin/forum/posts/:id
      def destroy
        thread = @post.thread
        if @post.destroy
          redirect_to admin_forum_thread_path(thread), notice: t('shared.destroyed')
        else
          redirect_to admin_forum_thread_path(thread), alert: t('shared.not_destroyed')
        end
      end

      private

      def set_and_authorize_post
        @post = ::Forum::Post.find(params[:id])
        authorize [:admin, @post]
      end

      def post_params
        params.require(:post).permit(:username, :message, :approved)
      end
    end
  end
end
