module Admin
  class GazCategoriesController < BaseController
    before_action :set_and_authorize_gaz_category, only: [:edit, :update, :destroy]

    # GET /admin/gaz_categories
    def index
      authorize [:admin, ::Gaz::Category]
      @gaz_categories = ::Gaz::Category.all
    end

    # GET /admin/gaz_categories/new
    def new
      authorize [:admin, ::Gaz::Category]
      @gaz_category = ::Gaz::Category.new
    end

    # POST /admin/gaz_categories
    def create
      authorize [:admin, ::Gaz::Category]
      @gaz_category = ::Gaz::Category.new(gaz_category_params)
      if @gaz_category.save
        redirect_to admin_gaz_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/gaz_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/gaz_categories/:id
    def update
      if @gaz_category.update(gaz_category_params)
        redirect_to admin_gaz_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/gaz_categories/:id
    def destroy
      if @gaz_category.destroy
        redirect_to admin_gaz_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_gaz_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_gaz_category
      @gaz_category = ::Gaz::Category.find(params[:id])
      authorize [:admin, @gaz_category]
    end

    def gaz_category_params
      params.require(:gaz_category).permit(:name, :yearname, :subname)
    end
  end
end
