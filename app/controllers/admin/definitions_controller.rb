module Admin
  class DefinitionsController < BaseController
    before_action :set_and_authorize_definition, only: [:edit, :update, :destroy]

    # GET /admin/definitions
    def index
      authorize [:admin, Definition]
      @letters = Definition.letters
      @search = DefinitionSearch.new(search_params)
      @definitions = @search.results
    end

    # GET /admin/definitions/new
    def new
      authorize [:admin, Definition]
      @definition = Definition.new
    end

    # POST /admin/definitions
    def create
      authorize [:admin, Definition]
      @definition = Definition.new(definition_params)
      if @definition.save
        redirect_to admin_definitions_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/definitions/:id/edit
    def edit
    end

    # PATCH/PUT /admin/definitions/:id
    def update
      if @definition.update(definition_params)
        redirect_to admin_definitions_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/definitions/:id
    def destroy
      if @definition.destroy
        redirect_to admin_definitions_url, notice: t('shared.destroyed')
      else
        redirect_to admin_definitions_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_definition
      @definition = Definition.find(params[:id])
      authorize [:admin, @definition]
    end

    def definition_params
      params.require(:definition).permit(
        :name, :description, :source
      )
    end
  end
end
