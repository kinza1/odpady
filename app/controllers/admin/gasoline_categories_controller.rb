module Admin
  class GasolineCategoriesController < BaseController
    before_action :set_and_authorize_gasoline_category, only: [:edit, :update, :destroy]

    # GET /admin/gasoline_categories
    def index
      authorize [:admin, ::Gasoline::Category]
      @gasoline_categories = ::Gasoline::Category.all
    end

    # GET /admin/gasoline_categories/new
    def new
      authorize [:admin, ::Gasoline::Category]
      @gasoline_category = ::Gasoline::Category.new
    end

    # POST /admin/gasoline_categories
    def create
      authorize [:admin, ::Gasoline::Category]
      @gasoline_category = ::Gasoline::Category.new(gasoline_category_params)
      if @gasoline_category.save
        redirect_to admin_gasoline_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/gasoline_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/gasoline_categories/:id
    def update
      if @gasoline_category.update(gasoline_category_params)
        redirect_to admin_gasoline_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/gasoline_categories/:id
    def destroy
      if @gasoline_category.destroy
        redirect_to admin_gasoline_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_gasoline_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_gasoline_category
      @gasoline_category = ::Gasoline::Category.find(params[:id])
      authorize [:admin, @gasoline_category]
    end

    def gasoline_category_params
      params.require(:gasoline_category).permit(:name, :yearname)
    end
  end
end
