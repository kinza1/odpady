module Admin
  class WastesController < BaseController
    before_action :set_and_authorize_waste, only: [:edit, :update, :destroy]

    # GET /admin/wastes
    def index
      authorize [:admin, :Waste]
      @wastes = Waste.order(code: :asc)
                     .page(params[:page])
                     .per(25)
    end

    # GET /admin/wastes/new
    def new
      authorize [:admin, :Waste]
      @waste = Waste.new
    end

    # POST /admin/wastes
    def create
      authorize [:admin, :Waste]
      @waste = Waste.new(waste_params)
      if @waste.save
        redirect_to admin_wastes_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/wastes/:id/edit
    def edit
    end

    # PATCH/PUT /admin/wastes/:id
    def update
      if @waste.update(waste_params)
        redirect_to admin_wastes_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/wastes/:id
    def destroy
      if @waste.destroy
        redirect_to admin_wastes_url, notice: t('shared.destroyed')
      else
        redirect_to admin_wastes_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_waste
      @waste = Waste.find(params[:id])
      authorize [:admin, @waste]
    end

    def waste_params
      params.require(:waste).permit(
        :code, :name, :parent_id, :dry_mass, :disposal,
        :recycling, :org_fiz, :battery, :dangerous, :communal,
        waste_fees_attributes: [:id, :year, :value, :_destroy]
      )
    end
  end
end
