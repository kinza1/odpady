module Admin
  class ArticlesController < BaseController
    before_action :set_and_authorize_article, only: [:edit, :update, :destroy]

    # GET /admin/articles
    def index
      authorize [:admin, :Article]
      @articles = Article.order(date: :desc)
    end

    # GET /admin/articles/new
    def new
      authorize [:admin, :Article]
      @article = Article.new(date: Time.current.midday)
    end

    # POST /admin/articles
    def create
      authorize [:admin, :Article]
      @article = Article.new(article_params)
      if @article.save
        redirect_to admin_articles_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/articles/:id/edit
    def edit
    end

    # PATCH/PUT /admin/articles/:id
    def update
      if @article.update(article_params)
        redirect_to admin_articles_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/articles/:id
    def destroy
      if @article.destroy
        redirect_to admin_articles_url, notice: t('shared.destroyed')
      else
        redirect_to admin_articles_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_article
      @article = Article.find(params[:id])
      authorize [:admin, @article]
    end

    def article_params
      params.require(:article).permit(
        :name, :body, :image, :date, :preview,
        :published, recommendation_ids: []
      )
    end
  end
end
