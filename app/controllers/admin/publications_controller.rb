module Admin
  class PublicationsController < BaseController
    before_action :set_and_authorize_publication, only: [:edit, :update, :destroy]

    # GET /admin/publications
    def index
      authorize [:admin, Publication]
      @publications = Publication.page(params[:page]).per(25)
    end

    # GET /admin/publications/new
    def new
      authorize [:admin, Publication]
      @publication = Publication.new
    end

    # POST /admin/publications
    def create
      authorize [:admin, Publication]
      @publication = Publication.new(publication_params)
      if @publication.save
        redirect_to admin_publications_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/publications/:id/edit
    def edit
    end

    # PATCH/PUT /admin/publications/:id
    def update
      if @publication.update(publication_params)
        redirect_to admin_publications_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/publications/:id
    def destroy
      if @publication.destroy
        redirect_to admin_publications_url, notice: t('shared.destroyed')
      else
        redirect_to admin_publications_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_publication
      @publication = Publication.find(params[:id])
      authorize [:admin, @publication]
    end

    def publication_params
      params.require(:publication).permit(
        :name, :author, :pdf, :law
      )
    end
  end
end
