module Admin
  class DocumentsController < BaseController
    before_action :set_and_authorize_document, only: [:edit, :update, :destroy]

    # GET /admin/document_categories/:document_category_id/documents
    def index
      authorize [:admin, ::Document]
      @document_category = ::Document::Category.find(params[:document_category_id])
      @documents = @document_category.documents
    end

    # GET /admin/document_categories/:document_category_id/documents/new
    def new
      authorize [:admin, ::Document]
      @document_category = ::Document::Category.find(params[:document_category_id])
      @document = @document_category.documents.new
    end

    # POST /admin/document_categories/:document_category_id/documents
    def create
      authorize [:admin, ::Document]
      @document_category = ::Document::Category.find(params[:document_category_id])
      @document = @document_category.documents.new(document_params)
      if @document.save
        redirect_to admin_document_category_documents_url(@document_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/document_categories/:document_category_id/documents/:id/edit
    def edit
      @document_category = ::Document::Category.find(params[:document_category_id])
    end

    # PATCH/PUT /admin/document_categories/:document_category_id/documents/:id
    def update
      @document_category = ::Document::Category.find(params[:document_category_id])
      if @document.update(document_params)
        redirect_to admin_document_category_documents_url(@document_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/document_categories/:document_category_id/documents/:id
    def destroy
      @document_category = ::Document::Category.find(params[:document_category_id])
      if @document.destroy
        redirect_to admin_document_category_documents_url(@document_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_document_category_documents_url(@document_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_document
      @document = ::Document.find(params[:id])
      authorize [:admin, @document]
    end

    def document_params
      params.require(:document).permit(:name, :doc, :pdf)
    end
  end
end
