module Admin
  class EnvironmentWaterCategoriesController < BaseController
    before_action :set_and_authorize_environment_water_category, only: [:edit, :update, :destroy]

    # GET /admin/environment_water_categories
    def index
      authorize [:admin, ::EnvironmentWater::Category]
      @environment_water_categories = ::EnvironmentWater::Category.all
    end

    # GET /admin/environment_water_categories/new
    def new
      authorize [:admin, ::EnvironmentWater::Category]
      @environment_water_category = ::EnvironmentWater::Category.new
    end

    # POST /admin/environment_water_categories
    def create
      authorize [:admin, ::EnvironmentWater::Category]
      @environment_water_category = ::EnvironmentWater::Category.new(environment_water_category_params)
      if @environment_water_category.save
        redirect_to admin_environment_water_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/environment_water_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/environment_water_categories/:id
    def update
      if @environment_water_category.update(environment_water_category_params)
        redirect_to admin_environment_water_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/environment_water_categories/:id
    def destroy
      if @environment_water_category.destroy
        redirect_to admin_environment_water_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_environment_water_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_environment_water_category
      @environment_water_category = ::EnvironmentWater::Category.find(params[:id])
      authorize [:admin, @environment_water_category]
    end

    def environment_water_category_params
      params.require(:environment_water_category).permit(:name, :yearname)
    end
  end
end
