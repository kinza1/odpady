module Admin
  class RecyclingsController < BaseController
    before_action :set_and_authorize_recycling, only: [:edit, :update, :destroy]

    # GET /admin/recycling_categories/:category_id/recyclings
    def index
      authorize [:admin, ::Recycling]
      @recycling_category = ::Recycling::Category.find(params[:recycling_category_id])
      @recyclings = @recycling_category.recyclings.order(:name).order(:year).page(params[:page]).per(25)
    end

    # GET /admin/recycling_categories/:category_id/recyclings/new
    def new
      authorize [:admin, ::Recycling]
      @recycling_category = ::Recycling::Category.find(params[:recycling_category_id])
      @recycling = @recycling_category.recyclings.new
    end

    # POST /admin/recycling_categories/:category_id/recyclings
    def create
      authorize [:admin, ::Recycling]
      @recycling_category = ::Recycling::Category.find(params[:recycling_category_id])
      @recycling = @recycling_category.recyclings.new(recycling_params)
      if @recycling.save
        redirect_to admin_recycling_category_recyclings_path(@recycling_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/recycling_categories/:category_id/recyclings/:id/edit
    def edit
      @recycling_category = ::Recycling::Category.find(params[:recycling_category_id])
    end

    # PATCH/PUT /admin/recycling_categories/:category_id/recyclings/:id
    def update
      @recycling_category = ::Recycling::Category.find(params[:recycling_category_id])
      if @recycling.update(recycling_params)
        redirect_to admin_recycling_category_recyclings_path(@recycling_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/recycling_categories/:category_id/recyclings/:id
    def destroy
      @recycling_category = ::Recycling::Category.find(params[:recycling_category_id])
      if @recycling.destroy
        redirect_to admin_recycling_category_recyclings_path(@recycling_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_recycling_category_recyclings_path(@recycling_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_recycling
      @recycling = ::Recycling.find(params[:id])
      authorize [:admin, @recycling]
    end

    def recycling_params
      params.require(:recyclings).permit(:name, :year, :value)
    end
  end
end
