module Admin
  class CoefficientsController < BaseController
    before_action :set_and_authorize_coefficient, only: [:edit, :update, :destroy]

    # GET /admin/coefficients
    def index
      authorize [:admin, :Coefficient]
      @coefficients = Coefficient.all
    end

    # GET /admin/coefficients/new
    def new
      authorize [:admin, :Coefficient]
      @coefficient = Coefficient.new
    end

    # POST /admin/coefficients
    def create
      authorize [:admin, :Coefficient]
      @coefficient = Coefficient.new(coefficient_params)
      if @coefficient.save
        redirect_to admin_coefficients_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/coefficients/:id/edit
    def edit
    end

    # PATCH/PUT /admin/coefficients/:id
    def update
      if @coefficient.update(coefficient_params)
        redirect_to admin_coefficients_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/coefficients/:id
    def destroy
      if @coefficient.destroy
        redirect_to admin_coefficients_url, notice: t('shared.destroyed')
      else
        redirect_to admin_coefficients_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_coefficient
      @coefficient = Coefficient.find(params[:id])
      authorize [:admin, @coefficient]
    end

    def coefficient_params
      params.require(:coefficient).permit(:year, :float)
    end
  end
end
