module Admin
  class RemovalsController < BaseController
    before_action :set_and_authorize_removal, only: [:edit, :update, :destroy]

    # GET /admin/removal_categories/:catproj_id/removals
    def index
      authorize [:admin, ::Removal]
      @removal_category = ::Removal::Category.find(params[:removal_category_id])
      @removals = @removal_category.removals
    end

    # GET /admin/removal_categories/:catproj_id/removals/new
    def new
      authorize [:admin, ::Removal]
      @removal_category = ::Removal::Category.find(params[:removal_category_id])
      @removal = @removal_category.removals.new
    end

    # POST /admin/removal_categories/:catproj_id/removals
    def create
      authorize [:admin, ::Removal]
      @removal_category = ::Removal::Category.find(params[:removal_category_id])
      @removal = @removal_category.removals.new(removal_params)
      if @removal.save
        redirect_to admin_removal_category_removals_path(@removal_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/removal_categories/:catproj_id/removals/:id/edit
    def edit
      @removal_category = ::Removal::Category.find(params[:removal_category_id])
    end

    # PATCH/PUT /admin/removal_categories/:catproj_id/removals/:id
    def update
      @removal_category = ::Removal::Category.find(params[:removal_category_id])
      if @removal.update(removal_params)
        redirect_to admin_removal_category_removals_path(@removal_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/removal_categories/:catproj_id/removals/:id
    def destroy
      @removal_category = ::Removal::Category.find(params[:removal_category_id])
      if @removal.destroy
        redirect_to admin_removal_category_removals_path(@removal_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_removal_category_removals_path(@removal_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_removal
      @removal = ::Removal.find(params[:id])
      authorize [:admin, @removal]
    end

    def removal_params
      params.require(:removal).permit(:removal_category_id, :year, :value, :name)
    end
  end
end
