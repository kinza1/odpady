module Admin
  class UsefulLinksController < BaseController
    before_action :set_and_authorize_useful_link, only: [:edit, :update, :destroy]

    # GET /admin/useful_links
    def index
      authorize [:admin, :UsefulLink]
      @useful_links = UsefulLink.page(params[:page]).per(25)
    end

    # GET /admin/useful_links/new
    def new
      authorize [:admin, :UsefulLink]
      @useful_link = UsefulLink.new
    end

    # POST /admin/useful_links
    def create
      authorize [:admin, :UsefulLink]
      @useful_link = UsefulLink.new(useful_link_params)
      if @useful_link.save
        redirect_to admin_useful_links_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/useful_links/:id/edit
    def edit
    end

    # PATCH/PUT /admin/useful_links/:id
    def update
      if @useful_link.update(useful_link_params)
        redirect_to admin_useful_links_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/useful_links/:id
    def destroy
      if @useful_link.destroy
        redirect_to admin_useful_links_url, notice: t('shared.destroyed')
      else
        redirect_to admin_useful_links_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_useful_link
      @useful_link = UsefulLink.find(params[:id])
      authorize [:admin, @useful_link]
    end

    def useful_link_params
      params.require(:useful_link).permit(:link)
    end
  end
end
