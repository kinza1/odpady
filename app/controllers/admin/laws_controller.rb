module Admin
  class LawsController < BaseController
    before_action :set_and_authorize_law, only: [:edit, :update, :destroy]

    # GET /admin/laws
    def index
      authorize [:admin, :Law]
      @laws = Law.all
    end

    # GET /admin/laws/new
    def new
      authorize [:admin, :Law]
      @law = Law.new
      @law.build_law_on_main
    end

    # POST /admin/laws
    def create
      authorize [:admin, :Law]
      @law = Law.new(law_params)
      if @law.save
        redirect_to admin_laws_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/laws/:id/edit
    def edit
      @law.law_on_main || @law.build_law_on_main
    end

    # PATCH/PUT /admin/laws/:id
    def update
      if @law.update(law_params)
        redirect_to admin_laws_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/laws/:id
    def destroy
      if @law.destroy
        redirect_to admin_laws_url, notice: t('shared.destroyed')
      else
        redirect_to admin_laws_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_law
      @law = Law.find(params[:id])
      authorize [:admin, @law]
    end

    def law_params
      params.require(:law).permit(
        :address, :title, :on_main, :pdf, :isap,
        law_on_main_attributes: [:_destroy, :id, :itemable_id, :itemable_type, :icon_id]
      )
    end
  end
end
