module Admin
  class UserCategoriesController < BaseController
    before_action :set_and_authorize_user_category, only: [:edit, :update, :destroy]

    # GET /admin/user_categories
    def index
      authorize [:admin, ::User::Category]
      @user_categories = ::User::Category.all
    end

    # GET /admin/user_categories/new
    def new
      authorize [:admin, ::User::Category]
      @user_category = ::User::Category.new
    end

    # POST /admin/user_categories
    def create
      authorize [:admin, ::User::Category]
      @user_category = ::User::Category.new(user_category_params)
      if @user_category.save
        redirect_to admin_user_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/user_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/user_categories/:id
    def update
      if @user_category.update(user_category_params)
        redirect_to admin_user_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/user_categories/:id
    def destroy
      if @user_category.destroy
        redirect_to admin_user_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_user_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_user_category
      @user_category = ::User::Category.find(params[:id])
      authorize [:admin, @user_category]
    end

    def user_category_params
      params.require(:user_category).permit(:name)
    end
  end
end
