module Admin
  class RemovalCategoriesController < BaseController
    before_action :set_and_authorize_removal_category, only: [:edit, :update, :destroy]

    # GET /admin/removal_categories
    def index
      authorize [:admin, ::Removal::Category]
      @removal_categories = ::Removal::Category.order(:name)
    end

    # GET /admin/removal_categories/new
    def new
      authorize [:admin, ::Removal::Category]
      @removal_category = ::Removal::Category.new
    end

    # POST /admin/removal_categories
    def create
      authorize [:admin, ::Removal::Category]
      @removal_category = ::Removal::Category.new(removal_category_params)
      if @removal_category.save
        redirect_to admin_removal_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/removal_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/removal_categories/:id
    def update
      if @removal_category.update(removal_category_params)
        redirect_to admin_removal_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/removal_categories/:id
    def destroy
      if @removal_category.destroy
        redirect_to admin_removal_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_removal_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_removal_category
      @removal_category = ::Removal::Category.find(params[:id])
      authorize [:admin, @removal_category]
    end

    def removal_category_params
      params.require(:removal_category).permit(:name, :yearname)
    end
  end
end
