module Admin
  class WaterCategoriesController < BaseController
    before_action :set_and_authorize_water_category, only: [:edit, :update, :destroy]

    # GET /admin/water_categories
    def index
      authorize [:admin, ::Water::Category]
      @water_categories = ::Water::Category.all
    end

    # GET /admin/water_categories/new
    def new
      authorize [:admin, ::Water::Category]
      @water_category = ::Water::Category.new
    end

    # POST /admin/water_categories
    def create
      authorize [:admin, ::Water::Category]
      @water_category = ::Water::Category.new(water_category_params)
      if @water_category.save
        redirect_to admin_water_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/water_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/water_categories/:id
    def update
      if @water_category.update(water_category_params)
        redirect_to admin_water_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/water_categories/:id
    def destroy
      if @water_category.destroy
        redirect_to admin_water_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_water_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_water_category
      @water_category = ::Water::Category.find(params[:id])
      authorize [:admin, @water_category]
    end

    def water_category_params
      params.require(:water_category).permit(:name, :yearname)
    end
  end
end
