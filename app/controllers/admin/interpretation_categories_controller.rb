module Admin
  class InterpretationCategoriesController < BaseController
    before_action :set_and_authorize_interpretation_category,
                  only: [:edit, :update, :destroy]

    # GET /admin/interpretation_categories
    def index
      authorize [:admin, ::Interpretation::Category]
      @interpretation_categories = ::Interpretation::Category.all
    end

    # GET /admin/interpretation_categories/new
    def new
      authorize [:admin, ::Interpretation::Category]
      @interpretation_category = ::Interpretation::Category.new
    end

    # POST /admin/interpretation_categories
    def create
      authorize [:admin, ::Interpretation::Category]
      @interpretation_category = ::Interpretation::Category.new(interpretation_category_params)
      if @interpretation_category.save
        redirect_to admin_interpretation_categories_url,
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/interpretation_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/interpretation_categories/:id
    def update
      if @interpretation_category.update(interpretation_category_params)
        redirect_to admin_interpretation_categories_url,
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/interpretation_categories/:id
    def destroy
      if @interpretation_category.destroy
        redirect_to admin_interpretation_categories_url,
                    notice: t('shared.destroyed')
      else
        redirect_to admin_interpretation_categories_url,
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_interpretation_category
      @interpretation_category = ::Interpretation::Category.find(params[:id])
      authorize [:admin, @interpretation_category]
    end

    def interpretation_category_params
      params.require(:interpretation_category).permit(:name)
    end
  end
end
