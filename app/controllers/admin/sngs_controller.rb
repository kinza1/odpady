module Admin
  class SngsController < BaseController
    before_action :set_and_authorize_sng, only: [:edit, :update, :destroy]

    # GET /admin/sng_categories/:category_id/sngs
    def index
      authorize [:admin, ::Sng]
      @sng_category = ::Sng::Category.find(params[:sng_category_id])
      @sngs = @sng_category.sngs.order(:name).order(:year).page(params[:page]).per(25)
    end

    # GET /admin/sng_categories/:category_id/sngs/new
    def new
      authorize [:admin, ::Sng]
      @sng_category = ::Sng::Category.find(params[:sng_category_id])
      @sng = @sng_category.sngs.new
    end

    # POST /admin/sng_categories/:category_id/sngs
    def create
      authorize [:admin, ::Sng]
      @sng_category = ::Sng::Category.find(params[:sng_category_id])
      @sng = @sng_category.sngs.new(sng_params)
      if @sng.save
        redirect_to admin_sng_category_sngs_path(@sng_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/sng_categories/:category_id/sngs/:id/edit
    def edit
      @sng_category = ::Sng::Category.find(params[:sng_category_id])
    end

    # PATCH/PUT /admin/sng_categories/:category_id/sngs/:id
    def update
      @sng_category = ::Sng::Category.find(params[:sng_category_id])
      if @sng.update(sng_params)
        redirect_to admin_sng_category_sngs_path(@sng_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/sng_categories/:category_id/sngs/:id
    def destroy
      @sng_category = ::Sng::Category.find(params[:sng_category_id])
      if @sng.destroy
        redirect_to admin_sng_category_sngs_path(@sng_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_sng_category_sngs_path(@sng_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_sng
      @sng = ::Sng.find(params[:id])
      authorize [:admin, @sng]
    end

    def sng_params
      params.require(:sngs).permit(:name, :year, :value, :sng1)
    end
  end
end
