module Admin
  class ManualCategoriesController < BaseController
    before_action :set_and_authorize_category, only: [:edit, :update, :destroy]

    # GET /admin/manual_categories
    def index
      authorize [:admin, ::Manual::Category]
      @manual_categories = ::Manual::Category.all
    end

    # GET /admin/manual_categories/new
    def new
      authorize [:admin, ::Manual::Category]
      @manual_category = ::Manual::Category.new
    end

    # POST /admin/manual_categories
    def create
      authorize [:admin, ::Manual::Category]
      @manual_category = ::Manual::Category.new(category_params)
      if @manual_category.save
        redirect_to admin_manual_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/manual_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/manual_categories/:id
    def update
      if @manual_category.update(category_params)
        redirect_to admin_manual_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/manual_categories/:id
    def destroy
      if @manual_category.destroy
        redirect_to admin_manual_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_manual_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_category
      @manual_category = ::Manual::Category.find(params[:id])
      authorize [:admin, @manual_category]
    end

    def category_params
      params.require(:manual_category).permit(:name)
    end
  end
end
