module Admin
  class DieselCategoriesController < BaseController
    before_action :set_and_authorize_diesel_category, only: [:edit, :update, :destroy]

    # GET /admin/diesel_categories
    def index
      authorize [:admin, ::Diesel::Category]
      @diesel_categories = ::Diesel::Category.all
    end

    # GET /admin/diesel_categories/new
    def new
      authorize [:admin, ::Diesel::Category]
      @diesel_category = ::Diesel::Category.new
    end

    # POST /admin/diesel_categories
    def create
      authorize [:admin, ::Diesel::Category]
      @diesel_category = ::Diesel::Category.new(diesel_category_params)
      if @diesel_category.save
        redirect_to admin_diesel_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/diesel_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/diesel_categories/:id
    def update
      if @diesel_category.update(diesel_category_params)
        redirect_to admin_diesel_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/diesel_categories/:id
    def destroy
      if @diesel_category.destroy
        redirect_to admin_diesel_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_diesel_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_diesel_category
      @diesel_category = ::Diesel::Category.find(params[:id])
      authorize [:admin, @diesel_category]
    end

    def diesel_category_params
      params.require(:diesel_category).permit(:name, :yearname)
    end
  end
end
