module Admin
  class BaseController < ApplicationController
    layout 'admin'

    # Admin panel is restricted for non-admins
    before_action :authenticate_user!

    # Ensure policies are used
    after_action :verify_authorized
  end
end
