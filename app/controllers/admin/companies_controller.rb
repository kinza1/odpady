module Admin
  class CompaniesController < BaseController
    before_action :set_and_authorize_company, only: [:edit, :update, :destroy]

    # GET /admin/companies
    def index
      authorize [:admin, :Company]
      @search = CompanySearch.new(search_params)
      @companies = @search.results
    end

    # GET /admin/companies/:id/edit
    def edit
    end

    # PATCH/PUT /admin/companies/:id
    def update
      if @company.update(company_params)
        redirect_to admin_companies_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    private

    def set_and_authorize_company
      @company = Company.find(params[:id])
      authorize [:admin, @company]
    end

    def company_params
      params.require(:company).permit(
        :state_id, :name, :logo, :homepage, :email, :zipcode,
        :city, :street, :phone, :cellphone, :fax,
        :contact_person, :business_profile, :active, :full,
        branch_ids: []
      )
    end
  end
end
