module Admin
  class InterpretationsController < BaseController
    before_action :set_and_authorize_interpretation, only: [:edit, :update, :destroy]

    # GET /admin/interpretation_categories/:category_id/interpretations
    def index
      authorize [:admin, ::Interpretation]
      @interpretation_category = ::Interpretation::Category.find(params[:interpretation_category_id])
      @interpretations = @interpretation_category.interpretations
    end

    # GET /admin/interpretation_categories/:category_id/interpretations/new
    def new
      authorize [:admin, ::Interpretation]
      @interpretation_category = ::Interpretation::Category.find(params[:interpretation_category_id])
      @interpretation = @interpretation_category.interpretations.new
      @interpretation.build_law_on_main
    end

    # POST /admin/interpretation_categories/:category_id/interpretations
    def create
      authorize [:admin, ::Interpretation]
      @interpretation_category = ::Interpretation::Category.find(params[:interpretation_category_id])
      @interpretation = @interpretation_category.interpretations
                                                .new(interpretation_params)
      if @interpretation.save
        redirect_to admin_interpretation_category_interpretations_url(@interpretation_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/interpretation_categories/:category_id/interpretations/:id/edit
    def edit
      @interpretation_category = ::Interpretation::Category.find(params[:interpretation_category_id])
      @interpretation.law_on_main || @interpretation.build_law_on_main
    end

    # PATCH/PUT /admin/interpretation_categories/:category_id/interpretations/:id
    def update
      @interpretation_category = ::Interpretation::Category.find(params[:interpretation_category_id])
      if @interpretation.update(interpretation_params)
        redirect_to admin_interpretation_category_interpretations_url(@interpretation_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/interpretation_categories/:category_id/interpretations/:id
    def destroy
      @interpretation_category = ::Interpretation::Category.find(params[:interpretation_category_id])
      if @interpretation.destroy
        redirect_to admin_interpretation_category_interpretations_url(@interpretation_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_interpretation_category_interpretations_url(@interpretation_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_interpretation
      @interpretation = ::Interpretation.find(params[:id])
      authorize [:admin, @interpretation]
    end

    def interpretation_params
      params.require(:interpretation).permit(
        :interpretation_category_id, :pdf, :body,
        law_on_main_attributes: [:_destroy, :id, :itemable_id, :itemable_type, :icon_id]
      )
    end
  end
end
