module Admin
  class StatesController < BaseController
    before_action :set_and_authorize_state, only: [:edit, :update, :destroy]

    # GET /admin/states
    def index
      authorize [:admin, :State]
      @states = State.order(position: :asc)
    end

    # GET /admin/states/new
    def new
      authorize [:admin, :State]
      @state = State.new
    end

    # POST /admin/states
    def create
      authorize [:admin, :State]
      @state = State.new(state_params)
      if @state.save
        redirect_to admin_states_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/states/:id/edit
    def edit
    end

    # PATCH/PUT /admin/states/:id
    def update
      if @state.update(state_params)
        redirect_to admin_states_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/states/:id
    def destroy
      if @state.destroy
        redirect_to admin_states_url, notice: t('shared.destroyed')
      else
        redirect_to admin_states_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_state
      @state = State.find(params[:id])
      authorize [:admin, @state]
    end

    def state_params
      params.require(:state).permit(:name)
    end
  end
end
