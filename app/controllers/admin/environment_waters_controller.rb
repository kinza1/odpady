module Admin
  class EnvironmentWatersController < BaseController
    before_action :set_and_authorize_environment_water, only: [:edit, :update, :destroy]

    # GET /admin/environment_water_categories/:category_id/environment_waters
    def index
      authorize [:admin, ::EnvironmentWater]
      @environment_water_category = ::EnvironmentWater::Category.find(params[:environment_water_category_id])
      @environment_waters = @environment_water_category.environment_waters.order(:name).order(:year).page(params[:page]).per(25)
    end

    # GET /admin/environment_water_categories/:category_id/environment_waters/new
    def new
      authorize [:admin, ::EnvironmentWater]
      @environment_water_category = ::EnvironmentWater::Category.find(params[:environment_water_category_id])
      @environment_water = @environment_water_category.environment_waters.new
    end

    # POST /admin/environment_water_categories/:category_id/environment_waters
    def create
      authorize [:admin, ::EnvironmentWater]
      @environment_water_category = ::EnvironmentWater::Category.find(params[:environment_water_category_id])
      @environment_water = @environment_water_category.environment_waters.new(environment_water_params)
      if @environment_water.save
        redirect_to admin_environment_water_category_environment_waters_path(@environment_water_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/environment_water_categories/:category_id/environment_waters/:id/edit
    def edit
      @environment_water_category = ::EnvironmentWater::Category.find(params[:environment_water_category_id])
    end

    # PATCH/PUT /admin/environment_water_categories/:category_id/environment_waters/:id
    def update
      @environment_water_category = ::EnvironmentWater::Category.find(params[:environment_water_category_id])
      if @environment_water.update(environment_water_params)
        redirect_to admin_environment_water_category_environment_waters_path(@environment_water_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/environment_water_categories/:category_id/environment_waters/:id
    def destroy
      @environment_water_category = ::EnvironmentWater::Category.find(params[:environment_water_category_id])
      if @environment_water.destroy
        redirect_to admin_environment_water_category_environment_waters_path(@environment_water_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_environment_water_category_environment_waters_path(@environment_water_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_environment_water
      @environment_water = ::EnvironmentWater.find(params[:id])
      authorize [:admin, @environment_water]
    end

    def environment_water_params
      params.require(:environment_waters).permit(:name, :year, :sum)
    end
  end
end
