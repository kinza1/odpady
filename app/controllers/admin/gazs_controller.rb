module Admin
  class GazsController < BaseController
    before_action :set_and_authorize_gaz, only: [:edit, :update, :destroy]

    # GET /admin/gaz_categories/:category_id/gazs
    def index
      authorize [:admin, ::Gaz]
      @gaz_category = ::Gaz::Category.find(params[:gaz_category_id])
      @gazs = @gaz_category.gazs.order(:name).order(:year).page(params[:page]).per(25)
    end

    # GET /admin/gaz_categories/:category_id/gazs/new
    def new
      authorize [:admin, ::Gaz]
      @gaz_category = ::Gaz::Category.find(params[:gaz_category_id])
      @gaz = @gaz_category.gazs.new
    end

    # POST /admin/gaz_categories/:category_id/gazs
    def create
      authorize [:admin, ::Gaz]
      @gaz_category = ::Gaz::Category.find(params[:gaz_category_id])
      @gaz = @gaz_category.gazs.new(gaz_params)
      if @gaz.save
        redirect_to admin_gaz_category_gazs_path(@gaz_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/gaz_categories/:category_id/gazs/:id/edit
    def edit
      @gaz_category = ::Gaz::Category.find(params[:gaz_category_id])
    end

    # PATCH/PUT /admin/gaz_categories/:category_id/gazs/:id
    def update
      @gaz_category = ::Gaz::Category.find(params[:gaz_category_id])
      if @gaz.update(gaz_params)
        redirect_to admin_gaz_category_gazs_path(@gaz_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/gaz_categories/:category_id/gazs/:id
    def destroy
      @gaz_category = ::Gaz::Category.find(params[:gaz_category_id])
      if @gaz.destroy
        redirect_to admin_gaz_category_gazs_path(@gaz_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_gaz_category_gazs_path(@gaz_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_gaz
      @gaz = ::Gaz.find(params[:id])
      authorize [:admin, @gaz]
    end

    def gaz_params
      params.require(:gazs).permit(:name, :year, :value, :power)
    end
  end
end
