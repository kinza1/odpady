module Admin
  class WatersController < BaseController
    before_action :set_and_authorize_water, only: [:edit, :update, :destroy]

    # GET /admin/water_categories/:category_id/waters
    def index
      authorize [:admin, ::Water]
      @water_category = ::Water::Category.find(params[:water_category_id])
      @waters = @water_category.waters.order(:name).order(:year).page(params[:page]).per(25)
    end

    # GET /admin/water_categories/:category_id/waters/new
    def new
      authorize [:admin, ::Water]
      @water_category = ::Water::Category.find(params[:water_category_id])
      @water = @water_category.waters.new
    end

    # POST /admin/water_categories/:category_id/waters
    def create
      authorize [:admin, ::Water]
      @water_category = ::Water::Category.find(params[:water_category_id])
      @water = @water_category.waters.new(water_params)
      if @water.save
        redirect_to admin_water_category_waters_path(@water_category),
                    notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/water_categories/:category_id/waters/:id/edit
    def edit
      @water_category = ::Water::Category.find(params[:water_category_id])
    end

    # PATCH/PUT /admin/water_categories/:category_id/waters/:id
    def update
      @water_category = ::Water::Category.find(params[:water_category_id])
      if @water.update(water_params)
        redirect_to admin_water_category_waters_path(@water_category),
                    notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/water_categories/:category_id/waters/:id
    def destroy
      @water_category = ::Water::Category.find(params[:water_category_id])
      if @water.destroy
        redirect_to admin_water_category_waters_path(@water_category),
                    notice: t('shared.destroyed')
      else
        redirect_to admin_water_category_waters_path(@water_category),
                    alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_water
      @water = ::Water.find(params[:id])
      authorize [:admin, @water]
    end

    def water_params
      params.require(:waters).permit(:name, :year, :value)
    end
  end
end
