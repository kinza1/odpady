module Admin
  class DocumentCategoriesController < BaseController
    before_action :set_and_authorize_document_category, only: [:edit, :update, :destroy]

    # GET /admin/document_categories
    def index
      authorize [:admin, ::Document::Category]
      @document_categories = ::Document::Category.all
    end

    # GET /admin/document_categories/new
    def new
      authorize [:admin, ::Document::Category]
      @document_category = ::Document::Category.new
    end

    # POST /admin/document_categories
    def create
      authorize [:admin, ::Document::Category]
      @document_category = ::Document::Category.new(document_category_params)
      if @document_category.save
        redirect_to admin_document_categories_url, notice: t('shared.created')
      else
        render :new
      end
    end

    # GET /admin/document_categories/:id/edit
    def edit
    end

    # PATCH/PUT /admin/document_categories/:id
    def update
      if @document_category.update(document_category_params)
        redirect_to admin_document_categories_url, notice: t('shared.updated')
      else
        render :edit
      end
    end

    # DELETE /admin/document_categories/:id
    def destroy
      if @document_category.destroy
        redirect_to admin_document_categories_url, notice: t('shared.destroyed')
      else
        redirect_to admin_document_categories_url, alert: t('shared.not_destroyed')
      end
    end

    private

    def set_and_authorize_document_category
      @document_category = ::Document::Category.find(params[:id])
      authorize [:admin, @document_category]
    end

    def document_category_params
      params.require(:document_category).permit(:name)
    end
  end
end
