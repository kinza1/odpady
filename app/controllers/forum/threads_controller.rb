module Forum
  class ThreadsController < ApplicationController
    include Bannerable
    before_action :set_and_authorize_section, only: :index

    # GET /forum/sections/:section_id/threads
    def index
      authorize ::Forum::Thread
      @threads = @section.threads.approved.includes(:user).order(updated_at: :desc)
      @threads = @threads.decorate.page(params[:page]).per(20)
    end

    # GET /forum/sections/:section_id/threads/new
    def new
      authorize ::Forum::Thread
      @form = CreateThreadForm.new(
        user: current_user
      )
    end

    # POST /forum/sections/:section_id/threads
    def create
      authorize ::Forum::Thread
      @form = CreateThreadForm.new(
        user: current_user,
        params: create_thread_form_params
      )
      if @form.save
        redirect_to forum_thread_posts_path(@form.thread)
      else
        render :new
      end
    end

    private

    def set_and_authorize_section
      @section = ::Forum::Section.active.find(params[:section_id])
      authorize @section, :show?
    end

    def create_thread_form_params
      params.require(:thread).permit(
        :username, :notify_me, :subject, :section, :message
      )
    end
  end
end
