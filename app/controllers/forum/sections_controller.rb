module Forum
  class SectionsController < ApplicationController
    include Bannerable

    # GET /forum
    # GET /forum/sections
    def index
      authorize ::Forum::Section
      @sections = ::Forum::Section.active.order(position: :asc)
    end

    # rubocop:disable Metrics/AbcSize
    def show
      @section = ::Forum::Section.find(params[:id])
      @threads = @section.threads.approved
      @threads.closed.where('closed_date < ?', Time.zone.today).destroy_all
      @threads = @threads.page(params[:page]).per(20)
      authorize @section
    end
    # rubocop:enable Metrics/AbcSize
  end
end
