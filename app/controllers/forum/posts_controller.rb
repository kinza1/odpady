module Forum
  class PostsController < ApplicationController
    include Bannerable
    before_action :set_and_authorize_thread
    before_action :set_and_authorize_section
    before_action :check_approved, only: :index

    # GET /forum/threads/:thread_id/posts
    def index
      authorize ::Forum::Post
      @main_post = @thread.posts.order(id: :asc).first
      @posts = @thread.posts
                      .where.not(id: @main_post.id)
                      .order(id: :asc)
                      .page(params[:page])
                      .per(10)
    end

    # POST /forum/threads/:thread_id/posts
    def create
      authorize ::Forum::Post
      @form = CreatePostForm.new(
        thread: @thread,
        user: current_user,
        params: create_post_form_params
      )
      @form.save
    end

    private

    def set_and_authorize_thread
      @thread = ::Forum::Thread.find(params[:thread_id]).decorate
      authorize @thread, :show?
    end

    def set_and_authorize_section
      @section = @thread.section
      authorize @section, :show?
    end

    def create_post_form_params
      params.require(:post).permit(:username, :message, :thread_id)
    end

    def check_approved
      redirect_to forum_sections_path unless @thread.approved
    end
  end
end
