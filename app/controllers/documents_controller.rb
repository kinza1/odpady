class DocumentsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /documents
  def index
    @documents = Document.order(name: :asc)
    @documents_by_categories = @documents.group_by(&:category)
  end
end
