class WelcomeController < ApplicationController
  include Bannerable

  # GET /
  def index
    authorize :welcome
    @articles = Article.published.order(date: :desc).limit(4)
    @links = Link.ordered
    @laws = LawOnMain.on_main_halfs
  end
end
