module Waters
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /waters/categories
    def index
      @categories = ::Water::Category.all
      @categoriesnames = @categories.map(&:name).uniq
    end
  end
end
