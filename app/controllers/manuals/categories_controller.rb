module Manuals
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /manuals/categories
    def index
      @categories = ::Manual::Category.order(name: :asc)
    end

    # GET /manuals/categories/:id
    def show
      @category = ::Manual::Category.find(params[:id])
    end
  end
end
