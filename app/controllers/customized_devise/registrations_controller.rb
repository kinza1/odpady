module CustomizedDevise
  class RegistrationsController < ::Devise::RegistrationsController
    skip_after_action :verify_authorized
    skip_after_action :verify_policy_scoped
    before_action :set_sign_in, only: [:new, :create]

    # GET /users/sign_up
    def new
      @form = UserRegistrationForm.new
    end

    # POST /users
    def create
      @form = UserRegistrationForm.new(user_registration_form_params)
      if @form.save
        @user = @form.user
        flash[:notice] = t('.success')
        respond_with @form.user, location: after_sign_up_path_for(@form.user)
      else
        render :new
      end
    end

    private

    def set_sign_in
      self.resource = resource_class.new(sign_in_params)
      clean_up_passwords(resource)
      yield resource if block_given?
    end

    def sign_in_params
      devise_parameter_sanitizer.sanitize(:sign_in)
    end

    def serialize_options(resource)
      methods = resource_class.authentication_keys.dup
      methods = methods.keys if methods.is_a?(Hash)
      methods << :password if resource.respond_to?(:password)
      { methods: methods, only: [:password] }
    end

    def user_registration_form_params
      params.require(:registration).permit(
        :username, :email, :password, :password_confirmation,
        :accept_rules, :accept_data_processing, :accept_advertisement
      )
    end
  end
end
