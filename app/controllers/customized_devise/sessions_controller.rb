module CustomizedDevise
  class SessionsController < ::Devise::SessionsController
    skip_after_action :verify_authorized
    skip_after_action :verify_policy_scoped
    before_action :set_registration_form, only: [:create, :new]

    private

    def set_registration_form
      @form = UserRegistrationForm.new
    end
  end
end
