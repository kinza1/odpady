module Diesels
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /diesels/categories
    def index
      @categories = ::Diesel::Category.all
    end
  end
end
