module Newsletter
  class SubscribersController < ApplicationController
    # POST /newsletter/subscribers
    def create
      authorize ::Newsletter::Subscriber
      @form = SubscribeToNewsletterForm.new(
        subscriber_params
      )

      if @form.save
        notify
        render json: { status: :ok }
      else
        render json: { errors: @form.errors }, status: :unprocessable_entity
      end
    end

    private

    def subscriber_params
      params.require(:subscriber).permit(
        :email, :accept_rules, :accept_data_processing,
        :accept_advertisement, :position, :state_id, :branch_id
      )
    end

    def notify
      EmailTemplate.find_by!(
        name: 'newsletter_subscription_for_admin'
      ).deliver('news@odpady-hepl.pl', email: @form.email)
      EmailTemplate.find_by!(
        name: 'newsletter_subscription_for_user'
      ).deliver(@form.email)
    end
  end
end
