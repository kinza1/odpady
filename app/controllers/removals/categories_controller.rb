module Removals
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /interpretations/removals
    def index
      @categories = ::Removal::Category.all
    end
  end
end
