class ManualsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /manual_categories/:category_id/manuals
  def index
    @manual_category = ::Manual::Category.find(params[:manual_category_id])
    @manuals = @manual_category.manuals
  end

  # GET /manuals/:id
  def show
    @manual = Manual.find(params[:id])
  end
end
