module Sngs
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized

    # GET /sngs/categories
    def index
      @categories = ::Sng::Category.all
    end
  end
end
