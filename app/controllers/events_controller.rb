class EventsController < ApplicationController
  include Bannerable
  skip_after_action :verify_authorized

  # GET /events
  def index
    @events = Event.published
    @events = @events.soon if params[:soon].present?
    @events = @events.old if params[:old].present?
    @events = @events.page(params[:page]).per(10)
  end

  # GET /events/:id
  def show
    @event = Event.published.find(params[:id])
  end
end
