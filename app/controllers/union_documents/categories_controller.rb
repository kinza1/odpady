module UnionDocuments
  class CategoriesController < ApplicationController
    include Bannerable

    skip_after_action :verify_authorized
    before_action :set_categories

    # GET /union_documents/categories
    def index
      @category = @categories.first
      @documents = @category.union_documents
      render :show
    end

    # GET /union_documents/categories/:id
    def show
      @category = UnionDocument::Category.find(params[:id])
      @documents = @category.union_documents
    end

    private

    def set_categories
      @categories = UnionDocument::Category.order(name: :asc)
    end
  end
end
