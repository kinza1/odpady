class ThreadCloseWorker
  include Sidekiq::Worker

  def perform(thread_id)
    threads = Forum::Thread.where(id: thread_id)
    try_to_destroy(threads.first) unless threads.empty?
  end

  private

  def try_to_destroy(thread)
    thread.destroy if thread.closed && thread.closed_date < Time.zone.now
  end
end
