class SubscribeToNewsletterForm
  include ActiveModel::Model
  include Virtus.model

  attribute :email, String
  attribute :position, String

  attribute :state_id, Integer
  attribute :branch_id, Integer

  validates :email, presence: true, format: { with: Devise.email_regexp }

  validates :accept_rules, :accept_data_processing,
            :accept_advertisement, acceptance: true

  def save
    prepare_data
    if valid?
      persist
    else
      false
    end
  end

  private

  def persist
    ActiveRecord::Base.transaction { persist! }
    true
  rescue
    false
  end

  def persist!
    if already_subscribed?
      persist_old
    else
      persist_new
    end
  end

  def persist_old
    return if old_subscription.enabled?
    old_subscription.update!(enabled: true)
  end

  def persist_new
    ::Newsletter::Subscriber.create!(email: email, position: position, branch_id: branch_id, state_id: state_id)
  end

  def prepare_data
    self.email = email.to_s.strip.downcase
  end

  def already_subscribed?
    old_subscription.present?
  end

  def old_subscription
    @old_subscription ||= ::Newsletter::Subscriber.find_by(email: email)
  end
end
