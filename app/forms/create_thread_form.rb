class CreateThreadForm
  include ActiveModel::Model
  include Virtus.model

  attribute :username, String
  attribute :notify_me, Boolean
  attribute :subject, String
  attribute :message, String
  attribute :section, ::Forum::Section
  attribute :user, User
  attribute :approved, Boolean

  # @!group Validations
  validates :subject, presence: true, length: { maximum: 60 }
  validates :message, presence: true, length: { maximum: 1500 }
  validates :section, presence: true
  validate :validate_username_not_taken
  # @!endgroup

  # @!group Guest-specific validations
  validates :notify_me, inclusion: { in: [false] }, if: :guest?
  validates :username, presence: true, length: { maximum: 12 }, if: :guest?
  # @!endgroup

  # @!group Registered user-specific validations
  validates :username, inclusion: { in: [nil, ''] }, unless: :guest?
  # @!endgroup

  def initialize(options = {})
    self.user = options[:user]
    self.section = options[:section]
    super(options[:params])
  end

  def save
    prepare_values
    if valid?
      persist
    else
      false
    end
  end

  def guest?
    user.nil?
  end

  def thread
    @thread ||= Forum::Thread.new(
      section_id: section,
      user: user,
      subject: subject,
      username: username,
      notify_me: notify_me,
      approved: approved
    )
    @thread
  end

  def post
    @post ||= thread.posts.build(
      user: user, username: username, message: message, approved: approved
    )
  end

  def sections
    Forum::Section.active.map { |s| [s.name, s.id] }
  end

  private

  def prepare_values
    if guest?
      self.notify_me = false
    else
      self.username = ''
      self.approved = true
    end
  end

  def persist
    ActiveRecord::Base.transaction { persist! }
    true
  rescue
    false
  end

  def persist!
    thread.save!
    post.save!
  end

  def validate_username_not_taken
    return if username.blank?
    errors.add(:username, :taken) if User.where(username: username).any?
  end
end
