class UserRegistrationForm
  include ActiveModel::Model
  include Virtus.model

  attribute :username, String
  attribute :email, String
  attribute :password, String
  attribute :password_confirmation, String

  # @!group Validations
  validates :username, presence: true

  validates :email, presence: true
  validates :email, format: { with: Devise.email_regexp }, allow_blank: true

  validates :password, presence: true
  validates :password, confirmation: true, allow_blank: true
  validates :password, length: { within: Devise.password_length }, allow_blank: true

  validates :accept_rules, acceptance: true
  validates :accept_data_processing, acceptance: true
  validates :accept_advertisement, acceptance: true

  validate :validate_username_uniqueness
  validate :validate_email_uniqueness
  # @!endgroup

  def save
    if valid?
      persist
    else
      false
    end
  end

  def user
    @user ||= User.new(attributes)
  end

  private

  def persist
    user.category = ::User::Category.first
    user.save
  end

  # @!group Custom validation rules
  def validate_username_uniqueness
    return if username.blank?
    return unless User.exists?(username: username)
    errors.add(:username, :taken)
  end

  def validate_email_uniqueness
    return if email.blank?
    return unless User.exists?(email: email)
    errors.add(:email, :taken)
  end
  # @!endgroup
end
