class CreatePostForm
  include ActiveModel::Model
  include Virtus.model

  attribute :username, String
  attribute :message, String

  attribute :thread_id, String
  attribute :thread, ::Forum::Thread
  attribute :user, User
  attribute :approved, Boolean

  # @!group Validations
  validates :message, presence: true, length: { maximum: 1500 }
  validates :thread, presence: true
  validate :validate_username_not_taken
  # @!endgroup

  # @!group Guest-specific validations
  validates :username, presence: true, length: { maximum: 12 }, if: :guest?
  # @!endgroup

  # @!group Registered user-specific validations
  validates :username, inclusion: { in: [nil, ''] }, unless: :guest?
  # @!endgroup

  def initialize(options = {})
    @persisted = false
    self.user = options[:user]
    self.thread = options[:thread]
    super(options[:params])
  end

  def save
    prepare_values
    if valid?
      persist
    else
      false
    end
  end

  def guest?
    user.nil?
  end

  def persisted?
    @persisted
  end

  def post
    @post ||= thread.posts.build(
      user: user, username: username,
      thread_id: thread.id, message: message,
      approved: approved
    )
  end

  private

  def prepare_values
    return if guest?
    self.approved = true
    self.username = ''
  end

  def persist
    ActiveRecord::Base.transaction { persist! }
    true
  rescue
    false
  end

  def persist!
    post.save!
    notify_topic_starter
    @persisted = true
  end

  def notify_topic_starter
    return unless thread.notify_me?
    return unless thread.user
    # @todo Notify user via e-mail here
  end

  def validate_username_not_taken
    return if username.blank?
    errors.add(:username, :taken) if User.where(username: username).any?
  end
end
