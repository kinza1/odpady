class CreateCompanyForm
  attr_reader :company

  def initialize(attrs = {})
    @company = attrs.fetch(:company, Company.new)
    super(attrs[:params])
  end

  include ActiveModel::Model
  include Virtus.model

  attribute :state_id, Integer
  attribute :name, String
  attribute :homepage, String
  attribute :email, String
  attribute :zipcode, String
  attribute :city, String
  attribute :street, String
  attribute :phone, String
  attribute :cellphone, String
  attribute :fax, String
  attribute :contact_person, String
  attribute :business_profile, String
  attribute :branch_ids, Array
  attribute :logo, ActionDispatch::Http::UploadedFile

  validates :name, :state_id, presence: true
  validates :business_profile, length: { maximum: 500 }
  validates :branch_ids, length: { maximum: 3 }

  def fake_logo
    logo.present? ? logo.original_filename : nil
  end

  def save
    set_company_attributes
    merge_logo_errors

    if valid?
      company.save
    else
      false
    end
  end

  private

  def set_company_attributes
    company.assign_attributes(attributes)
  end

  def merge_logo_errors
    company.valid?
    company.errors.each do |key, error|
      errors.add(key, error) if key.eql?(:logo)
    end
  end

  # rubocop:disable Metrics/MethodLength
  def attributes
    {
      state_id: state_id,
      name: name,
      logo: logo,
      homepage: homepage,
      email: email,
      zipcode: zipcode,
      city: city,
      street: street,
      phone: phone,
      cellphone: cellphone,
      fax: fax,
      contact_person: contact_person,
      business_profile: business_profile,
      branch_ids: branch_ids
    }
  end
  # rubocop:enable Metrics/MethodLength
end
