class WasteDecorator < BaseDecorator
  delegate_all

  def need_legend?
    legend_fields.any? do |field|
      send(field)
    end
  end

  def legend_fields
    %w(dry_mass disposal recycling org_fiz
       battery dangerous communal)
  end
end
