module Forum
  class ThreadDecorator < BaseDecorator
    delegate_all

    def username
      if guest?
        if object.approved
          object.username
        else
          I18n.t('forum.guest')
        end
      else
        user.username
      end
    end

    def last_post
      @last_post ||= posts.latest.first
    end

    def got_last
      last_post.present?
    end
  end
end
