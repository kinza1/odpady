module Forum
  class PostDecorator < BaseDecorator
    delegate_all

    def self.collection_decorator_class
      PaginatingDecorator
    end

    def username
      if guest?
        if object.approved
          object.username
        else
          I18n.t('forum.guest')
        end
      else
        user.username
      end
    end
  end
end
