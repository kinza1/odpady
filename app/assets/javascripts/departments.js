$(function(){
  $(".contact_details_showmore").each(function() {
      var self = $(this);
      $(this).click(function() {
          $(this).parent().parent().find(".contact_details__moreinfo").slideToggle('slow', function() {
              if ($(this).is(':visible')) {
                  self.find("span").text("ukryj");
                  self.find("img").css("transform", "rotate(180deg)");
              } else {
                  self.find("span").text("pokaż");
                  self.find("img").css("transform", "rotate(0deg)");
              }
          });
      });
  });
});
