// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require chosen-jquery
//= require ckeditor/init
//= require forum
//= require newsletter
//= require departments
//= require search
//= require companies
//= require wastes
//= require calculator
//= require toastr.min

$(document).ready(function() {
    // contact_details_showmore();
    // multiple_select();
    // tabs();
    // newsletter();
    // collapsed_categories();
    // show_forumthreads();
    // hide_forum_answer();
    nav_dropdowns();

    $('.chosen').chosen();
	
	$('.menu_icon').click(function(){
		$(this).toggleClass('active');
		$('.nav__menu').slideToggle(300);
	})
});

function nav_dropdowns() {
  $('.down-arrow').each(function() {
      $(this).click(function() {
          $(this).parent().find(".nav__menu__drop").slideToggle();
      });
      $(".down-arrow").blur(function() {
          $(this).parent().find(".nav__menu__drop").slideUp();
      });
  });
}
