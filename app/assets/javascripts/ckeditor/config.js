CKEDITOR.editorConfig = function(config){
  config.language = 'pl';

  config.toolbar_ForumToolbar = [
    { name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
    { name: 'styles', items : [ 'Styles','Format' ] },
    { name: 'links', items : [ 'Link','Unlink' ] },
    { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText' ] },
    { name: 'paragraph', items : ['Blockquote'] }
  ];

  config.toolbar = 'ForumToolbar';

  config.toolbar = 'AdminToolbar';

  config.toolbar_AdminToolbar = [
    { name: 'document', items : [ 'NewPage','Preview' ] },
    { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
    { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','Scayt' ] },
    { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'
                 ,'Iframe' ] },
                '/',
    { name: 'styles', items : [ 'Styles','Format' ] },
    { name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote' ] },
    { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
    { name: 'tools', items : [ 'Maximize','-','About' ] }
  ];
};
