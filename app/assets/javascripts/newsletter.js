$(function(){
  $('#subscribe_button').click(function(e){
    e.preventDefault();

    $('.newsletter-modal-box').removeClass('hide');
    $('.newsletter-modal.thanks').addClass('hide');
    $('.newsletter-modal.form').removeClass('hide');
    $('#subscriber_email').val($('#subscribe_email').val());
  });

  $('.newsletter_close.form').click(function(){
    $('.newsletter-modal-box').addClass('hide');
  });

  $('.newsletter_close.thanks').click(function(){
    $('.newsletter-modal-box').addClass('hide');
  });

  $('#new_subscriber').submit(function(e){
    var _this = this;
    e.preventDefault();
    $('.newsletter-modal__input.subscriber_email').removeClass('validate-error');
    $(this).find('div.error-span').remove();

    var form = $(this).serializeArray();
    var formObject = {};

    $.each(form, function(i, v) {
      formObject[v.name] = v.value;
    });

    $.ajax({
      url: '/newsletter/subscribers',
      type: 'POST',
      data: formObject,
      success: function(data) {
        $(_this)[0].reset();
        $('#subscribe_email').val('');
        $('.newsletter-modal.form').addClass('hide');
        $('.newsletter-modal.thanks').removeClass('hide');
      },
      error: function(data) {
        errors = data.responseJSON.errors;

        $.each(errors, function(key, errs){
          $('.subscriber_' + key).addClass('validate-error');

          $.each(errs, function(i, v){
            $('.subscriber_' + key).append('<div class="error-span">' + v + '</div>');
          });
        });
      }
    });

    return false;
  });
});
