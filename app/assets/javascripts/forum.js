$(function(){
  var hash = window.location.hash;

  console.log(hash);

  if (hash !== "") {
    $('html, body').animate({
      scrollTop: $(hash + "-post").offset().top
    }, 0);
  }

  $('.forum__btn-answer ').click(function(e){
    var name = $(this).parent('.answer__buttons').siblings('.top').find('p .answer__name').text();
    var text = $(this).parent('.answer__buttons').siblings('.answer__text').html();
    CKEDITOR.instances.post_message.insertHtml('<blockquote><cite>Do <b>' + name + ':</b></cite>' + text + '</blockquote><br/>')
    $('html,body').animate({scrollTop: $('#post_message').offset().top}, 'slow');
  });

  $('.forum__btn-quote').click(function(e){
    var text = $(this).parent('.answer__buttons').siblings('.answer__text').html()
    CKEDITOR.instances.post_message.insertHtml('<blockquote>' + text + '</blockquote><br/>')
    $('html,body').animate({scrollTop: $('#post_message').offset().top}, 'slow');
  });
});
