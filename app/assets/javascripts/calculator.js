$(function(){
  var coef = $('#radio-coef-1').data('float');
  var theme = 'Usunięcie drzew';
  var tree = $('#tree-select').val();
  var l = 0;
  var t = false;

  $('.radio-coef').change(function(){
    coef = $(this).data('float');

    calculate(coef, theme, tree, l, t);
  });

  $('#tree-select').change(function(){
    tree = $(this).val();

    calculate(coef, theme, tree, l, t);
  });

  $('.radio-theme').change(function(){
    theme = $(this).data('theme');

    if (theme === 'Usunięcie drzew') {
      $('.podaj-input').text('Podaj obwód w cm')
      $('.tree-select').show();
    } else {
      $('.podaj-input').text('Podaj powierzchnię w m2')
      $('.tree-select').hide();
    }

    if (theme === 'Usunięcie drzew' || theme === 'Usunięcie krzewów') {
      $('.tereny-select').show();
    } else {
      $('.tereny-select').hide();
    }

    calculate(coef, theme, tree, l, t);
  });

  $('#podaj').keyup(function(){
    l = $(this).val() || 0;

    calculate(coef, theme, tree, l, t);
  });

  $('#podaj').change(function(){
      l = $(this).val() || 0;

      calculate(coef, theme, tree, l, t);
  });

  $('.radio-t').change(function(){
    t = $(this).data('value') === 1;

    calculate(coef, theme, tree, l, t);
  });
});

var calculate = function(coef, theme, tree, l, t){
  if (theme === 'Usunięcie drzew') {
    var val;

    if (l <= 25) {
      val = 1;
    } else if (l <= 50) {
      val = 1.51;
    } else if (l <= 100) {
      val = 2.37;
    } else if (l <= 200) {
      val = 3.7;
    } else if (l <= 300) {
      val = 5.55;
    } else if (l <= 500) {
      val = 5.55;
    } else if (l <= 700) {
      val = 10;
    } else {
      val = 12.96;
    }

    result = tree * l * coef * val;

    if (t) {
      result *= 2;
    }

    result2 = result * 3;

    render_1(result, result2, theme);
  } else if (theme === 'Usunięcie krzewów') {
    result = 231.28 * l * coef;

    if (t) {
      result *= 2;
    }

    result2 = result * 3;

    render_1(result, result2, theme);
  } else if (theme === 'Zniszczenie trawnika') {
    result = 53.19 * l * coef;

    if (t) {
      result *= 2;
    }

    render_2(result, 'trawnika', l);
  } else if (theme === 'Zniszczenie kwitnika') {
    var val;

    if (coef === 1.0) {
      val = 1;
    } else {
      val = 1.0013;
    }

    result = 456.19 * l * coef * val;

    render_2(result, 'kwitnika', l);
  }
}

var render_1 = function(result, result2, theme) {
  result_text = result.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ').replace('.', ',');
  result_text2 = result2.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ').replace('.', ',');

  if (result < 0) {
    render_3();
  } else {
    if (theme === 'Usunięcie drzew')
      $('.charges__sum').html('<p><span class="l">Opłata za usunięcie drzewa</span><span class="r">' + result_text + ' zł</span></p><p><span class="l">Kara za usunięcie drzewa</span><span class="r">' + result_text2 + ' zł</span></p>');
    else
      $('.charges__sum').html('<p><span class="l">Opłata za usunięcie krzewów</span><span class="r">' + result_text + ' zł</span></p><p><span class="l">Kara za usunięcie krzewów</span><span class="r">' + result_text2 + ' zł</span></p>');
  }
}

var render_2 = function(result, type, l) {
  result_text = result.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ').replace('.', ',');
  if (result < 0) {
    render_3();
  } else {
    $('.charges__sum').html('<p><span class="l">Kara za zniszczenie ' + l +' m² ' + type + ':</span><span class="r">' + result_text + ' zł</span></p>');
  }
}

var render_3 = function() {
  $('#podaj').val('');
  $('.charges__sum').html('<p><span class="l">Wprowadź rozmiar, aby zobaczyć wynik.</span></p>')
}
