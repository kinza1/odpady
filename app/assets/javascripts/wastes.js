$(function(){
  $(".single__title").each(function(){
    $(this).click(function(){
      $(this).parent().find(".catalogue__collapsed__1").slideToggle();
    });
  });
  $(".single__title__1").each(function(){
    $(this).click(function(){
      $(this).parent().find(".catalogue__collapsed__2").slideToggle();
    });
  });
  $(".show-hide").each(function(){
    var self = $(this);
    $(this).click(function(){
      $(this).parent().parent().find('.catalogue__moreinfo').slideToggle('', function(){
        if ($(this).is(':visible')) {
            self.find("span").text("ukryj");
            self.find("img").css("transform", "rotate(180deg)");
        } else {
            self.find("span").text("pokaż");
            self.find("img").css("transform", "rotate(0deg)");
        }
      });
    });
  });

  var timeout;
  var boxes = [];
  var text = '';

  $('#waste-search').keyup(function(){
    var _this = this;

    clearTimeout(timeout);

    timeout = setTimeout(function(){
      text = $(_this).val();
      wasteFilter(boxes, text);
    }, 300);
  });

  $('.radio-custom').change(function(){
    var $this = $(this);

    if ($this.is(':checked')) {
      boxes.push($this.data('name'))
    } else {
      var i = boxes.indexOf($this.data('name'));
      if(i != -1) {
      	boxes.splice(i, 1);
      }
    }

    wasteFilter(boxes, text);
  });
});

var wasteFilter = function(boxes, text) {
  $('.tree-holder').hide();

  if (text !== '') {
    $.each($('.tree-holder'), function(_, item){
      $item = $(item);
      var s = $item.data('search');

      if (s.match(text) && (boxes.length == 0 || boxes.every(function(box){ return $item.data(box.replace('_', '-')) === "" }))) {
        $item.show();

        parent = $item.parent('.tree-holder')
        parent.show();

        parent = parent.parent('.tree-holder')
        parent.show();
      }
    });
  } else if (boxes.length > 0) {
    var selector = '';

    $.each(boxes, function(i, box){
      selector += '[data-' + box.replace('_', '-') + ']'
    });

    $.each($('.tree-holder' + selector), function(_, item){
      $item = $(item);

      $item.show();
      parent = $item.parent('.tree-holder')

      parent.show();
      parent = parent.parent('.tree-holder')

      parent.show();
    });
  } else if (boxes.length == 0) {
    $('.tree-holder').hide();
    $('.parent-tree-holder').show();
  }
}
