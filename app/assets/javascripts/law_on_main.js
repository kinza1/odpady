$(function(){
  check = $('.law_on_main_check').is(':checked');

  if (!check) {
    $('.law_on_main_fields').find('.radio_buttons').hide();
  }

  $('.law_on_main_check').change(function(e){
    check = $('.law_on_main_check').is(':checked');

    if (check) {
      $('.law_on_main_fields').find('.radio_buttons').show();
    } else {
      $('.law_on_main_fields').find('.radio_buttons').hide();
    }
  });
});
