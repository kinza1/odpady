require 'rails_helper'

RSpec.describe ThreadCheckService, type: :model do
  let(:thread) { create :forum_thread, closed_date: 7.days.ago }
  let(:user) { create :user }
  let(:service) { ThreadCheckService.new(thread, user) }

  context 'thread closing', worker: true do
    it '#check should add post and start worker' do
      expect { service.check(true) }.to change(ThreadCloseWorker.jobs, :size).by(1)
      expect(thread.posts.count).to eq(1)
    end
  end

  context 'thread opening', worker: true do
    let(:thread) { create :forum_thread, closed_date: 7.days.ago, closed: true }

    before do
      thread.posts.create(attributes_for(:forum_post))
    end

    it '#check should remove post and without worker' do
      expect { service.check(false) }.to change(ThreadCloseWorker.jobs, :size).by(0)
      expect(thread.posts.count).to eq(0)
    end
  end
end
