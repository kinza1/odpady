require 'rails_helper'

RSpec.describe Law, type: :model do
  it { is_expected.to validate_presence_of(:title) }
end
