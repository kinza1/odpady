require 'rails_helper'

RSpec.describe Banner, type: :model do
  it { is_expected.to validate_presence_of(:image) }
  it { is_expected.to validate_presence_of(:link) }
end
