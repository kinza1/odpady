require 'rails_helper'

RSpec.describe UnionDocument, type: :model do
  it { is_expected.to validate_presence_of(:title) }
end
