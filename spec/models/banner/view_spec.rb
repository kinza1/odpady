require 'rails_helper'

RSpec.describe Banner::View, type: :model do
  it { is_expected.to validate_presence_of(:banner) }
  it { is_expected.to validate_presence_of(:date) }
end
