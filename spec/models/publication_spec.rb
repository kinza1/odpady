require 'rails_helper'

RSpec.describe Publication, type: :model do
  it { is_expected.to validate_presence_of(:name) }
end
