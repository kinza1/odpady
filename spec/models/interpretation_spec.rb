require 'rails_helper'

RSpec.describe Interpretation, type: :model do
  it { is_expected.to validate_presence_of(:body) }
end
