require 'rails_helper'

RSpec.describe GovernmentPlan, type: :model do
  it 'validations' do
    is_expected.to validate_presence_of(:state_id)
  end

  it 'relations' do
    is_expected.to belong_to(:state)
  end
end
