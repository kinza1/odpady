require 'rails_helper'

RSpec.describe Forum::Thread, type: :model do
  it { is_expected.to validate_presence_of(:section) }
  it { is_expected.to validate_presence_of(:subject) }
  it { is_expected.to validate_length_of(:subject).is_at_most(60) }

  describe 'Username validation' do
    context 'When thread is created by a registered user' do
      subject { build(:forum_thread) }
      it { is_expected.to allow_values(nil, '').for(:username) }
      it { is_expected.not_to allow_values('User').for(:username) }
    end

    context 'When thread is created by a guest' do
      subject { build(:forum_thread, :guest) }
      it { is_expected.not_to allow_values(nil, '').for(:username) }
      it { is_expected.to allow_value('User').for(:username) }
    end
  end

  describe 'Notify-me flag validation' do
    context 'When thread is created by a registered user' do
      subject { build(:forum_thread) }
      it { is_expected.to allow_value(true, false).for(:notify_me) }
    end

    context 'When thread is created by a guest' do
      subject { build(:forum_thread, :guest) }
      it { is_expected.to allow_value(false).for(:notify_me) }
      it { is_expected.not_to allow_value(true).for(:notify_me) }
    end
  end
end
