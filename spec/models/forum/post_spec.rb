require 'rails_helper'

RSpec.describe Forum::Post, type: :model do
  it { is_expected.to validate_presence_of(:thread) }
  it { is_expected.to validate_presence_of(:message) }
  it { is_expected.to validate_length_of(:message).is_at_most(1500) }

  describe 'Username validation' do
    context 'When post is created by a registered user' do
      subject { build(:forum_post) }
      it { is_expected.to allow_values(nil, '').for(:username) }
      it { is_expected.not_to allow_values('User').for(:username) }
    end

    context 'When post is created by a guest' do
      subject { build(:forum_post, :guest) }
      it { is_expected.not_to allow_values(nil, '').for(:username) }
      it { is_expected.to allow_value('User').for(:username) }
    end
  end
end
