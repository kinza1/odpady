require 'rails_helper'

RSpec.describe Newsletter::Subscriber, type: :model do
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
  it { is_expected.to allow_values('test@example.com', '123@567.com').for(:email) }
  it { is_expected.not_to allow_values('@example.com', '123').for(:email) }
end
