require 'rails_helper'

RSpec.describe ArticleRecommendation, type: :model do
  it 'relations' do
    is_expected.to belong_to(:article)
    is_expected.to belong_to(:recommendation)
  end
end
