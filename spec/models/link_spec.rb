require 'rails_helper'

RSpec.describe Link, type: :model do
  it 'validations' do
    %w(label title url).each do |field|
      is_expected.to validate_presence_of(field.to_sym)
    end
  end

  describe 'class methods' do
    context '.ordered' do
      let(:first) { create :link, sort_order: 1 }
      let(:second) { create :link, sort_order: 2 }

      before do
        first
        second
      end

      it 'should return links in right order' do
        expect(Link.ordered.first).to eq first
      end
    end
  end
end
