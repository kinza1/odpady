require 'rails_helper'

RSpec.describe Calendar, type: :model do
  it { is_expected.to validate_presence_of(:date) }
  it { is_expected.to validate_presence_of(:concerned_to) }
  it { is_expected.to validate_presence_of(:concerned_to) }
end
