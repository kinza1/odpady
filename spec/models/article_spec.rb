require 'rails_helper'

RSpec.describe Article, type: :model do
  it 'validations' do
    %w(name body image date).each do |field|
      is_expected.to validate_presence_of(field.to_sym)
    end
  end

  it 'relations' do
    is_expected.to have_many(:article_recommendations)
    is_expected.to have_many(:recommendations)
  end
end
