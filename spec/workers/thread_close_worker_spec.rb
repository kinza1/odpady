require 'rails_helper'

RSpec.describe ThreadCloseWorker, type: :model do
  let(:thread) { create :forum_thread, closed_date: 8.days.ago, closed: true }

  it 'should remove thread' do
    Sidekiq::Testing.inline! do
      ThreadCloseWorker.perform_async(thread.id)

      expect { Forum::Thread.find(thread.id) }.to raise_exception(ActiveRecord::RecordNotFound)
    end
  end
end
