require 'rails_helper'
require 'capybara/rails'
require 'capybara/poltergeist'
require 'database_cleaner'
require 'chosen-rails/rspec'

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, window_size: [800, 600])
end
Capybara.javascript_driver = :poltergeist

RSpec.configure do |config|
  config.include Chosen::Rspec::FeatureHelpers, type: :feature
  config.use_transactional_fixtures = false

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each, js: true) do
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

  # Include helpers
  config.include FeatureHelpers::Login, type: :feature
end
