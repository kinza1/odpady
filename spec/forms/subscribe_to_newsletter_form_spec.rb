require 'rails_helper'

RSpec.describe SubscribeToNewsletterForm, type: :model do
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to allow_values('test@example.com', '123@567.com').for(:email) }
  it { is_expected.not_to allow_values('@example.com', '123').for(:email) }

  context 'With valid data' do
    subject { SubscribeToNewsletterForm.new(email: 'hello@example.com') }

    context 'When email is new' do
      it 'returns true' do
        expect(subject.save).to eq(true)
      end

      it 'creates new Newsletter::Subscriber record' do
        expect { subject.save }.to change(::Newsletter::Subscriber, :count).by(1)
        subscriber = ::Newsletter::Subscriber.last
        expect(subscriber.email).to eq('hello@example.com')
        expect(subscriber).to be_enabled
      end
    end

    context 'When email was already subscribed' do
      let!(:subscriber) { create(:newsletter_subscriber, email: 'hello@example.com', enabled: enabled) }

      context 'Subscriber is enabled' do
        let(:enabled) { true }

        it 'returns true' do
          expect(subject.save).to eq(true)
        end

        it 'does not create any new subscribers' do
          expect { subject.save }.not_to change(::Newsletter::Subscriber, :count)
        end

        it 'does not change subscriber activity flag' do
          expect { subject.save }.not_to change { subscriber.reload.enabled }
        end
      end

      context 'Subscriber is disabled' do
        let(:enabled) { false }

        it 'returns true' do
          expect(subject.save).to eq(true)
        end

        it 'does not create any new subscribers' do
          expect { subject.save }.not_to change(::Newsletter::Subscriber, :count)
        end

        it 'does not change subscriber activity flag' do
          expect { subject.save }.to change { subscriber.reload.enabled }.from(false).to(true)
        end
      end
    end
  end

  context 'With invalid data' do
    subject { SubscribeToNewsletterForm.new(email: 'invalid e-mail') }

    it 'returns false' do
      expect(subject.save).to eq(false)
    end

    it 'does not create any new subscribers' do
      expect { subject.save }.not_to change(::Newsletter::Subscriber, :count)
    end
  end
end
