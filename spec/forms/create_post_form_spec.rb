require 'rails_helper'

RSpec.describe CreatePostForm, type: :model do
  let(:user) { create(:user) }
  let!(:thread) { create(:forum_thread) }

  it { is_expected.to validate_presence_of(:thread) }
  it { is_expected.to validate_presence_of(:message) }
  it { is_expected.to validate_length_of(:message).is_at_most(1500) }

  describe 'Username presence validation' do
    context 'When post is created by a registered user' do
      subject { CreatePostForm.new(user: user) }
      it { is_expected.to allow_values(nil, '').for(:username) }
      it { is_expected.not_to allow_values('User').for(:username) }
    end

    context 'When thread is created by a guest' do
      subject { CreatePostForm.new(user: nil) }
      it { is_expected.not_to allow_values(nil, '').for(:username) }
      it { is_expected.to allow_value('User').for(:username) }
      it { is_expected.to validate_length_of(:username).is_at_most(12) }
    end
  end

  describe 'Username uniqueness validation' do
    subject { CreatePostForm.new(params: { username: 'Tester' }) }

    context 'When there is no user with the same username' do
      it 'is valid' do
        subject.valid?
        expect(subject.errors[:username]).to eq([])
      end
    end

    context 'When there is user with the same username' do
      before { create(:user, username: 'Tester') }

      it 'is invalid' do
        subject.valid?
        expect(subject.errors[:username]).to eq([t('errors.messages.taken')])
      end
    end
  end

  describe '#save' do
    context 'With valid data' do
      subject do
        CreatePostForm.new(
          user: user, thread: thread, params: {
            username: 'Guest', message: 'This is a reply'
          }
        )
      end

      shared_examples_for 'successful creation' do
        it 'returns true' do
          expect(subject.save).to eq(true)
        end

        it 'creates a new post' do
          expect { subject.save }.to change(Forum::Post, :count).by(1)
          post = subject.post
          expect(post.thread).to eq(thread)
          expect(post.user).to eq(user)
          expect(post.message).to eq('This is a reply')
        end
      end

      context 'By a registered user' do
        it_behaves_like 'successful creation'

        it 'resets username' do
          subject.save
          expect(subject.post.username).to eq('')
        end
      end

      context 'By a guest' do
        let(:user) { nil }
        it_behaves_like 'successful creation'

        it 'saves username' do
          subject.save
          expect(subject.post.username).to eq('Guest')
        end
      end
    end

    context 'With invalid data' do
      subject { CreatePostForm.new }

      it 'returns false' do
        expect(subject.save).to eq(false)
      end

      it 'does not create any posts' do
        expect { subject.save }.not_to change(Forum::Post, :count)
      end
    end
  end
end
