require 'rails_helper'

RSpec.describe CreateThreadForm, type: :model do
  let(:user) { create(:user) }
  let(:section) { create(:forum_section) }

  it { is_expected.to validate_presence_of(:section) }
  it { is_expected.to validate_presence_of(:subject) }
  it { is_expected.to validate_length_of(:subject).is_at_most(60) }
  it { is_expected.to validate_presence_of(:message) }
  it { is_expected.to validate_length_of(:message).is_at_most(1500) }

  describe 'Username presence validation' do
    context 'When thread is created by a registered user' do
      subject { CreateThreadForm.new(user: user) }
      it { is_expected.to allow_values(nil, '').for(:username) }
      it { is_expected.not_to allow_values('User').for(:username) }
    end

    context 'When thread is created by a guest' do
      subject { CreateThreadForm.new(user: nil) }
      it { is_expected.not_to allow_values(nil, '').for(:username) }
      it { is_expected.to allow_value('User').for(:username) }
      it { is_expected.to validate_length_of(:username).is_at_most(12) }
    end
  end

  describe 'Username uniqueness validation' do
    subject { CreateThreadForm.new(params: { username: 'Tester' }) }

    context 'When there is no user with the same username' do
      it 'is valid' do
        subject.valid?
        expect(subject.errors[:username]).to eq([])
      end
    end

    context 'When there is user with the same username' do
      before { create(:user, username: 'Tester') }

      it 'is invalid' do
        subject.valid?
        expect(subject.errors[:username]).to eq([t('errors.messages.taken')])
      end
    end
  end

  describe 'Notify-me flag validation' do
    context 'When thread is created by a registered user' do
      subject { CreateThreadForm.new(user: user) }
      it { is_expected.to allow_value(true, false).for(:notify_me) }
    end

    context 'When thread is created by a guest' do
      subject { CreateThreadForm.new(user: nil) }
      subject { build(:forum_thread, :guest) }
      it { is_expected.to allow_value(false).for(:notify_me) }
      it { is_expected.not_to allow_value(true).for(:notify_me) }
    end
  end

  describe '#save' do
    context 'With valid data' do
      subject do
        CreateThreadForm.new(
          user: user, section: section.id, params: {
            username: 'Guest', subject: 'New thread', message: 'Hello', notify_me: true
          }
        )
      end

      shared_examples_for 'successful creation' do
        it 'returns true' do
          subject.save
          expect(subject.save).to eq(true)
        end

        it 'creates a new thread' do
          expect { subject.save }.to change(Forum::Thread, :count).by(1)
          thread = subject.thread
          expect(thread.section).to eq(section)
          expect(thread.user).to eq(user)
          expect(thread.subject).to eq('New thread')
        end

        it 'creates a new post' do
          expect { subject.save }.to change(Forum::Post, :count).by(1)
          post = subject.post
          expect(post.thread).to eq(subject.thread)
          expect(post.user).to eq(user)
          expect(post.message).to eq('Hello')
        end
      end

      context 'By a registered user' do
        it_behaves_like 'successful creation'

        it 'resets username' do
          subject.save
          expect(subject.thread.username).to eq('')
          expect(subject.post.username).to eq('')
        end

        it 'saves notify_me flag' do
          subject.save
          expect(subject.thread.notify_me).to eq(true)
        end
      end

      context 'By a guest' do
        let(:user) { nil }
        it_behaves_like 'successful creation'

        it 'saves username' do
          subject.save
          expect(subject.thread.username).to eq('Guest')
          expect(subject.post.username).to eq('Guest')
        end

        it 'resets notify_me flag' do
          subject.save
          expect(subject.thread.notify_me).to eq(false)
        end
      end
    end

    context 'With invalid data' do
      subject { CreateThreadForm.new }

      it 'returns false' do
        expect(subject.save).to eq(false)
      end

      it 'does not create any threads' do
        expect { subject.save }.not_to change(Forum::Thread, :count)
      end

      it 'does not create any posts' do
        expect { subject.save }.not_to change(Forum::Post, :count)
      end
    end
  end
end
