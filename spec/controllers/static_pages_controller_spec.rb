require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  describe 'GET #regulamin1' do
    it 'returns http success' do
      get :regulamin1
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #regulamin2' do
    it 'returns http success' do
      get :regulamin2
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #reklama' do
    it 'returns http success' do
      get :reklama
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #dla_prasy' do
    it 'returns http success' do
      get :dla_prasy
      expect(response).to have_http_status(:success)
    end
  end
end
