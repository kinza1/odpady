FactoryGirl.define do
  factory :project_category, class: 'Project::Category' do
    sequence(:name) { |i| "Category #{i}" }
  end
end
