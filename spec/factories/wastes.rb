FactoryGirl.define do
  factory :waste do
    sequence(:code) { |i| "Code #{i}" }
    sequence(:name) { |i| "Name #{i}" }
  end
end
