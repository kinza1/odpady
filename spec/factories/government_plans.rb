FactoryGirl.define do
  factory :government_plan do
    state
    body 'MyText'
    image { File.open(Rails.root.join('spec/fixtures/company.png')) }
  end
end
