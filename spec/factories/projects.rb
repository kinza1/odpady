FactoryGirl.define do
  factory :project do
    association :category, factory: :project_category
    name 'MyString'
    body 'MyText'
    pdf 'https://www.mos.gov.pl/g2/big/2015_08/598410f95f0729e6ee385372f4eb0672.pdf'
  end
end
