FactoryGirl.define do
  factory :branch do
    sequence(:name) { |i| "Branch #{i}" }
  end
end
