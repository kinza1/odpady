FactoryGirl.define do
  factory :union_document do
    association :category, factory: :union_document_category
    title 'MyString'
    address 'MyText'
    pdf 'MyString'
    eur_lex 'MyString'
  end
end
