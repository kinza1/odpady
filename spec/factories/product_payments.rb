FactoryGirl.define do
  factory :product_payment do
    name 'MyString'
    year 1
    sum 1.5
    percentage 1.5
  end
end
