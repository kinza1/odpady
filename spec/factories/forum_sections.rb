FactoryGirl.define do
  factory :forum_section, class: 'Forum::Section' do
    sequence(:name) { |i| "Forum section #{i}" }
    position 1
    enabled true
  end
end
