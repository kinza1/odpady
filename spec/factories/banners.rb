FactoryGirl.define do
  factory :banner do
    description 'MyText'
    image { File.open(Rails.root.join('spec/fixtures/company.png')) }
    link 'http://google.com'
    top false
    sidebar false
    views_count 0
    clicks_count 0
  end
end
