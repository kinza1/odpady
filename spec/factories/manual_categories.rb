FactoryGirl.define do
  factory :manual_category, class: '::Manual::Category' do
    sequence(:name) { |i| "Category #{i}" }
  end
end
