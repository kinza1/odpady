FactoryGirl.define do
  factory :definition do
    sequence(:name) { |i| "Definition #{i}" }
    description 'MyString'
    source 'MyString'
  end
end
