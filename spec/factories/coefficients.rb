FactoryGirl.define do
  factory :coefficient do
    year 1
    float 1.5
  end
end
