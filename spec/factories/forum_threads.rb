FactoryGirl.define do
  factory :forum_thread, class: 'Forum::Thread' do
    user
    association :section, factory: :forum_section
    subject 'Jak segregować śmieci?'
    notify_me false

    trait :guest do
      user nil
      username 'Gość'
    end

    transient do
      topic_user nil
      topic_username 'Guest'
      topic_message 'Hello'
    end

    after :create do |thread, evaluator|
      thread.posts << create(:forum_post, thread: thread,
                                          user: evaluator.topic_user,
                                          username: evaluator.topic_username,
                                          message: evaluator.topic_message)
    end
  end
end
