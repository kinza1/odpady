FactoryGirl.define do
  factory :state do
    sequence(:name) { |i| "State #{i}" }
  end
end
