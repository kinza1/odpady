FactoryGirl.define do
  factory :department do
    name 'Adamiak, Żmuda and Dudziński'
    homepage 'http://kihn.net/estella_nolan'
    email 'isaiah.parker@kshlerin.com'
    zipcode '23-989'
    city 'Złoczew'
    adress 'ul. Górniak 13185'
    state
    phone '44-479-71-55'
    fax '77-443-52-97'
  end
end
