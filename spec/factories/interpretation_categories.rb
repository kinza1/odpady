FactoryGirl.define do
  factory :interpretation_category, class: 'Interpretation::Category' do
    sequence(:name) { |i| "Category #{i}" }
  end
end
