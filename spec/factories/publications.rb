FactoryGirl.define do
  factory :publication do
    name 'Pozwolenia emisyjne'
    author 'Marek GÓRSKI'
    pdf { File.open(Rails.root.join('spec/fixtures/publication.pdf')) }
  end
end
