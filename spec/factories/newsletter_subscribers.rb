FactoryGirl.define do
  factory :newsletter_subscriber, class: 'Newsletter::Subscriber' do
    sequence(:email) { |i| "mail-#{i}@example.com" }
    enabled true
  end
end
