FactoryGirl.define do
  factory :union_document_category, class: 'UnionDocument::Category' do
    name 'MyString'
  end
end
