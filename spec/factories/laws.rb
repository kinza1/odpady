FactoryGirl.define do
  factory :law do
    address 'MyString'
    title 'MyString'
    pdf { File.open(Rails.root.join('spec/fixtures/calendar.pdf')) }
    isap 'MyString'
  end
end
