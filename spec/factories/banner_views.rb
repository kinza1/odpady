FactoryGirl.define do
  factory :banner_view, class: 'Banner::View' do
    banner
    date { Time.current.midday }
    count 1
  end
end
