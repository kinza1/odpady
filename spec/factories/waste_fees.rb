FactoryGirl.define do
  factory :waste_fee do
    year 1
    value 1.5
    waste nil
  end
end
