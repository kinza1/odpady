FactoryGirl.define do
  factory :event do
    name 'MyString'
    body 'MyText'
    start_at '2016-06-23 19:21:57'
    image { File.open(Rails.root.join('spec/fixtures/event.png')) }
    published true
  end
end
