FactoryGirl.define do
  factory :banner_click, class: 'Banner::Click' do
    banner
    date { Time.current.midday }
    count 1
  end
end
