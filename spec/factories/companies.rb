FactoryGirl.define do
  factory :company do
    state
    sequence(:name) { |i| "Company #{i}" }
    logo { File.open(Rails.root.join('spec/fixtures/company.png')) }
    homepage 'http://will.org/britney_ortiz'
    email 'tom@thiel.net'
    zipcode '44-636'
    city 'Międzyrzec'
    street 'al. Olejniczak'
    phone '83-526-35-68'
    cellphone '88-196-92-31'
    fax '95-167-95-76'
    contact_person 'Noemi Frąckowiak'
    business_profile 'Cross-platform dedicated projection'
    active true
  end
end
