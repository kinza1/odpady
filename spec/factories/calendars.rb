FactoryGirl.define do
  factory :calendar do
    date '2016-07-08'
    concerned_to 'MyText'
    todo 'MyText'
    body 'MyText'
  end
end
