FactoryGirl.define do
  factory :document do
    association :category, factory: :document_category
    name 'MyString'
    doc { File.open(Rails.root.join('spec/fixtures/document.doc')) }
    pdf { File.open(Rails.root.join('spec/fixtures/document.pdf')) }
  end
end
