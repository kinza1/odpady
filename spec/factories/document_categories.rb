FactoryGirl.define do
  factory :document_category, class: 'Document::Category' do
    sequence(:name) { |i| "Category #{i}" }
  end
end
