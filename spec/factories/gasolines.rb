FactoryGirl.define do
  factory :gasolines do
    name 'MyString'
    year 1
    value 1.5
    bs false
    category nil
  end
end
