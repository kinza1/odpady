FactoryGirl.define do
  factory :link do
    label 'MyString'
    title 'MyString'
    url 'MyString'
    image 'MyString'
  end
end
