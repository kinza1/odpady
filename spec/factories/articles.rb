FactoryGirl.define do
  factory :article do
    name 'MyString'
    body 'MyText'
    preview 'Preview of the article'
    image { File.open(Rails.root.join('spec/fixtures/event.png')) }
    date { Time.current }
    published true
  end
end
