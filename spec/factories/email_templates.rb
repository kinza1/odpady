FactoryGirl.define do
  factory :email_template do
    sequence(:name) { |i| "template-#{i}" }
    subject 'MyString'
    body 'MyText'
  end
end
