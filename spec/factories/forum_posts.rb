FactoryGirl.define do
  factory :forum_post, class: 'Forum::Post' do
    association :thread, factory: :forum_thread
    user
    message 'Witam. Mam pytanie.'

    trait :guest do
      user nil
      username 'Gość'
    end
  end
end
