FactoryGirl.define do
  factory :water do
    name 'MyString'
    year 1
    value 1.5
    category nil
  end
end
