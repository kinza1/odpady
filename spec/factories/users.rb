FactoryGirl.define do
  factory :user do
    sequence(:username) { |i| "user-#{i}" }
    sequence(:email) { |i| "user-#{i}@example.com" }
    password 'qwerty123'
    admin false
    approved true

    factory :admin do
      admin true
    end
  end
end
