FactoryGirl.define do
  factory :manual do
    association :category, factory: :manual_category
    name 'MyString'
    description 'MyText'
    body 'MyText'
  end
end
