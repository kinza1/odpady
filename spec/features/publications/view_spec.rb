require 'feature_helper'

RSpec.feature 'View a list of publications' do
  given!(:publications) { create_list(:publication, 2) }
  scenario 'View a list of publications' do
    visit publications_path

    publications.each do |publication|
      within "#publication_#{publication.id}" do
        expect(page).to have_content(publication.name)
      end
    end
  end
end
