require 'feature_helper'

RSpec.feature 'Update a department' do
  given(:admin) { create(:admin) }
  given!(:department) { create(:department, name: 'Initial name') }

  scenario 'Admin updates a department' do
    login(admin)
    visit edit_admin_department_path(department)

    within 'form#edit_department' do
      fill_in 'department_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#department_#{department.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
