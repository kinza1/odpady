require 'feature_helper'

RSpec.feature 'View a list of departments' do
  given(:admin) { create(:admin) }
  given!(:departments) { create_list(:department, 2) }

  scenario 'Admin sees a list of departments' do
    login(admin)

    visit admin_departments_path

    departments.each do |department|
      within "#department_#{department.id}" do
        expect(page).to have_content(department.name)
      end
    end
  end
end
