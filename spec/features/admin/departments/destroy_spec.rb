require 'feature_helper'

RSpec.feature 'Destroy a department' do
  given(:admin) { create(:admin) }
  given!(:department) { create(:department) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin delete a department' do
    login(admin)
    visit edit_admin_department_path(department)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_departments_path)
    expect(page).to have_no_selector("#department_#{department.id}")
  end
end
