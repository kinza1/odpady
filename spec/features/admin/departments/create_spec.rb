require 'feature_helper'

RSpec.feature 'Create a new department' do
  given(:admin) { create(:admin) }
  given!(:state) { create(:state) }
  given(:add_button) { t('admin.departments.index.actions.add') }

  scenario 'Admin creates a department' do
    login(admin)
    visit admin_departments_path
    click_on add_button

    within 'form#new_department' do
      fill_in 'department_name', with: 'Więcek-Łukasiewicz'
      fill_in 'department_homepage', with: 'http://toy.biz/mya'
      fill_in 'department_email', with: 'jeika@borer.com'
      fill_in 'department_zipcode', with: '92-179'
      fill_in 'department_city', with: 'Gołdap'
      fill_in 'department_adress', with: 'ul. Zawada'
      select state.name, from: 'department[state_id]'
      fill_in 'department_phone', with: '42-482-38-80'
      fill_in 'department_fax', with: '65-334-02-93'
      find('input[type="submit"]').click
    end

    within '#departments' do
      expect(page).to have_content('Więcek-Łukasiewicz')
    end
  end
end
