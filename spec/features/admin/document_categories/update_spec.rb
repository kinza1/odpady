require 'feature_helper'

RSpec.feature 'Update a category' do
  given(:admin) { create(:admin) }
  given!(:document_category) { create(:document_category, name: 'Initial name') }

  scenario 'Admin updates a category' do
    login(admin)
    visit edit_admin_document_category_path(document_category)

    within 'form#edit_document_category' do
      fill_in 'document_category_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#document_category_#{document_category.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
