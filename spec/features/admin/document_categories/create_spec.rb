require 'feature_helper'

RSpec.feature 'Create a new document_category' do
  given(:admin) { create(:admin) }

  given(:add_button) { t('admin.document_categories.index.actions.add') }

  scenario 'Admin creates a document_category' do
    login(admin)
    visit admin_document_categories_path
    click_on add_button

    within 'form#new_document_category' do
      fill_in 'document_category_name', with: 'producenci'
      find('input[type="submit"]').click
    end

    within '#document_categories' do
      expect(page).to have_content('producenci')
    end
  end
end
