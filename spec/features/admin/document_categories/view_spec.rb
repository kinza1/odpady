require 'feature_helper'

RSpec.feature 'View a list of document_categories' do
  given(:admin) { create(:admin) }
  given!(:document_categories) { create_list(:document_category, 2) }

  scenario 'Admin sees a list of document_categories' do
    login(admin)

    visit admin_document_categories_path

    document_categories.each do |document_category|
      within "#document_category_#{document_category.id}" do
        expect(page).to have_content(document_category.name)
      end
    end
  end
end
