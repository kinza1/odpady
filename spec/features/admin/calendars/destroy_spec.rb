require 'feature_helper'

RSpec.feature 'Destroy a calendar' do
  given(:admin) { create(:admin) }
  given!(:calendar) { create(:calendar) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin delete a calendar' do
    login(admin)
    visit edit_admin_calendar_path(calendar)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_calendars_path)
    expect(page).to have_no_selector("#calendar_#{calendar.id}")
  end
end
