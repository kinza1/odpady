require 'feature_helper'

RSpec.feature 'Update a calendar' do
  given(:admin) { create(:admin) }
  given!(:calendar) { create(:calendar, concerned_to: 'Initial concerned_to') }

  scenario 'Admin updates a calendar' do
    login(admin)
    visit edit_admin_calendar_path(calendar)

    within 'form#edit_calendar' do
      fill_in 'calendar_concerned_to', with: 'New concerned_to'
      find('input[type="submit"]').click
    end

    within "#calendar_#{calendar.id}" do
      expect(page).to have_no_content('Initial concerned_to')
      expect(page).to have_content('New concerned_to')
    end
  end
end
