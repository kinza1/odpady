require 'feature_helper'

RSpec.feature 'View a list of calendars' do
  given(:admin) { create(:admin) }
  given!(:calendars) { create_list(:calendar, 2) }

  scenario 'Admin sees a list of calendars' do
    login(admin)

    visit admin_calendars_path

    calendars.each do |calendar|
      within "#calendar_#{calendar.id}" do
        expect(page).to have_content(calendar.concerned_to)
      end
    end
  end
end
