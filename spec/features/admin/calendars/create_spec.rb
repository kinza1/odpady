require 'feature_helper'

RSpec.feature 'Create a new calendar' do
  given(:admin) { create(:admin) }

  scenario 'Admin creates a calendar' do
    login(admin)
    visit new_admin_calendar_path

    within 'form#new_calendar' do
      fill_in 'calendar_concerned_to', with: 'concerned_to'
      fill_in 'calendar_todo', with: 'todo'
      select 'maj', from: 'calendar[date(2i)]'
      select '24', from: 'calendar[date(3i)]'
      fill_in 'calendar_body', with: 'body'
      find('input[type="submit"]').click
    end

    within '#calendars' do
      expect(page).to have_content('concerned_to')
    end
  end
end
