require 'feature_helper'

RSpec.feature 'Update a document' do
  given(:admin) { create(:admin) }
  given(:category) { create(:document_category) }
  given!(:document) { create(:document, name: 'Initial name', category: category) }

  scenario 'Admin updates a document' do
    login(admin)

    visit edit_admin_document_category_document_path(category, document)

    within 'form' do
      fill_in 'document_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#document_#{document.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
