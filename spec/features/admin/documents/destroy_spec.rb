require 'feature_helper'

RSpec.feature 'Destroy a category' do
  given(:admin) { create(:admin) }
  given!(:document_category) { create(:document_category) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin deletes a category' do
    login(admin)
    visit edit_admin_document_category_path(document_category)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_document_categories_path)
    expect(page).to have_no_selector("#document_category_#{document_category.id}")
  end
end
