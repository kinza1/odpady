require 'feature_helper'

RSpec.feature 'Create a new document_category' do
  given(:admin) { create(:admin) }
  given!(:category) { create(:document_category) }
  given(:add_button) { t('admin.documents.index.actions.add') }

  scenario 'Admin creates a document_category' do
    login(admin)
    visit admin_document_category_documents_path(category)
    click_on add_button

    within 'form#new_document' do
      fill_in 'document_name', with: 'producenci'
      page.attach_file 'document_doc', "#{Rails.root}/spec/fixtures/document.doc"
      page.attach_file 'document_pdf', "#{Rails.root}/spec/fixtures/document.pdf"
      find('input[type="submit"]').click
    end

    within '#documents' do
      expect(page).to have_content('producenci')
    end
  end
end
