require 'feature_helper'

RSpec.feature 'View a list of documents in admin panel' do
  given(:admin) { create(:admin) }
  given!(:category) { create(:document_category) }
  given!(:documents) { create_list(:document, 2, category: category) }

  scenario 'Admin sees a list of documents' do
    login(admin)

    visit admin_document_category_documents_path(category)

    documents.each do |document|
      within "#document_#{document.id}" do
        expect(page).to have_content(document.name)
      end
    end
  end
end
