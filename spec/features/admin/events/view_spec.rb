require 'feature_helper'

RSpec.feature 'View a list of events' do
  given(:admin) { create(:admin) }
  given!(:events) { create_list(:event, 2) }

  scenario 'Admin sees a list of events' do
    login(admin)

    visit admin_events_path

    events.each do |event|
      within "#event_#{event.id}" do
        expect(page).to have_content(event.name)
      end
    end
  end
end
