require 'feature_helper'

RSpec.feature 'Create a new event' do
  given(:admin) { create(:admin) }

  given(:add_button) { t('admin.events.index.actions.add') }

  scenario 'Admin creates an event' do
    login(admin)
    visit admin_events_path
    click_on add_button

    within 'form#new_event' do
      fill_in 'event_name', with: 'New event'
      fill_in 'event_body', with: 'One two three'
      select '2014', from: 'event[start_at(1i)]'
      select 'maj', from: 'event[start_at(2i)]'
      select '24', from: 'event[start_at(3i)]'
      select '19', from: 'event[start_at(4i)]'
      select '00', from: 'event[start_at(5i)]'
      page.attach_file 'event_image', "#{Rails.root}/spec/fixtures/event.png"
      find('input[type="submit"]').click
    end

    within '#events' do
      expect(page).to have_content('New event')
      expect(page).to have_content('2014-05-24 19:00')
    end
  end
end
