require 'feature_helper'

RSpec.feature 'Update a event' do
  given(:admin) { create(:admin) }
  given!(:event) { create(:event, name: 'Initial name') }

  scenario 'Admin updates a event' do
    login(admin)
    visit edit_admin_event_path(event)

    within 'form#edit_event' do
      fill_in 'event_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#event_#{event.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
