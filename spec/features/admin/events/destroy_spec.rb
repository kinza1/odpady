require 'feature_helper'

RSpec.feature 'Destroy a event' do
  given(:admin) { create(:admin) }
  given!(:event) { create(:event) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin destroys a event' do
    login(admin)
    visit edit_admin_event_path(event)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_events_path)
    expect(page).to have_no_selector("#event_#{event.id}")
  end
end
