require 'feature_helper'

RSpec.feature 'View a list of forum sections' do
  given(:admin) { create(:admin) }
  given!(:sections) { create_list(:forum_section, 2) }

  scenario 'Admin sees a list of forum sections' do
    login(admin)

    visit admin_forum_sections_path

    sections.each do |section|
      within "#forum_section_#{section.id}" do
        expect(page).to have_content(section.name)
      end
    end
  end
end
