require 'feature_helper'

RSpec.feature 'Destroy a forum section' do
  given(:admin) { create(:admin) }
  given!(:section) { create(:forum_section) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin updates a forum sections' do
    login(admin)
    visit edit_admin_forum_section_path(section)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_forum_sections_path)
    expect(page).to have_no_selector("#forum_section_#{section.id}")
  end
end
