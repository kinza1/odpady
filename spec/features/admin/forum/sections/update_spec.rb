require 'feature_helper'

RSpec.feature 'Update a forum section' do
  given(:admin) { create(:admin) }
  given!(:section) { create(:forum_section, name: 'Initial name') }

  scenario 'Admin updates a forum sections' do
    login(admin)
    visit edit_admin_forum_section_path(section)

    within 'form#edit_section' do
      fill_in 'section_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#forum_section_#{section.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
