require 'feature_helper'

RSpec.feature 'Create a new forum section' do
  given(:admin) { create(:admin) }

  given(:add_button) { t('admin.forum.sections.index.actions.add') }

  scenario 'Admin creates a new forum sections' do
    login(admin)
    visit admin_forum_sections_path
    click_on add_button

    within 'form#new_section' do
      fill_in 'section_name', with: 'Dyskusja ogólna'
      find('input[type="submit"]').click
    end

    within '#sections' do
      expect(page).to have_content('Dyskusja ogólna')
    end
  end
end
