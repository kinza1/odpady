require 'feature_helper'

RSpec.feature 'Create a new manual_category' do
  given(:admin) { create(:admin) }

  given(:add_button) { t('admin.manual_categories.index.actions.add') }

  scenario 'Admin creates a manual_category' do
    login(admin)
    visit admin_manual_categories_path
    click_on add_button

    within 'form#new_manual_category' do
      fill_in 'manual_category_name', with: 'producenci'
      find('input[type="submit"]').click
    end

    within '#manual_categories' do
      expect(page).to have_content('producenci')
    end
  end
end
