require 'feature_helper'

RSpec.feature 'Update a category' do
  given(:admin) { create(:admin) }
  given!(:manual_category) { create(:manual_category, name: 'Initial name') }

  scenario 'Admin updates a category' do
    login(admin)
    visit edit_admin_manual_category_path(manual_category)

    within 'form#edit_manual_category' do
      fill_in 'manual_category_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#manual_category_#{manual_category.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
