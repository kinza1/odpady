require 'feature_helper'

RSpec.feature 'View a list of manual_categories' do
  given(:admin) { create(:admin) }
  given!(:manual_categories) { create_list(:manual_category, 2) }

  scenario 'Admin sees a list of manual_categories' do
    login(admin)

    visit admin_manual_categories_path

    manual_categories.each do |manual_category|
      within "#manual_category_#{manual_category.id}" do
        expect(page).to have_content(manual_category.name)
      end
    end
  end
end
