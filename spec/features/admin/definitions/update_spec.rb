require 'feature_helper'

RSpec.feature 'Update a definition' do
  given(:admin) { create(:admin) }
  given!(:definition) { create(:definition, name: 'Initial name') }

  scenario 'Admin updates a definition' do
    login(admin)
    visit edit_admin_definition_path(definition)

    within 'form#edit_definition' do
      fill_in 'definition_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#definition_#{definition.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
