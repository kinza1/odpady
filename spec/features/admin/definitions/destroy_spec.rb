require 'feature_helper'

RSpec.feature 'Destroy a definition' do
  given(:admin) { create(:admin) }
  given!(:definition) { create(:definition) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin delete a definition' do
    login(admin)
    visit edit_admin_definition_path(definition)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_definitions_path)
    expect(page).to have_no_selector("#definition_#{definition.id}")
  end
end
