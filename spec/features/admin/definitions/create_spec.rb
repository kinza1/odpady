require 'feature_helper'

RSpec.feature 'Create a new definition' do
  given(:admin) { create(:admin) }
  given(:name) { 'Aglomeracja' }
  given(:description) { 'rozumie się przez to miasto lub kilka miast o wspólnych granicach administracyjnych' }
  given(:source) { 'Prawo ochrony środowiska Dz.U. 2001 Nr 62, poz. 627' }

  scenario 'Admin creates a definition' do
    login(admin)
    visit new_admin_definition_path

    within 'form#new_definition' do
      fill_in 'definition_name', with: name
      fill_in 'definition_description', with: description
      fill_in 'definition_source', with: source
      find('input[type="submit"]').click
    end

    within "#definition_#{Definition.last.id}" do
      expect(page).to have_content(name)
      expect(page).to have_content(description)
      expect(page).to have_content(source)
    end
  end
end
