require 'feature_helper'

RSpec.feature 'View a list of newsletter_subscribers' do
  given(:admin) { create(:admin) }
  given!(:newsletter_subscribers) { create_list(:newsletter_subscriber, 2) }

  scenario 'Admin sees a list of newsletter_subscribers' do
    login(admin)

    visit admin_newsletter_subscribers_path

    newsletter_subscribers.each do |newsletter_subscriber|
      within "#newsletter_subscriber_#{newsletter_subscriber.id}" do
        expect(page).to have_content(newsletter_subscriber.email)
      end
    end
  end
end
