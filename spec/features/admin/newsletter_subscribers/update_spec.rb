require 'feature_helper'

RSpec.feature 'Update a newsletter_subscriber' do
  given(:admin) { create(:admin) }
  given!(:newsletter_subscriber) { create(:newsletter_subscriber, email: 'initial@email.po') }

  scenario 'Admin updates a newsletter_subscribers' do
    login(admin)
    visit edit_admin_newsletter_subscriber_path(newsletter_subscriber)

    within 'form#edit_subscriber' do
      fill_in 'subscriber_email', with: 'new@email.po'
      find('input[type="submit"]').click
    end

    within "#newsletter_subscriber_#{newsletter_subscriber.id}" do
      expect(page).to have_no_content('Initial email')
      expect(page).to have_content('new@email.po')
    end
  end
end
