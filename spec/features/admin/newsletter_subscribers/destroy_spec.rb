require 'feature_helper'

RSpec.feature 'Destroy a newsletter_subscriber' do
  given(:admin) { create(:admin) }
  given!(:newsletter_subscriber) { create(:newsletter_subscriber) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin updates a newsletter_subscribers' do
    login(admin)
    visit edit_admin_newsletter_subscriber_path(newsletter_subscriber)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_newsletter_subscribers_path)
    expect(page).to have_no_selector("#newsletter_subscriber_#{newsletter_subscriber.id}")
  end
end
