require 'feature_helper'

RSpec.feature 'Create a new newsletter_subscribers' do
  given(:admin) { create(:admin) }
  given(:add_button) { t('admin.newsletter.subscribers.index.actions.add') }

  scenario 'Admin creates an newsletter_subscribers' do
    login(admin)
    visit admin_newsletter_subscribers_path
    click_on add_button

    within 'form#new_subscriber' do
      fill_in 'subscriber_email', with: 'new@email.po'
      find('input[type="submit"]').click
    end

    within '#subscribers' do
      expect(page).to have_content('new@email.po')
    end
  end
end
