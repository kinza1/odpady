require 'feature_helper'

RSpec.feature 'Destroy a manual' do
  given(:admin) { create(:admin) }
  given!(:manual_category) { create(:manual_category) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin deletes a manual' do
    login(admin)
    visit edit_admin_manual_category_path(manual_category)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_manual_categories_path)
    expect(page).to have_no_selector("#manual_category_#{manual_category.id}")
  end
end
