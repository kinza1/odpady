require 'feature_helper'

RSpec.feature 'View a list of manuals in admin panel' do
  given(:admin) { create(:admin) }
  given!(:category) { create(:manual_category) }
  given!(:manuals) { create_list(:manual, 2, category: category) }

  scenario 'Admin sees a list of manuals' do
    login(admin)

    visit admin_manual_category_manuals_path(category)

    manuals.each do |manual|
      within "#manual_#{manual.id}" do
        expect(page).to have_content(manual.name)
      end
    end
  end
end
