require 'feature_helper'

RSpec.feature 'Update a manual' do
  given(:admin) { create(:admin) }
  given(:category) { create(:manual_category) }
  given!(:manual) { create(:manual, name: 'Initial name', category: category) }

  scenario 'Admin updates a manual' do
    login(admin)

    visit edit_admin_manual_category_manual_path(category, manual)

    within 'form' do
      fill_in 'manual_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#manual_#{manual.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
