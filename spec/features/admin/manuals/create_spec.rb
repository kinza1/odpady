require 'feature_helper'

RSpec.feature 'Create a new manual_category' do
  given(:admin) { create(:admin) }
  given!(:category) { create(:manual_category) }
  given(:add_button) { t('admin.manuals.index.actions.add') }

  scenario 'Admin creates a manual_category' do
    login(admin)
    visit admin_manual_category_manuals_path(category)
    click_on add_button

    within 'form#new_manual' do
      fill_in 'manual_name', with: 'nazwa'
      fill_in 'manual_body', with: 'body'
      find('input[type="submit"]').click
    end

    within '#manuals' do
      expect(page).to have_content('nazwa')
    end
  end
end
