require 'feature_helper'

RSpec.feature 'View a list of publications' do
  given(:admin) { create(:admin) }
  given!(:publications) { create_list(:publication, 2) }

  scenario 'Admin sees a list of publications' do
    login(admin)

    visit admin_publications_path

    publications.each do |publication|
      within "#publication_#{publication.id}" do
        expect(page).to have_content(publication.name)
      end
    end
  end
end
