require 'feature_helper'

RSpec.feature 'Update a publication' do
  given(:admin) { create(:admin) }
  given!(:publication) { create(:publication, name: 'Initial name') }

  scenario 'Admin updates a publication' do
    login(admin)
    visit edit_admin_publication_path(publication)

    within 'form#edit_publication' do
      fill_in 'publication_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#publication_#{publication.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
