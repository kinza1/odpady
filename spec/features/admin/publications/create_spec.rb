require 'feature_helper'

RSpec.feature 'Create a new publication' do
  given(:admin) { create(:admin) }

  given(:name) { 'Pozwolenia emisyjne w systemie przepisów o ochronie środowiska przed zanieczyszczeniem – po zmianach z lipca 2014 r.' }
  given(:author) { 'Marek GÓRSKI' }

  scenario 'Admin creates a publication' do
    login(admin)
    visit new_admin_publication_path

    within 'form#new_publication' do
      fill_in 'publication_name', with: name
      fill_in 'publication_author', with: author
      page.attach_file 'publication_pdf', "#{Rails.root}/spec/fixtures/publication.pdf"
      find('input[type="submit"]').click
    end

    within "#publication_#{Publication.last.id}" do
      expect(page).to have_content(name)
      expect(page).to have_content(author)
    end
  end
end
