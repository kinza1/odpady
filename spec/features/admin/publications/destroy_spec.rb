require 'feature_helper'

RSpec.feature 'Destroy a publication' do
  given(:admin) { create(:admin) }
  given!(:publication) { create(:publication) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin delete a publication' do
    login(admin)
    visit edit_admin_publication_path(publication)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_publications_path)
    expect(page).to have_no_selector("#publication_#{publication.id}")
  end
end
