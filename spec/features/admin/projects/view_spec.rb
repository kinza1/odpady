require 'feature_helper'

RSpec.feature 'View a list of projects in admin panel' do
  given(:admin) { create(:admin) }
  given!(:category) { create(:project_category) }
  given!(:projects) { create_list(:project, 2, category: category) }

  scenario 'Admin sees a list of projects' do
    login(admin)

    visit admin_project_category_projects_path(category)

    projects.each do |project|
      within "#project_#{project.id}" do
        expect(page).to have_content(project.name)
      end
    end
  end
end
