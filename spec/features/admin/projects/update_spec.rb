require 'feature_helper'

RSpec.feature 'Update a project' do
  given(:admin) { create(:admin) }
  given(:category) { create(:project_category) }
  given!(:project) { create(:project, name: 'Initial name', category: category) }

  scenario 'Admin updates a project' do
    login(admin)

    visit edit_admin_project_category_project_path(category, project)

    within 'form' do
      fill_in 'project_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#project_#{project.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
