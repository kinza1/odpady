require 'feature_helper'

RSpec.feature 'Create a new project' do
  given(:admin) { create(:admin) }
  given(:category) { create(:project_category) }
  given(:add_button) { t('admin.projects.index.actions.add') }

  scenario 'Admin creates a project' do
    login(admin)
    visit admin_project_category_projects_path(category)
    click_on add_button

    within 'form#new_project' do
      fill_in 'project_name', with: 'nazwa'
      fill_in 'project_body', with: 'producenci'
      fill_in 'project_body', with: 'https://www.mos.gov.pl/g2/big/2015_08/598410f95f0729e6ee385372f4eb0672.pdf'
      find('input[type="submit"]').click
    end

    within '#projects' do
      expect(page).to have_content('nazwa')
    end
  end
end
