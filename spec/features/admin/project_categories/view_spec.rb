require 'feature_helper'

RSpec.feature 'View a list of project_categories' do
  given(:admin) { create(:admin) }
  given!(:project_categories) { create_list(:project_category, 2) }

  scenario 'Admin sees a list of project_categories' do
    login(admin)

    visit admin_project_categories_path

    project_categories.each do |project_category|
      within "#project_category_#{project_category.id}" do
        expect(page).to have_content(project_category.name)
      end
    end
  end
end
