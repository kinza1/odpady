require 'feature_helper'

RSpec.feature 'Update a category' do
  given(:admin) { create(:admin) }
  given!(:union_document_category) { create(:union_document_category, name: 'Initial name') }

  scenario 'Admin updates a category' do
    login(admin)
    visit edit_admin_union_document_category_path(union_document_category)

    within 'form#edit_union_document_category' do
      fill_in 'union_document_category_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#union_document_category_#{union_document_category.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
