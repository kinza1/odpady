require 'feature_helper'

RSpec.feature 'View a list of union_document_categories' do
  given(:admin) { create(:admin) }
  given!(:union_document_categories) { create_list(:union_document_category, 2) }

  scenario 'Admin sees a list of union_document_categories' do
    login(admin)

    visit admin_union_document_categories_path

    union_document_categories.each do |union_document_category|
      within "#union_document_category_#{union_document_category.id}" do
        expect(page).to have_content(union_document_category.name)
      end
    end
  end
end
