require 'feature_helper'

RSpec.feature 'Create a new union_document_category' do
  given(:admin) { create(:admin) }

  given(:add_button) { t('admin.union_document_categories.index.actions.add') }

  scenario 'Admin creates a union_document_category' do
    login(admin)
    visit admin_union_document_categories_path
    click_on add_button

    within 'form#new_union_document_category' do
      fill_in 'union_document_category_name', with: 'producenci'
      find('input[type="submit"]').click
    end

    within '#union_document_categories' do
      expect(page).to have_content('producenci')
    end
  end
end
