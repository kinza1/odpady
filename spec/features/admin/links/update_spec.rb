require 'feature_helper'

RSpec.feature 'Update a link' do
  given(:admin) { create(:admin) }
  given!(:link) { create(:link, title: 'Initial title') }

  scenario 'Admin updates a link' do
    login(admin)
    visit edit_admin_link_path(link)

    within 'form#edit_link' do
      fill_in 'link_title', with: 'New title'
      find('input[type="submit"]').click
    end

    within "#link_#{link.id}" do
      expect(page).to have_no_content('Initial title')
      expect(page).to have_content('New title')
    end
  end
end
