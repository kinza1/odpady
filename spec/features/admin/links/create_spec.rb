require 'feature_helper'

RSpec.feature 'Create a new link' do
  given(:admin) { create(:admin) }

  scenario 'Admin creates a link' do
    login(admin)
    visit new_admin_link_path

    within 'form#new_link' do
      fill_in 'link_title', with: 'title'
      fill_in 'link_label', with: 'label'
      fill_in 'link_url', with: 'http://page.com'
      find('input[type="submit"]').click
    end

    within "#link_#{Link.last.id}" do
      expect(page).to have_content('label')
      expect(page).to have_content('title')
    end
  end
end
