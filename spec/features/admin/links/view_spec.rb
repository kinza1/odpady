require 'feature_helper'

RSpec.feature 'View a list of links' do
  given(:admin) { create(:admin) }
  given!(:links) { create_list(:link, 2) }

  scenario 'Admin sees a list of links' do
    login(admin)

    visit admin_links_path

    links.each do |link|
      within "#link_#{link.id}" do
        expect(page).to have_content(link.title)
      end
    end
  end
end
