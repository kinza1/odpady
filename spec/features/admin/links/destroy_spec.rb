require 'feature_helper'

RSpec.feature 'Destroy a link' do
  given(:admin) { create(:admin) }
  given!(:link) { create(:link) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin delete a link' do
    login(admin)
    visit edit_admin_link_path(link)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_links_path)
    expect(page).to have_no_selector("#link_#{link.id}")
  end
end
