require 'feature_helper'

RSpec.feature 'Update a waste' do
  given(:admin) { create(:admin) }
  given!(:waste) { create(:waste, name: 'Initial name') }

  scenario 'Admin updates a waste' do
    login(admin)
    visit edit_admin_waste_path(waste)

    within 'form#edit_waste' do
      fill_in 'waste_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#waste_#{waste.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
