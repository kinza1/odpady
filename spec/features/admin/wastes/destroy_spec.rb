require 'feature_helper'

RSpec.feature 'Destroy a waste' do
  given(:admin) { create(:admin) }
  given!(:waste) { create(:waste) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin delete a waste' do
    login(admin)
    visit edit_admin_waste_path(waste)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_wastes_path)
    expect(page).to have_no_selector("#waste_#{waste.id}")
  end
end
