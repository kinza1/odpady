require 'feature_helper'

RSpec.feature 'Create a new waste' do
  given(:admin) { create(:admin) }

  given(:code) { '01' }
  given(:name) { 'Test code' }

  scenario 'Admin creates a waste' do
    login(admin)
    visit new_admin_waste_path

    within 'form#new_waste' do
      fill_in 'waste_code', with: code
      fill_in 'waste_name', with: name
      find('input[type="submit"]').click
    end

    within "#waste_#{Waste.last.id}" do
      expect(page).to have_content(code)
      expect(page).to have_content(name)
    end
  end
end
