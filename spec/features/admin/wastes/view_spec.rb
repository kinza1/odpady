require 'feature_helper'

RSpec.feature 'View a list of wastes' do
  given(:admin) { create(:admin) }
  given!(:wastes) { create_list(:waste, 2) }

  scenario 'Admin sees a list of wastes' do
    login(admin)

    visit admin_wastes_path

    wastes.each do |waste|
      within "#waste_#{waste.id}" do
        expect(page).to have_content(waste.name)
      end
    end
  end
end
