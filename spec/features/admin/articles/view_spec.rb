require 'feature_helper'

RSpec.feature 'View a list of articles' do
  given(:admin) { create(:admin) }
  given!(:articles) { create_list(:article, 2) }

  scenario 'Admin sees a list of articles' do
    login(admin)

    visit admin_articles_path

    articles.each do |article|
      within "#article_#{article.id}" do
        expect(page).to have_content(article.name)
      end
    end
  end
end
