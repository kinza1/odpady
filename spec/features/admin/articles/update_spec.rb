require 'feature_helper'

RSpec.feature 'Update a article' do
  given(:admin) { create(:admin) }
  given!(:article) { create(:article, name: 'Initial name') }

  scenario 'Admin updates a articles' do
    login(admin)
    visit edit_admin_article_path(article)

    within 'form#edit_article' do
      fill_in 'article_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#article_#{article.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
