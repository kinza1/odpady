require 'feature_helper'

RSpec.feature 'Destroy a article' do
  given(:admin) { create(:admin) }
  given!(:article) { create(:article) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin updates a articles' do
    login(admin)
    visit edit_admin_article_path(article)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_articles_path)
    expect(page).to have_no_selector("#article_#{article.id}")
  end
end
