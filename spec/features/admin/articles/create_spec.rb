require 'feature_helper'

RSpec.feature 'Create a new article' do
  given(:admin) { create(:admin) }
  given(:add_button) { t('admin.articles.index.actions.add') }

  before do
    @article = create :article
  end

  scenario 'Admin creates an article' do
    login(admin)
    visit admin_articles_path
    click_on add_button

    within 'form#new_article' do
      fill_in 'article_name', with: 'New article'
      fill_in 'article_body', with: 'One two three'
      page.attach_file 'article_image', "#{Rails.root}/spec/fixtures/article.png"
      find('input[type="submit"]').click
      chosen_select(@article.name, from: 'article_recommendation_ids')

      find('input[type="submit"]').click
    end

    expect(Article.last.recommendations.count).to_not eq 0

    within '#articles' do
      expect(page).to have_content('New article')
    end
  end
end
