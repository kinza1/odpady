require 'feature_helper'

RSpec.feature 'Create a new state' do
  given(:admin) { create(:admin) }

  given(:add_button) { t('admin.states.index.actions.add') }

  scenario 'Admin creates a state' do
    login(admin)
    visit admin_states_path
    click_on add_button

    within 'form#new_state' do
      fill_in 'state_name', with: 'dolnośląskie'
      find('input[type="submit"]').click
    end

    within '#states' do
      expect(page).to have_content('dolnośląskie')
    end
  end
end
