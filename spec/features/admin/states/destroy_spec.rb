require 'feature_helper'

RSpec.feature 'Destroy a state' do
  given(:admin) { create(:admin) }
  given!(:state) { create(:state) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin updates a states' do
    login(admin)
    visit edit_admin_state_path(state)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_states_path)
    expect(page).to have_no_selector("#state_#{state.id}")
  end
end
