require 'feature_helper'

RSpec.feature 'View a list of states' do
  given(:admin) { create(:admin) }
  given!(:states) { create_list(:state, 2) }

  scenario 'Admin sees a list of states' do
    login(admin)

    visit admin_states_path

    states.each do |state|
      within "#state_#{state.id}" do
        expect(page).to have_content(state.name)
      end
    end
  end
end
