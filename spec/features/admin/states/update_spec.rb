require 'feature_helper'

RSpec.feature 'Update a state' do
  given(:admin) { create(:admin) }
  given!(:state) { create(:state, name: 'Initial name') }

  scenario 'Admin updates a state' do
    login(admin)
    visit edit_admin_state_path(state)

    within 'form#edit_state' do
      fill_in 'state_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#state_#{state.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
