require 'feature_helper'

RSpec.feature 'Create a new interpretation' do
  given(:admin) { create(:admin) }
  given(:category) { create(:interpretation_category) }
  given(:add_button) { t('admin.interpretations.index.actions.add') }

  scenario 'Admin creates a interpretation' do
    login(admin)
    visit admin_interpretation_category_interpretations_path(category)
    click_on add_button

    within 'form#new_interpretation' do
      fill_in 'interpretation_body', with: 'producenci'
      fill_in 'interpretation_pdf', with: 'https://www.mos.gov.pl/g2/big/2015_08/598410f95f0729e6ee385372f4eb0672.pdf'
      find('input[type="submit"]').click
    end

    within '#interpretations' do
      expect(page).to have_content('producenci')
    end
  end
end
