require 'feature_helper'

RSpec.feature 'Update a interpretation' do
  given(:admin) { create(:admin) }
  given(:category) { create(:interpretation_category) }
  given!(:interpretation) { create(:interpretation, body: 'Initial body', category: category) }

  scenario 'Admin updates a interpretation' do
    login(admin)

    visit edit_admin_interpretation_category_interpretation_path(category, interpretation)

    within 'form' do
      fill_in 'interpretation_body', with: 'New body'
      find('input[type="submit"]').click
    end

    within "#interpretation_#{interpretation.id}" do
      expect(page).to have_no_content('Initial body')
      expect(page).to have_content('New body')
    end
  end
end
