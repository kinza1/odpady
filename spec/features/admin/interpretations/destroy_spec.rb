require 'feature_helper'

RSpec.feature 'Destroy a interpretation' do
  given(:admin) { create(:admin) }
  given!(:interpretation_category) { create(:interpretation_category) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin deletes a interpretation' do
    login(admin)
    visit edit_admin_interpretation_category_path(interpretation_category)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_interpretation_categories_path)
    expect(page).to have_no_selector("#interpretation_category_#{interpretation_category.id}")
  end
end
