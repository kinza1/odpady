require 'feature_helper'

RSpec.feature 'View a list of interpretations in admin panel' do
  given(:admin) { create(:admin) }
  given!(:category) { create(:interpretation_category) }
  given!(:interpretations) { create_list(:interpretation, 2, category: category) }

  scenario 'Admin sees a list of interpretations' do
    login(admin)

    visit admin_interpretation_category_interpretations_path(category)

    interpretations.each do |interpretation|
      within "#interpretation_#{interpretation.id}" do
        expect(page).to have_content(interpretation.body)
      end
    end
  end
end
