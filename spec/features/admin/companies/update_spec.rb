require 'feature_helper'

RSpec.feature 'Update a company' do
  given(:admin) { create(:admin) }
  given!(:company) { create(:company, name: 'Initial name') }

  scenario 'Admin updates a company' do
    login(admin)
    visit edit_admin_company_path(company)

    within 'form#edit_company' do
      fill_in 'company_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#company_#{company.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
