require 'feature_helper'

RSpec.feature 'View a list of companies' do
  given(:admin) { create(:admin) }
  given!(:companies) { create_list(:company, 2) }

  scenario 'Admin sees a list of companies' do
    login(admin)

    visit admin_companies_path

    companies.each do |company|
      within "#company_#{company.id}" do
        expect(page).to have_content(company.name)
      end
    end
  end
end
