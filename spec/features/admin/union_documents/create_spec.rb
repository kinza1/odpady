require 'feature_helper'

RSpec.feature 'Create a new union_document_category' do
  given(:admin) { create(:admin) }
  given!(:category) { create(:union_document_category) }
  given(:add_button) { t('admin.union_documents.index.actions.add') }

  scenario 'Admin creates a union_document_category' do
    login(admin)
    visit admin_union_document_category_union_documents_path(category)
    click_on add_button

    within 'form#new_union_document' do
      fill_in 'union_document_title', with: 'Tytuł'
      fill_in 'union_document_address', with: 'Adres'
      fill_in 'union_document_pdf', with: 'Pdf'
      fill_in 'union_document_eur_lex', with: 'eur_lex'
      find('input[type="submit"]').click
    end

    within '#union_documents' do
      expect(page).to have_content('Tytuł')
    end
  end
end
