require 'feature_helper'

RSpec.feature 'View a list of union_documents in admin panel' do
  given(:admin) { create(:admin) }
  given!(:category) { create(:union_document_category) }
  given!(:union_documents) { create_list(:union_document, 2, category: category) }

  scenario 'Admin sees a list of union_documents' do
    login(admin)

    visit admin_union_document_category_union_documents_path(category)

    union_documents.each do |union_document|
      within "#union_document_#{union_document.id}" do
        expect(page).to have_content(union_document.title)
      end
    end
  end
end
