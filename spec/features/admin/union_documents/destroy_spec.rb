require 'feature_helper'

RSpec.feature 'Destroy a category' do
  given(:admin) { create(:admin) }
  given!(:union_document_category) { create(:union_document_category) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin deletes a category' do
    login(admin)
    visit edit_admin_union_document_category_path(union_document_category)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_union_document_categories_path)
    expect(page).to have_no_selector("#union_document_category_#{union_document_category.id}")
  end
end
