require 'feature_helper'

RSpec.feature 'Update a union_document' do
  given(:admin) { create(:admin) }
  given(:category) { create(:union_document_category) }
  given!(:union_document) { create(:union_document, title: 'Initial title', category: category) }

  scenario 'Admin updates a union_document' do
    login(admin)

    visit edit_admin_union_document_category_union_document_path(category, union_document)

    within 'form' do
      fill_in 'union_document_title', with: 'New title'
      find('input[type="submit"]').click
    end

    within "#union_document_#{union_document.id}" do
      expect(page).to have_no_content('Initial title')
      expect(page).to have_content('New title')
    end
  end
end
