require 'feature_helper'

RSpec.feature 'Create a new law' do
  given(:admin) { create(:admin) }

  scenario 'Admin creates a law' do
    login(admin)
    visit new_admin_law_path

    within 'form#new_law' do
      fill_in 'law_title', with: 'title'
      fill_in 'law_address', with: 'address'
      fill_in 'law_pdf', with: 'Link to PDF'
      fill_in 'law_isap', with: 'Link to ISAP'
      find('input[type="submit"]').click
    end

    within "#law_#{Law.last.id}" do
      expect(page).to have_content('address')
      expect(page).to have_content('title')
    end
  end
end
