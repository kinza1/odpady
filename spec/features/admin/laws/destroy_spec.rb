require 'feature_helper'

RSpec.feature 'Destroy a law' do
  given(:admin) { create(:admin) }
  given!(:law) { create(:law) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin delete a law' do
    login(admin)
    visit edit_admin_law_path(law)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_laws_path)
    expect(page).to have_no_selector("#law_#{law.id}")
  end
end
