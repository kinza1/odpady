require 'feature_helper'

RSpec.feature 'Update a law' do
  given(:admin) { create(:admin) }
  given!(:law) { create(:law, title: 'Initial title') }

  scenario 'Admin updates a law' do
    login(admin)
    visit edit_admin_law_path(law)

    within 'form#edit_law' do
      fill_in 'law_title', with: 'New title'
      find('input[type="submit"]').click
    end

    within "#law_#{law.id}" do
      expect(page).to have_no_content('Initial title')
      expect(page).to have_content('New title')
    end
  end
end
