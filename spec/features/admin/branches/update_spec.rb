require 'feature_helper'

RSpec.feature 'Update a branch' do
  given(:admin) { create(:admin) }
  given!(:branch) { create(:branch, name: 'Initial name') }

  scenario 'Admin updates a branch' do
    login(admin)
    visit edit_admin_branch_path(branch)

    within 'form#edit_branch' do
      fill_in 'branch_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#branch_#{branch.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
