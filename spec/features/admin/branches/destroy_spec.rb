require 'feature_helper'

RSpec.feature 'Destroy a branch' do
  given(:admin) { create(:admin) }
  given!(:branch) { create(:branch) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin deletes a branch' do
    login(admin)
    visit edit_admin_branch_path(branch)

    within '.actions' do
      click_on destroy_button
    end

    expect(current_path).to eq(admin_branches_path)
    expect(page).to have_no_selector("#branch_#{branch.id}")
  end
end
