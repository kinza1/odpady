require 'feature_helper'

RSpec.feature 'Create a new branch' do
  given(:admin) { create(:admin) }

  given(:add_button) { t('admin.branches.index.actions.add') }

  scenario 'Admin creates a branch' do
    login(admin)
    visit admin_branches_path
    click_on add_button

    within 'form#new_branch' do
      fill_in 'branch_name', with: 'producenci'
      find('input[type="submit"]').click
    end

    within '#branches' do
      expect(page).to have_content('producenci')
    end
  end
end
