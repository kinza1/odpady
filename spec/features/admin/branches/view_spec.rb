require 'feature_helper'

RSpec.feature 'View a list of branches' do
  given(:admin) { create(:admin) }
  given!(:branches) { create_list(:branch, 2) }

  scenario 'Admin sees a list of branches' do
    login(admin)

    visit admin_branches_path

    branches.each do |branch|
      within "#branch_#{branch.id}" do
        expect(page).to have_content(branch.name)
      end
    end
  end
end
