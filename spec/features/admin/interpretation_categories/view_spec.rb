require 'feature_helper'

RSpec.feature 'View a list of interpretation_categories' do
  given(:admin) { create(:admin) }
  given!(:interpretation_categories) { create_list(:interpretation_category, 2) }

  scenario 'Admin sees a list of interpretation_categories' do
    login(admin)

    visit admin_interpretation_categories_path

    interpretation_categories.each do |interpretation_category|
      within "#interpretation_category_#{interpretation_category.id}" do
        expect(page).to have_content(interpretation_category.name)
      end
    end
  end
end
