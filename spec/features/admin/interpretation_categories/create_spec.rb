require 'feature_helper'

RSpec.feature 'Create a new interpretation_category' do
  given(:admin) { create(:admin) }

  given(:add_button) { t('admin.interpretation_categories.index.actions.add') }

  scenario 'Admin creates a interpretation_category' do
    login(admin)
    visit admin_interpretation_categories_path
    click_on add_button

    within 'form#new_interpretation_category' do
      fill_in 'interpretation_category_name', with: 'producenci'
      find('input[type="submit"]').click
    end

    within '#interpretation_categories' do
      expect(page).to have_content('producenci')
    end
  end
end
