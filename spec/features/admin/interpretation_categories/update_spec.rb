require 'feature_helper'

RSpec.feature 'Update a category' do
  given(:admin) { create(:admin) }
  given!(:interpretation_category) { create(:interpretation_category, name: 'Initial name') }

  scenario 'Admin updates a category' do
    login(admin)
    visit edit_admin_interpretation_category_path(interpretation_category)

    within 'form#edit_interpretation_category' do
      fill_in 'interpretation_category_name', with: 'New name'
      find('input[type="submit"]').click
    end

    within "#interpretation_category_#{interpretation_category.id}" do
      expect(page).to have_no_content('Initial name')
      expect(page).to have_content('New name')
    end
  end
end
