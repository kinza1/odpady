require 'feature_helper'

RSpec.feature 'Destroy a banner' do
  given(:admin) { create(:admin) }
  given!(:banner) { create(:banner) }

  given(:destroy_button) { t('shared.destroy') }

  scenario 'Admin destroys a banner' do
    login(admin)

    visit admin_banners_path

    within "#banner_#{banner.id}" do
      find('.fa.fa-cog').click
      click_on destroy_button
    end

    expect(current_path).to eq(admin_banners_path)
    expect(page).to have_no_selector("#banner_#{banner.id}")
  end
end
