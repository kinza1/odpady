require 'feature_helper'

RSpec.feature 'View a list of banners' do
  given(:admin) { create(:admin) }
  given!(:banners) { create_list(:banner, 2) }

  scenario 'Admin sees a list of banners' do
    login(admin)

    visit admin_banners_path

    banners.each do |banner|
      within "#banner_#{banner.id}" do
        expect(page).to have_content(banner.description)
      end
    end
  end
end
