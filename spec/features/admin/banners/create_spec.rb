require 'feature_helper'

RSpec.feature 'Create a new banner' do
  given(:admin) { create(:admin) }

  given(:add_button) { t('admin.banners.index.actions.add') }

  scenario 'Admin creates an banner' do
    login(admin)
    visit admin_banners_path
    click_on add_button

    within 'form#new_banner' do
      fill_in 'banner_description', with: 'New banner'
      fill_in 'banner_link', with: 'http://google.com'
      page.attach_file 'banner_image', "#{Rails.root}/spec/fixtures/company.png"
      find('input[type="submit"]').click
    end

    within '#banners' do
      expect(page).to have_content('New banner')
    end
  end
end
