require 'feature_helper'

RSpec.feature 'Update a banner' do
  given(:admin) { create(:admin) }
  given!(:banner) { create(:banner, description: 'Initial description') }

  scenario 'Admin updates a banner' do
    login(admin)
    visit edit_admin_banner_path(banner)

    within 'form' do
      fill_in 'banner_description', with: 'New description'
      find('input[type="submit"]').click
    end

    within "#banner_#{banner.id}" do
      expect(page).to have_no_content('Initial description')
      expect(page).to have_content('New description')
    end
  end
end
