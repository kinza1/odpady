require 'feature_helper'

RSpec.feature 'View a list of definitions' do
  given!(:definitions) { create_list(:definition, 2) }
  scenario 'View a list of definitions' do
    visit definitions_path

    definitions.each do |definition|
      within "#definition_#{definition.id}" do
        expect(page).to have_content(definition.name)
      end
    end
  end
end
