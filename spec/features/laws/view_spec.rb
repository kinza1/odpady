require 'feature_helper'

RSpec.feature 'View a list of laws' do
  given!(:laws) { create_list(:law, 2) }

  scenario 'View a list of laws' do
    visit laws_path
    laws.each do |law|
      within "#law_#{law.id}" do
        expect(page).to have_content(law.title)
      end
    end
  end
end
