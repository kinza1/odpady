require 'feature_helper'

RSpec.feature 'Create a new company' do
  given!(:state) { create(:state) }

  scenario 'User creates a company' do
    visit new_company_path

    within 'form#new_company' do
      fill_in 'company_name', with: 'Więcek-Łukasiewicz'
      fill_in 'company_homepage', with: 'http://toy.biz/mya'
      fill_in 'company_email', with: 'jeika@borer.com'
      fill_in 'company_zipcode', with: '92-179'
      fill_in 'company_city', with: 'Gołdap'
      fill_in 'company_street', with: 'ul. Zawada'
      select state.name, from: 'company[state_id]'
      fill_in 'company_phone', with: '42-482-38-80'
      fill_in 'company_cellphone', with: '72-922-67-47'
      fill_in 'company_fax', with: '65-334-02-93'
      fill_in 'company_contact_person', with: 'Oskar Przybylski'
      fill_in 'company_business_profile', with: 'Optional client-driven alliance'
      page.attach_file 'company_logo', "#{Rails.root}/spec/fixtures/company.png"
      find('input[type="submit"]').click
    end

    expect(current_path).to eq(companies_path)

    created_company = Company.order(id: :asc).last
    expect(created_company.name).to eq('Więcek-Łukasiewicz')
  end
end
