require 'feature_helper'

RSpec.feature 'View a list of companies' do
  given!(:active_companies) { create_list(:company, 2, active: true) }
  given!(:inactive_company) { create(:company, active: false) }

  scenario 'View a list of companies' do
    visit companies_path

    active_companies.each do |company|
      within "#company_#{company.id}" do
        expect(page).to have_content(company.name)
      end
    end

    expect(page).to have_no_selector("#company_#{inactive_company.id}")
  end
end
