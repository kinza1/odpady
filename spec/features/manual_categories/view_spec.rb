require 'feature_helper'

RSpec.feature 'View a list of manual categories' do
  given!(:categories) { create_list(:manual_category, 2) }

  scenario 'View a list of manual_categories' do
    visit manuals_categories_path
    categories.each do |category|
      within "#manual_category_#{category.id}" do
        expect(page).to have_content(category.name)
      end
    end
  end
end
