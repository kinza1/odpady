require 'feature_helper'

RSpec.feature 'View a list of departments' do
  given!(:departments) { create_list(:department, 2) }
  scenario 'View a list of departments' do
    visit departments_path

    departments.each do |department|
      within "#department_#{department.id}" do
        expect(page).to have_content(department.name)
      end
    end
  end
end
