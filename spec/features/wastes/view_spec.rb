require 'feature_helper'

RSpec.feature 'View a list of wastes' do
  given!(:wastes) { create_list(:waste, 2) }
  scenario 'View a list of wastes' do
    visit wastes_path

    within '.catalogue__collapsed__categories' do
      wastes.each do |waste|
        expect(page).to have_content(waste.code)
        expect(page).to have_content(waste.name)
      end
    end
  end
end
