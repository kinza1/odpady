source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.6'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.15'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# JavaScript libraries
gem 'jquery-rails'
gem 'bootstrap-datepicker-rails'
gem 'compass-rails'
gem 'chosen-rails'
gem 'ckeditor'

# CSS libraries
gem 'bootstrap-sass'
gem 'font-awesome-rails'

# Use Puma as the app server
gem 'puma'

# Use Capistrano for deployment
group :development do
  gem 'capistrano', '3.5.0'
  gem 'capistrano-rvm'
  gem 'capistrano-rails'
  gem 'capistrano3-puma'
  gem 'capistrano-sidekiq'
end

# Testing framework
group :development, :test do
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'shoulda-matchers'
  gem 'capybara'
  gem 'poltergeist'
  gem 'database_cleaner'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # Better error page for Rack apps
  gem 'better_errors'
  gem 'binding_of_caller'

  # Use letter opener for test emails
  gem 'letter_opener'

  # Static code analyzer
  gem 'rubocop', require: false

  # Git pre-commit hooks
  gem 'overcommit', require: false

  # Skip assets log
  gem 'quiet_assets'

  gem 'faker'
end

# Call 'byebug' anywhere in the code to stop execution and get a debugger console
gem 'byebug', group: [:development, :test]

# Use slim as a template markup language
gem 'slim-rails'

# Authentication
gem 'devise'

# Authorization
gem 'pundit'

# Internationalization gems
gem 'rails-i18n' # Global localization rules
gem 'devise-i18n' # Devise translations
# gem 'i18n-js' # JavaScript localization

# Form helpers
gem 'simple_form'
gem 'cocoon'

# Attributes on Steroids for PORO
gem 'virtus'

# Liquid template language support
gem 'liquidize'

# Presentation logic layer
gem 'draper'

# Upload files
gem 'carrierwave'
gem 'mini_magick'

# Pagination
gem 'kaminari'

# Pagination styles for admin panel
gem 'bootstrap-kaminari-views'

# Flexible search
gem 'searchlight'

# Delayed jobs
gem 'sidekiq'

# Search
gem 'elasticsearch-model'
gem 'elasticsearch-rails'

# CSV importer
gem 'smarter_csv'
